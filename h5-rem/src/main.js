import { createApp } from 'vue'
import App from './App.vue'

// 引入 amfe-flexible
import 'amfe-flexible'

createApp(App).mount('#app')
