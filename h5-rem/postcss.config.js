module.exports = {
  plugins: {
    'postcss-pxtorem': {
      // 37.5 表示 375 设计稿
      // 37.5 是在 375 宽度下, amfe-flexible 计算出来的 html 元素 的 font-size
      rootValue: 37.5, 
      // 哪些css属性可以转换
      propList: ['*'],
    }
  }
}