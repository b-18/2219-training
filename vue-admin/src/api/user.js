import { post } from "../utils/request"

// export function login(body) {
//   // post 请求
//   return request.post('/admin/login', body)
// }
export function login(adminname, password) {
  // request.post(地址, object对象)
  return post('/admin/login', {
    adminname,
    password
  })
}