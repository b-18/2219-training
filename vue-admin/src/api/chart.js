import { get } from '../utils/request'

// 获取图表数据
export function getChartData() {
  return get('/data/simpleData')
}