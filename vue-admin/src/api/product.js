import { get, post } from '../utils/request'

// 产品列表
export function getProductList(count, limitNum) {
  return get('/pro/list', {
    count, // 页码
    limitNum // 每一页显示多少个
})
}

// 修改秒杀状态
export function updateSecKillFlag(proid, flagNum) {
  return post('/pro/updateFlag', {
    proid,
    type: 'isseckill',
    // flagNum 1: 选中 2: 未选中
    // flag 要求传 true 或 false
    flag: flagNum === 1
  })
}