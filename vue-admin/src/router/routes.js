import LoginPage from '../page/login.vue'
import AdminPage from '../page/admin.vue'
import NotFoundPage from '../page/404.vue'

import DashboardPage from '../page/dashboard.vue'

import ProductPage from '../page/product.vue'
import ProductList from '../page/product-list.vue'
import ProductSecKill from '../page/product-seckill.vue'
import ProductRecommend from '../page/product-recommend.vue'
import ProductCategory from '../page/product-category.vue'

import EditorPage from '../page/editor.vue'
import TinyMce from '../page/editor-tinymce.vue'

import ChartPage from '../page/chart.vue'
import Echarts from '../page/chart-echarts.vue'
import EchartsBar from '../page/echarts-bar.vue'
import EchartsLine from '../page/echarts-line.vue'
import EchartsPie from '../page/echarts-pie.vue'
import EchartsScatter from '../page/echarts-scatter.vue'

const routes = [
  {
    path: '/',
    // 默认重定向到登录页
    redirect: { name: 'login' }
  },
  {
    path: '/login',
    name: 'login',
    component: LoginPage
  },
  {
    path: '/admin',
    name: 'admin',
    component: AdminPage,
    meta: { title: 'Admin首页' },

    // 路由级别的拦截
    beforeEnter() {
      // 进入admin页面, 一定走这里
      // if (!localStorage.getItem('token')) {
      //   return {
      //     name: 'login'
      //   }
      // }
    },
    children: [
      {
        path: 'dashboard',
        component: DashboardPage,
        // 路由元信息
        // meta 用来传递自定义属性
        meta: { title: 'DashBoard' }
      },
      {
        path: 'product',
        component: ProductPage,
        meta: { title: '产品管理' },
        children: [
          {
            path: 'list',
            component: ProductList,
            meta: { title: '产品列表' }
          },
          {
            path: 'seckill',
            component: ProductSecKill,
            meta: { title: '秒杀管理' }
          },
          {
            path: 'recommend',
            component: ProductRecommend,
            meta: { title: '推荐管理' }
          },
          {
            path: 'category',
            component: ProductCategory,
            meta: { title: '分类列表' }
          }
        ]
      },
      {
        path: 'editor',
        component: EditorPage,
        meta: { title: '富文本编辑器' },
        children: [
          {
            path: 'tinymce',
            component: TinyMce,
            meta: { title: 'TinyMCE' },
          }
        ]
      },
      {
        path: 'chart',
        component: ChartPage,
        meta: { title: '可视化图表' },
        children: [
          {
            path: 'echarts',
            component: Echarts,
            meta: { title: 'ECharts' },
            children: [
              {
                path: 'bar',
                component: EchartsBar,
                meta: { title: '柱状图' }
              },
              {
                path: 'line',
                component: EchartsLine,
                meta: { title: '折线图' }
              },
              {
                path: 'pie',
                component: EchartsPie,
                meta: { title: '饼图' }
              },
              {
                path: 'scatter',
                component: EchartsScatter,
                meta: { title: '散点图' }
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: '/404',
    name: '404',
    component: NotFoundPage
  }
]


export default routes