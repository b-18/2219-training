import { createRouter, createWebHistory } from 'vue-router'

import routes from './routes'

const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach((to, from) => {
  // 没有找到对应的路由, 跳转到 404 页面
  if (to.matched.length === 0) {
    router.push({ name: '404' })
    return
  }

  // path 已 /admin 开头
  // if (to.path.startsWith('/admin') && !localStorage.getItem('token')) {
  //   // 进去 admin 之前, 检测下localStorage里是否有 token
  //   // 没有不让进, 跳转到登录
  //   router.push({ name: 'login' })
  //   return false
  // }
})

export default router