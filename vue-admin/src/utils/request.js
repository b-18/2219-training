import axios from "axios"
import { ElMessage } from 'element-plus'

import router from '../router'

const request = axios.create({
  // 接口地址的公共前缀
  baseURL: 'http://121.89.205.189:3000/admin'
  // baseURL: 'http://120.46.204.16:8008/admin'
})

// 请求拦截
// 所有的请求都会被这里拦截
request.interceptors.request.use((req) => {
  // req 表示请求对象
  // 给请求的 header 加 一个字段, 叫 token
  req.headers.token = localStorage.getItem('token')

  // 一定要返回
  return req
})

// 响应拦截
// 所有的请求返回都会走这里
// 1. 状态码全局处理(比如,自动跳登录s)
// 2. 统一处理返回的数据结构, 返回 res.data, 减少一层数据包裹
// 3. 统一弹后端返回的错误信息
request.interceptors.response.use((res) => {
  if (res.data.code === '10119') {
    // 没有token(或token过期), 自动跳转到登录
    router.push({ name: 'login'})
  }

  if (res.data.code !== '200') {
    // 状态码不是200时, 统一弹错误提示
    ElMessage({
      type: 'warning',
      // res.data.message是后端返回的
      message: res.data.message
    })
  }

  // {
  //   data: {
  //     data: {
  //       code: '200'
  //     }
  //   }
  // }
  // 通过这里处理变成:
  // {
  //   data: {
  //     code: '200'
  //   }
  // }
  // 取数据减少一层

  // 一定要 return
  return res.data
})

export default request

// 封装 get 请求
export function get(url, data) {
  return request.get(url, {
    params: data
  })
}

// post 请求
export const post = request.post