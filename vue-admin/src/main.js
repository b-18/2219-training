import { createApp } from 'vue'
import ElementPlus from 'element-plus'
/**
 * 引入 element-plus
 * 1) app.use 注册
 * 2) 引入 element-plus/dist/index.css
 */

import * as ElementIcons from '@element-plus/icons-vue'


import App from './App.vue'

import router from './router'

// element-plus 的 css 文件
import 'element-plus/dist/index.css'
import 'reset-css'


import './assets/style/layout.css'

const app = createApp(App)

// 注册 router
app.use(router)

// 注册 element plus
// 注册的组件时全局组件
// 在项目的模板里直接使用组件
app.use(ElementPlus)

// 注册图标
// 图标的menu组件名称 和 html的 menu 冲突了, 改个名字
// menu -> icon-menu
const keyAlias = {
  Menu: 'IconMenu'
}
for (let key of Object.keys(ElementIcons)) {
  // ?? 用法
  // 值1 ?? 值2
  // 当 值1 为 undefined 或者 null 的时候, 使用 值2
  app.component(keyAlias[key] ?? key, ElementIcons[key])
}
  
app.mount('#app')
