import { createRouter, createWebHistory } from 'vue-router'

import List from './views/list.vue'
import Detail from './views/detail.vue'
import Login from './views/login.vue'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      component: List
    },
    {
      path: '/detail/:id',
      component: Detail
    },
    {
      path: '/login',
      component: Login
    }
  ]
})

export default router