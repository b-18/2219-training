import axios from "axios"

const http = axios.create({
  baseURL: 'http://localhost:3000/api/mall'
})

http.interceptors.response.use(res => res.data)

export default http