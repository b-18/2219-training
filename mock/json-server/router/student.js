const jsonServer = require('json-server')
const path = require('path')

const router = jsonServer.router(path.join(__dirname, '..', 'db', 'student.json'))

router.render = (req, res) => {
  // 定义接口的返回格式
  // { code, message, data }
  res.jsonp({
    code: '200',
    message: 'success',
    data: res.locals.data
  })
}

module.exports = router