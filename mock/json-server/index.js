const jsonServer = require('json-server')
const mallRouter = require('./router/mall')
const todoRouter = require('./router/todo')
const studentRouter = require('./router/student')

// 1. 创建  json-server
const server = jsonServer.create()

// 默认附带了几个插件, 开启一下
const middlewares = jsonServer.defaults()
server.use(middlewares)

// 接口的地址的 前缀
server.use('/api/mall', mallRouter)
server.use('/api/todo', todoRouter)
server.use('/api/stumgr', studentRouter)

// 2. 启动 json-server
const port = 3000
server.listen(port, () => {
  console.log(`JSON Server is running at localhost:${port}`)
})