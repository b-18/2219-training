const redBorder = {
  mounted(el) {
    el.style.outline = '1px solid tomato'
  }
}

// plugin 是一个对象
// 必须包含一个 install 的方法
const redBorderPlugin = {
  // 安装方法
  install(app) {
    app.directive("red-border", redBorder);
  }
}

export default redBorderPlugin