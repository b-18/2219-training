

# 一、vue简介

Vue是一款用于构建用户界面的 JavaScript 框架。

它基于标准 HTML、CSS 和 JavaScript 构建，并提供了一套声明式的、组件化的编程模型，帮助你高效地开发用户界面。

无论是简单还是复杂的界面，Vue 都可以胜任。

# 二、vue3选项式API

绝大多数和vue2相同

## 1.vue3初探

### 1.1 MVX

目标：理解MVVM、MVC、MVP

MV系列框架中，M和V分别指Model层和View层，但其功能会因为框架的不同而变化。

Model层是数据模型，用来存储数据；

View层是视图，展示Model层的数据。

虽然在不同的框架中，Model层和View层的内容可能会有所差别，但是其基础功能不变，变的只是 `数据的传输方式 `。

#### 1.1.1 MVC(**Model-View-Controller**)

[MVC](https://so.csdn.net/so/search?q=MVC&spm=1001.2101.3001.7020)是模型-视图-控制器，它是MVC、MVP、MVVM这三者中最早产生的框架，其他两个框架是以它为基础发展而来的。

MVC的目的就是将M和V的代码分离，且MVC是单向通信，必须通过Controller来承上启下。

> Model：模型层，数据模型及其业务逻辑，是针对业务模型建立的数据结构，Model与View无关，而与业务有关。
>
> View：视图层，用于与用户实现交互的页面，通常实现数据的输入和输出功能。
>
> Controller：控制器，用于连接Model层和View层，完成Model层和View层的交互。还可以处理页面业务逻辑，它接收并处理来自用户的请求，并将Model返回给用户。

![image-20220908001836530](assets/image-20220908001836530.png)

上图可以看出各部分之间的通信是单向的，呈三角形状。

具体MVC框架流程图如图

![image-20220908002149336](assets/image-20220908002149336.png)

从上图可以看出，Controller层触发View层时，并不会更新View层中的数据，View层的数据是通过监听Model层数据变化自动更新的，与Controller层无关。换言之，Controller存在的目的是确保M和V的同步，一旦M改变，V应该同步更新。

同时，我们可以看到，MVC框架大部分逻辑都集中在Controller层，代码量也集中在Controller层，这带给Controller层很大压力，而已经有独立处理事件能力的View层却没有用到；而且，Controller层与View层之间是一一对应的，断绝了View层复用的可能，因而产生了很多冗余代码。

MVC  房东 -房客 -中介

为了解决上述问题，MVP框架被提出

#### 1.1.2 MVP（Model-View-Presenter）

MVP是模型-视图-表示器，它比MVC框架大概晚出现20年，是从MVC模式演变而来的。它们的基本思想有相同之处：Model层提供数据，View层负责视图显示，Controller/Presenter层负责逻辑的处理。将Controller改名为Presenter的同时改变了通信方向。

> Model：模型层，用于数据存储以及业务逻辑。
>
> View：视图层，用于展示与用户实现交互的页面，通常实现数据的输入和输出功能。
>
> Presenter：表示器，用于连接M层、V层，完成Model层与View层的交互，还可以进行业务逻辑的处理。

![image-20220908002243164](assets/image-20220908002243164.png)

上图可以看出各部分之间的通信是双向的。

> 在MVC框架中，View层可以通过访问Model层来更新，但在MVP框架中，View层不能再直接访问Model层，必须通过Presenter层提供的接口，然后Presenter层再去访问Model层。

![image-20220908002315234](assets/image-20220908002315234.png)

从上图可以看出，View层和Model层互不干涉，View层也自由了很多，所以View层可以抽离出来做成组件，在复用性上就比MVC框架好很多。

但是，由于View层和Model层都需要经过Presenter层，导致Presenter层比较复杂，维护起来也会有一定的问题；而且，因为没有绑定数据，所有数据都需要Presenter层进行“手动同步”，代码量较大，虽然比起MVC框架好很多，但还是有比较多冗余部分。

为了让View层和Model层的数据始终保持一致，MVVM框架出现了。

#### 1.1.3 **MVVM**（Model-View-ViewModel）

MVVM是模型-视图-视图模型。MVVM与MVP框架区别在于：MVVM采用双向绑定：View的变动，自动反映在ViewModel，反之亦然。

> Model：数据模型（数据处理业务），指的是后端传递的数据。
>
> View：视图，将Model的数据以某种方式展示出来。
>
> ViewModel：视图模型，数据的双向绑定（当Model中的数据发生改变时View就感知到，当View中的数据发生变化时Model也能感知到），是MVVM模式的核心。ViewModel 层把 Model 层和 View 层的数据同步自动化了，解决了 MVP 框架中数据同步比较麻烦的问题，不仅减轻了 ViewModel 层的压力，同时使得数据处理更加方便——只需告诉 View 层展示的数据是 Model 层中的哪一部分即可。

![image-20220908002410800](assets/image-20220908002410800.png)

上图可以看出各部分之间的通信是双向的，而且我们可以看出，MVVM框架图和MVP框架图很相似，两者都是从View层开始触发用户的操作，之后经过第三层，最后到达Model层。而关键问题就在于这第三层的内容，Presenter层是采用手动写方法来调用或修改View层和Model层；而ViewModel层双向绑定了View层和Model层，因此，随着View层的数据变化，系统会自动修改Model层的数据，反之同理。

具体MVVM框架流程图如图

![image-20220908002444206](assets/image-20220908002444206.png)

从上图可以看出，View层和Model层之间的数据传递经过了ViewModel层，ViewModel层并没有对其进行“手动绑定”，不仅使速度有了一定的提高，代码量也减少很多，相比于MVC框架和MVP框架，MVVM框架有了长足的进步。

从MVVM第一张图可以看出，MVVM框架有大致两个方向：

> 1、模型-->视图   ——实现方式：数据绑定
>
> 2、视图-->模型   ——实现方式：DOM事件监听

存在两个方向都实现的情况，叫做数据的双向绑定。双向数据绑定可以说是一个模板引擎，它会根据数据的变化实时渲染。如图View层和Model层之间的修改都会同步到对方。

![image-20220908002601014](assets/image-20220908002601014.png)

MVVM模型中数据绑定方法一般有四种：

> - 数据劫持vue2
> - 原生Proxy  vue3
> - 发布-订阅模式
> - 脏值检查

Vue2.js使用的就是数据劫持和发布-订阅模式两种方法。了解Vue.js数据绑定流程前，我们需要了解这三个概念：

> - Observer：数据监听器，用于监听数据变化，如果数据发生改变，不论是在View层还是在Model层，Observer都会知道，然后告诉Watcher。
> - Compiler：指定解析器，用于对数据进行解析，之后绑定指定的事件，在这里主要用于更新视图。
> - Watcher：订阅者。

> 首先将需要绑定的数据劫持方法找出来，之后用Observer监听这堆数据，如果数据发生变化，Observer就会告诉Watcher，然后Watcher会决定让那个Compiler去做出相应的操作，这样就完成了数据的双向绑定。

vue3.js使用更快的原生 Proxy，消除了之前 Vue2.x 中基于 Object.defineProperty 的实现所存在的很多限制：无法监听 属性的添加和删除、数组索引和长度的变更，并可以支持 Map、Set、WeakMap 和 WeakSet！

带来的特性：

> vue3.0实现响应式
>
> Proxy支持监听原生数组
>
> Proxy的获取数据，只会递归到需要获取的层级，不会继续递归
>
> Proxy可以监听数据的手动新增和删除

Proxy对象用于定义基本操作的自定义行为（如属性查找，赋值，枚举，函数调用等）。
其实就是在对目标对象的操作之前提供了拦截，可以对外界的操作进行过滤和改写，修改某些操作的默认行为，这样我们可以不直接操作对象本身，而是通过操作对象的代理对象来间接来操作对象，达到预期的目的~

### 1.2 vue特性

目标：理解声明式，对比传统DOM开发

![image-20220913091144383](assets/image-20220913091144383.png)

Vue从设计角度来讲，虽然能够涵盖这张图上所有的东西，但是你并不需要一上手就把所有东西全用上，都是可选的。

**声明式渲染和组件系统是**Vue的核心库所包含内容，而路由、状态管理、构建工具都有专门解决方案。这些解决方案相互独立，我们可以在核心的基础上任意选用其他的部件（以插件形势使用），不一定要全部整合在一起。

Vue.js的核心是一个允许采用简洁的模板语法来声明式的将数据渲染进DOM的系统。

假设需要输出 “hello 千锋HTML5大前端”

> 准备工作：cnpm
>
> https://npmmirror.com/
>
> ```sh
> $ npm install -g cnpm --registry=https://registry.npmmirror.com
> ```
>
> 以后就可以使用cnpm 代替 npm
>
> 如果遇到类似于以下**psl这种错误
>
> ![image-20220914103517117](assets/image-20220914103517117.png)
>
> 只需要找到这个文件删除即可（这个错误只会出现在windows电脑下）
>
> > 补充：如果使用cnpm出现 `randomUUID is not a function`，解决方法
> >
> > ```sh
> > $ npm uninstall -g cnpm
> > $ npm install cnpm@7.1.0 -g
> > ```

传统开发模式的原生js，jQuery代码如下：

```html
<div id="test"></div>
<!--原生js-->
<script>
  const msg = "hello 千锋HTML5大前端"
  const test = document.getElementById('test')
  test.innerHTML = msg // test.innerText = msg  test.contentText=""
</script>
<!--jQuery-->
<script>
	var msg = 'hello 千锋HTML5大前端'
  	$('#test').html(msg) // $('#test').text(msg)
</script>
```

> ```sh
> $ cnpm i vue jquery 
> ```
>
> 拷贝 `node_modules/vue/dist/vue.global.js`以及` vue.global.prod.js`，还有 `jquery/dist/jquery.js`以及`jquery.min.js`到lib文件夹

完整代码：`01_before.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>01_传统的DOM操作</title>
</head>
<body>
  <div id="jsDOM"></div>
  <div id="jQueryDOM"></div>
</body>
<script src="lib/jquery.js"></script>
<script>
  const msg = '<h1>hello 千锋HTML5大前端</h1>'
  // jsDOM
  const jsDOM = document.getElementById('jsDOM')
  jsDOM.innerHTML = msg // 解析输出
  // jsDOM.innerText = msg // 转义输出
  // jsDOM.textContent = msg // 转义输出

  // jQueryDOM
  const jQueryDOM = $("#jQueryDOM") // jQuery DOM 选择器
  jQueryDOM.html(msg) // 解析输出
  // jQueryDOM.text(msg) // 转义输出

  console.log(jsDOM)
  console.log(jQueryDOM[0])

</script>
</html>
```

`02_vue3.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>vue3解决dom问题</title>
  <script src="lib/vue.global.js"></script>
</head>
<body>
  <div id="app">
    {{ msg }}
  </div>
</body>
<script>
  // 创建应用的方法
  const { createApp } = window.Vue

  // 创建应用
  const app = createApp({
    // 业务
    data () { // 初始化数据，在vue3中data写法为函数，返回一个对象，对象中写初始化数据
      return {
        msg: 'hello 千锋HTML5大前端'
      }
    }
  })

  // 挂载应用
  app.mount('#app')

</script>
</html>
```

`03_vue2.html`

```sh
$ cnpm i vue@2 
```

> 拷贝 vue下的 vue.js 以及vue.min.js到lib文件夹

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>03_vue2解决DOM</title>
  <script src="lib/vue.js"></script>
</head>
<body>
  <div id="app">
    {{ msg }}
  </div>
</body>
<script>
  // new Vue({
  //   el: '#app', // 挂载到 id 为 app的DOM上
  //   data: { // 只有在new Vue 时才写为对象，其余时刻写为函数返回对象
  //     msg: 'hello 千锋HTML5大前端'
  //   }
  // })

  new Vue({
    data: { // 只有在new Vue 时才写为对象，其余时刻写为函数返回对象
      msg: 'hello 千锋HTML5大前端!!'
    }
  }).$mount('#app')
</script>
</html>
```



### 1.3 vue3十大新特性

```
* setup   ----   组合式API
* ref     ----   组合式API
* reactive  ----   组合式API
* 计算属性computed  ----   组合式API 以及 选项式API
* 计算属性watch     ----   组合式API 以及 选项式API
* watchEffect函数  ----   组合式API
* 生命周期钩子      ----   组合式API 以及 选项式API
* 自定义hook函数   ----   组合式API
* toRef和toRefs   ----   组合式API 以及 选项式API
* 其他新特性
  * shallowReactive 与 shallowRef ----   组合式API 
  * readonly 与 shallowReadonly ----   组合式API 
  * toRaw 与 markRaw ----   组合式API 
  * customRef ----   组合式API
  * provide 与 inject ----   组合式API 以及 选项式API
  * 响应式数据的判断 ----   组合式API 以及 选项式API
* 新的组件  ----- 类似于新增的HTML标签
  * Fragment
  * Teleport
  * Suspense
* 其他变化
  * data选项应始终被声明为一个函数
  * 过渡类名的更改
  * 移除keyCode作为 v-on 的修饰符，同时也不再支持config.keyCodes  --- 事件处理
  * 移除v-on.native修饰符  --- 事件处理
  * 移除过滤器（filter）  ---  单独讲解vue2和vue3差异化
```

### 1.4 创建第一个vue3应用

每个 Vue 应用都是通过 [`createApp`](https://cn.vuejs.org/api/application.html#createapp) 函数创建一个新的 **应用实例**：

```js
<div id="app"></div>

import { createApp } from 'vue'

const app = createApp({
  /* 根组件选项 */
})
app.mount('#app')
```

简单计数器案例：`04_counter.html`

```js
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>04_简单计数器</title>
</head>
<body>
  <div id="app">
    <div>count is: {{ count }}</div>
    <div>doubleCount is: {{ doubleCount }}</div>
    <button @click="count++">加1</button>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  Vue.createApp({
    data () {
      return {
        count: 10
      }
    },
    computed: {// 计算属性 依赖性
      doubleCount () {
        return this.count * 2
      }
    }
  }).mount('#app')
</script>
</html>
```

> 1.我们传入 `createApp` 的对象实际上是一个组件，每个应用都需要一个“根组件”，其他组件将作为其子组件。
>
> 应用实例必须在调用了 `.mount()` 方法后才会渲染出来。该方法接收一个“容器”参数，可以是一个实际的 DOM 元素或是一个 CSS 选择器字符串：

### 1.5 API风格

[参考链接](https://cn.vuejs.org/guide/extras/composition-api-faq.html#does-composition-api-cover-all-use-cases)

目标：选项式API以及组合式API如何选择

`05_composition.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>05_组合式API</title>
</head>
<body>
  <div id="app">
    <div>count is: {{ count }}</div>
    <button @click="count++">加1</button>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref } =  Vue

  createApp({
    setup () {

      const count = ref(10)

      return {
        count
      }
      
    }
  }).mount('#app')
</script>
</html>
```



使用组合式API可以

*  更好的逻辑复用
*  更灵活的代码组织
*  更好的类型推导
*  更小的生产包体积

> 选项式 API 确实允许你在编写组件代码时“少思考”，这是许多用户喜欢它的原因。然而，在减少费神思考的同时，它也将你锁定在规定的代码组织模式中，没有摆脱的余地，这会导致在更大规模的项目中难以进行重构或提高代码质量。在这方面，组合式 API 提供了更好的长期可维护性。
>
> 组合式 API 能够覆盖所有状态逻辑方面的需求
>
> 一个项目可以同时使用两种API
>
> 选项式API不会被抛弃

## 2.模板与指令

### 2.1 模板语法

学习：插值表达式、js表达式、v-cloak

Vue 使用一种基于 HTML 的模板语法，使我们能够声明式地将其组件实例的数据绑定到呈现的 DOM 上。所有的 Vue 模板都是语法层面合法的 HTML，可以被符合规范的浏览器和 HTML 解析器解析。

在底层机制中，Vue 会将模板编译成高度优化的 JavaScript 代码。结合响应式系统，当应用状态变更时，Vue 能够智能地推导出需要重新渲染的组件的最少数量，并应用最少的 DOM 操作。

#### 2.1.1 文本插值

最基本的数据绑定形式是文本插值，它使用的是“Mustache[ˈmʌstæʃ]”语法 (即双大括号)：

```
<span>Message: {{ msg }}</span>
```

> 双大括号标签会被替换为相应组件实例中 `msg` 属性的值。同时每次 `msg` 属性更改时它也会同步更新。

#### 2.1.2 js表达式

 Vue 实际上在所有的数据绑定中都支持完整的 JavaScript 表达式：

```
{{ number + 1 }}

{{ ok ? 'YES' : 'NO' }}

{{ message.split('').reverse().join('') }}

<div :id="`list-${id}`"></div>
```

> 这些表达式都会被作为 JavaScript ，以组件为作用域解析执行。
>
> 在 Vue 模板内，JavaScript 表达式可以被使用在如下场景上：
>
> - 在文本插值中 (双大括号)
> - 在任何 Vue 指令 (以 `v-` 开头的特殊 attribute) attribute 的值中
>
> 绑定在表达式中的方法在组件每次更新时都会被重新调用，因此**不**应该产生任何副作用，比如改变数据或触发异步操作。

#### 2.1.3 v-cloak

用于隐藏尚未完成编译的 DOM 模板。

当使用直接在 DOM 中书写的模板时，可能会出现一种叫做“未编译模板闪现”的情况：用户可能先看到的是还没编译完成的双大括号标签，直到挂载的组件将它们替换为实际渲染的内容。

`v-cloak` 会保留在所绑定的元素上，直到相关组件实例被挂载后才移除。配合像 `[v-cloak] { display: none }` 这样的 CSS 规则，它可以在组件编译完毕前隐藏原始模板。

```html
[v-cloak] {
  display: none;
}

<div v-cloak>
  {{ message }}
</div>
```

> 直到编译完成前，`<div>` 将不可见。

完整案例：`06_mustache.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>06_模板语法</title>
  <style>
    [v-cloak] {
      display: none;
    }
  </style>
</head>
<body>
  <!-- v-cloak -->
  <div id="app" v-cloak>
    <!-- 文本插值 -->
    <div>{{ msg }}</div>
    <!-- js表达式 -->
    <div>{{ number + 1 }}</div>
    <div>{{ flag ? 'ok': 'no' }}</div>
    <div>{{ message.split('').reverse().join('-') }}</div>
    <div v-bind:id="'list-' + id">111</div>
    <div v-bind:id=`list-${id}`>222</div>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>

  const { createApp } = Vue
  
  const app = createApp({
    data () {
      return {
        msg: 'hello vue',
        number: 100,
        flag: true,
        message: 'hello world',
        id: 100
      }
    }
  })

  app.mount('#app')
  
</script>
</html>
```



### 2.2 文本类指令

学习：v-text、v-html、v-pre

#### 2.2.1 v-html & v-text

双大括号将会将数据插值为纯文本，而不是 HTML。若想插入 HTML，你需要使用 [`v-html` 指令](https://cn.vuejs.org/api/built-in-directives.html#v-html)：

```
 <div v-html="rawHTML"></div>
 <div v-text="rawHTML"></div>
 <div>{{rawHTML}}</div>
```

> 在网站上动态渲染任意 HTML 是非常危险的，因为这非常容易造成 [XSS 漏洞](https://en.wikipedia.org/wiki/Cross-site_scripting)。请仅在内容安全可信时再使用 `v-html`，并且**永远不要**使用用户提供的 HTML 内容(script也属于HTML内容)。

#### 2.2.2 v-pre

元素内具有 `v-pre`，所有 Vue 模板语法都会被保留并按原样渲染。最常见的用例就是显示原始双大括号标签及内容。

```
 <div v-pre>{{ rawHTML }}</div>
```

完整案例: `07_text.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>07_文本类指令</title>
</head>
<body>
  <div id="app">
    <!-- 解析输出 -->
    <div v-html="msg"></div>
    <!-- 转义输出 -->
    <div v-text="msg"></div>
    <!-- 文本插值 -->
    <div>{{ msg }}</div>

    <!-- v-pre 含有该指令的标签内部的语法不会被vue解析 -->
    <div v-pre>{{ msg }}</div>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>

  const { createApp } = Vue
  
  const app = createApp({
    data () {
      return {
        msg: '<h1>hello vue</h1>'
      }
    }
  })

  app.mount('#app')
  
</script>
</html>
```

### 2.3 属性绑定

学习：v-bind 以及简写形式

双大括号不能在 HTML attributes 中使用。想要响应式地绑定一个 attribute，应该使用 [`v-bind` 指令](https://cn.vuejs.org/api/built-in-directives.html#v-bind)：

```
<div v-bind:id="myId"></div>
```

> 如果绑定的值是 `null` 或者 `undefined`，那么该 attribute 将会从渲染的元素上移除。

因为 `v-bind` 非常常用，提供了特定的简写语法：

```
<div :id="myId"></div>
```

> 开头为 `:` 的 attribute 可能和一般的 HTML attribute 看起来不太一样，但它的确是合法的 attribute 名称字符，并且所有支持 Vue 的浏览器都能正确解析它。

[布尔型 attribute](https://developer.mozilla.org/zh-CN/docs/Web/HTML/Attributes#布尔值属性) 依据 true / false 值来决定 attribute 是否应该存在于该元素上。[`disabled`](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/disabled) 就是最常见的例子之一。

```
<button :disabled="flag">Button</button>
```

当 `flag` 为[真值](https://developer.mozilla.org/en-US/docs/Glossary/Truthy)或一个空字符串 (即 `<button disabled="">`) 时，元素会包含这个 `disabled` attribute。而当其为其他[假值](https://developer.mozilla.org/en-US/docs/Glossary/Falsy)时 attribute 将被忽略。

如果你有像这样的一个包含多个 attribute 的 JavaScript 对象：

```js
data () {
  return {
	 obj: {
       a: 1,
	   b: 2
     }
  }
}
```

```
<div v-bind="obj"></div>
```

完整案例: `08_attribute.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>08_绑定属性以及缩写形式</title>
</head>
<body>
  <div id="app">
    <!-- 如果属性的值是变量，boolean类型，number类型，对象，数组，null，undefined，需要使用绑定属性 -->
    <!-- 使用绑定属性遇到 null，undefined，则该属性自动不显示  -->
    <div v-bind:msg="msg" v-bind:flag="true" v-bind:num="100" v-bind:obj="{a: 1, b: 2}" v-bind:arr="['a', 'b']" v-bind:nu="null" v-bind:un="undefined"></div>
    <div :msg="msg" :flag="true" :num="100" :obj="{a: 1, b: 2}" :arr="['a', 'b']" :nu="null" :un="undefined"></div>
    <!-- 注意隐式类型转换，非空即为真，确保时 使用绑定属性 -->
    <button :disabled="false">按钮</button>
    <!-- 如果属性的值需要拼接字符串完成，且含有变量，可以使用 绑定属性结合 字符串拼接以及模板字符串 实现  -->
    <div v-bind:id="'list-' + id">111</div>
    <div v-bind:id=`list-${id}`>222</div>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>

  const { createApp } = Vue
  const app = createApp({
    data () {
      return {
        msg: 'hello vue',
        id: 100
      }
    }
  })

  app.mount('#app')
  
</script>

</html>
```



> 如果一个属性的值是变量，boolean类型，number类型，对象，数组，null，undefined，使用绑定属性

### 2.4 事件绑定

学习：v-on以及简写形式，methods应用

我们可以使用 `v-on` 指令 (简写为 `@`) 来监听 DOM 事件，并在事件触发时执行对应的 JavaScript。用法：`v-on:click="methodName"` 或 `@click="handler"`。

事件处理器的值可以是：

1. **内联事件处理器**：事件被触发时执行的内联 JavaScript 语句 (与 `onclick` 类似)。
2. **方法事件处理器**：一个指向组件上定义的方法的属性名或是路径。

·内联事件处理器·通常用于简单场景，例如：

```
<button @click="count++">加 1</button>
<p>Count is: {{ count }}</p>
```

随着事件处理器的逻辑变得愈发复杂，内联代码方式变得不够灵活。因此 `v-on` 也可以接受一个方法名或对某个方法的调用。

`方法事件处理器`

```js
<button @click="greet">问候</button>
 data () {
   return {
      name: 'Vue.js'
   }
 },
 methods: {
   greet(event) {
     // 方法中的 `this` 指向当前活跃的组件实例
     alert(`Hello ${this.name}!`)
     // `event` 是 DOM 原生事件
     if (event) {
       alert(event.target.tagName)
     }
  }
}
```

除了直接绑定方法名，你还可以在内联事件处理器中调用方法。这允许我们向方法传入自定义参数以代替原生事件：

```
methods: {
  say(message) {
    alert(message)
  }
}
```

```
<button @click="say('hello')">说 hello</button>
<button @click="say('bye')">说 bye</button>
```

内联事件处理器中访问原生 DOM 事件。你可以向该处理器方法传入一个特殊的 `$event` 变量，或者使用内联箭头函数：

```
 <!-- 使用特殊的 $event 变量 -->
 <button @click="warn('表单不能提交.', $event)">提交</button>
 <!-- 使用内联箭头函数 -->
 <button @click="(event) => warn('表单不能提交.', event)">提交</button>
```

```
methods: {
  warn(message, event) {
    // 这里可以访问 DOM 原生事件
    if (event) {
      event.preventDefault()
    }
    alert(message)
  }
}
```

在处理事件时调用 `event.preventDefault()` 或 `event.stopPropagation()` 是很常见的。尽管我们可以直接在方法内调用，但如果方法能更专注于数据逻辑而不用去处理 DOM 事件的细节会更好。

为解决这一问题，Vue 为 `v-on` 提供了**事件修饰符**。

```html
<!-- 单击事件将停止传递 -->
<a @click.stop="doThis"></a>

<!-- 提交事件将不再重新加载页面 -->
<form @submit.prevent="onSubmit"></form>

<!-- 修饰语可以使用链式书写 -->
<a @click.stop.prevent="doThat"></a>

<!-- 也可以只有修饰符 -->
<form @submit.prevent></form>

<!-- 仅当 event.target 是元素本身时才会触发事件处理器 -->
<!-- 例如：事件处理器不来自子元素 -->
<div @click.self="doThat">...</div>

<!-- 添加事件监听器时，使用 `capture` 捕获模式 -->
<!-- 例如：指向内部元素的事件，在被内部元素处理前，先被外部处理 -->
<div @click.capture="doThis">...</div>

<!-- 点击事件最多被触发一次 -->
<a @click.once="doThis"></a>

<!-- 滚动事件的默认行为 (scrolling) 将立即发生而非等待 `onScroll` 完成 -->
<!-- 以防其中包含 `event.preventDefault()` -->
<div @scroll.passive="onScroll">...</div>
```

```html
 <!-- 事件修饰符 -->
 <!-- 阻止冒泡-原生 -->
 <div class="parent" @click="clickParent1">
 <div class="child" @click="clickChild1"></div>
 </div>
 <!-- 阻止冒泡-事件修饰符 -->
 <div class="parent" @click="clickParent2">
 <div class="child" @click.stop="clickChild2"></div>
 </div>
```

```js
methods: {
	clickParent1 (event) {
    console.log('parent1')
    },
    clickChild1 (event) {
    event.stopPropagation()
    console.log('child1')
    },
    clickParent2 (event) {
    console.log('parent2')
    },
    clickChild2 (event) {
    console.log('child2')
    },
}
```

```css
.parent {
    width: 200px;
    height: 200px;
    background: #f66;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 10px;
}
.child {
    width: 100px;
    height: 100px;
    border: 1px solid #ccc;
}
```

在监听键盘事件时，我们经常需要检查特定的按键。Vue 允许在 `v-on` 或 `@` 监听按键事件时`添加按键修饰符`。

```
 <!-- 按键修饰符 -->
 <!-- 原生 -->
 <input type="text" @keyup="print1">
 <!-- vue -->
 <input type="text" @keyup.enter="print2">
```

```js
methods: {
	print1 (event) {
        if (event.keyCode === 13) {
        	console.log('打印1')
        }
    },
    print2 () {
    	console.log('打印2')
    }
}
```

```
.enter
.tab
.delete (捕获“Delete”和“Backspace”两个按键)
.esc
.space
.up
.down
.left
.right
```

你可以使用以下`系统按键修饰符`来触发鼠标或键盘事件监听器，只有当按键被按下时才会触发

```
.ctrl
.alt
.shift
.meta
```

```
<!-- Alt + Enter -->
<input @keyup.alt.enter="clear" />
<!-- Ctrl + 点击 -->
<div @click.ctrl="doSomething">Do something</div>
```

完整案例: `09_event.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>09_绑定事件</title>
  <style>
    .parent {
      width: 200px;
      height: 200px;
      background-color: #f66;
      display: flex;
      justify-content: center;
      align-items: center;
    }
    .child {
      width: 100px;
      height: 100px;
      background-color: #ccc;
    }
  </style>
</head>
<body>
  <div id="app">
    <!-- 内联事件处理器 -->
    <button v-on:click="count++">加1</button> {{ count }}<br />
    <!-- 方法事件处理器 -->
    <button v-on:click="add">加1</button> {{ count }}<br />

    <button @click="say('hello', $event)">say hello</button>
    <button @click="say('bye', $event)">say bye</button>

    <!-- 事件对象 - 阻止事件冒泡，阻止默认事件 -->
    <form @submit="getUserInfo">
      <input type="text" name="userName" placeholder="用户名" />
      <input type="password" name="password" placeholder="密码" />
      <input type="submit" value="提交">
    </form>
    <div class="parent" @click="print('parent', $event)">
      <div class="child" @click="print('child', $event)"></div>
    </div>

    <!-- 事件修饰符 - 阻止事件冒泡，阻止默认事件  -->
    <form @submit.prevent="getUserInfoVue">
      <input type="text" name="userName" placeholder="用户名" />
      <input type="password" name="password" placeholder="密码" />
      <input type="submit" value="提交">
    </form>
    <div class="parent" @click="printVue('parent')">
      <div class="child" @click.stop="printVue('child')"></div>
    </div>

    <!-- 事件对象 -  回车时打印数据 -->
    <input type="text" @keyup="printData" />
    <!-- 按键修饰符 -  回车时打印数据 -->
    <input type="text" @keyup.enter="printDataVue" />
    <!-- vue2中可以 根据 keyCode 作为修饰符, vue3中不支持 -->
    <input type="text" @keyup.13="printDataVueCode" />

    <!-- 使用系统修饰符可以自定义组合按键 -->
    <!-- 用户点击 alt 加 enter时 清空输入框数据 -->
    <!-- v-model 属于表单的输入绑定 -->
    <input type="text" v-model="text" @keyup.alt.enter="clear"/> {{ text }}
    <div @click.ctrl="doSomething">Do something</div>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>

  const { createApp } = Vue
  const app = createApp({
    data () {
      return {
        count: 10,
        name: 'Vue.js',
        text: '1'
      }
    },
    methods: { // 所有自定义的vue的事件都应该写在 methods 中
      // 如果使用事件时，没有添加(),那么事件含有默认参数为 event
      add (event) {
        // this其实就是vue的实例
        // this.count++
        // this.count += 1
        this.count = this.count + 1
        console.log(this.name)
        console.log(event) // PointerEvent 与原生js中的事件对象保持一致（react中使用的不是原生js的事件对象）
      },
      // 如果使用事件时添加(),并且还想用事件对象，那么传递事件对象的vue的专属参数 $event
      say (msg, event) {
        console.log(msg, event)
      },
      getUserInfo (event) {
        event.preventDefault()
      },
      getUserInfoVue () {

      },
      print (msg, event) {
        event.stopPropagation()
        console.log(msg)
      },
      printVue (msg) {
        console.log(msg)
      },
      printData (event) {
        if (event.keyCode === 13) {
          console.log('1')
        }
      },
      printDataVue () {
        console.log(2)
      },
      printDataVueCode () {
        console.log(3)
      },
      clear () {
        this.text = ''
      },
      doSomething () {
        console.log('doSomething')
      }
    }
  })

  app.mount('#app')
  
</script>

</html>
```



### 2.5条件渲染

学习：v-if、v-else-if、v-else、v-show

`v-if` 指令用于条件性地渲染一块内容。这块内容只会在指令的表达式返回真值时才被渲染。

也可以使用 `v-else` 为 `v-if` 添加一个“else 区块”

一个 `v-else` 元素必须跟在一个 `v-if` 或者 `v-else-if` 元素后面，否则它将不会被识别。

```
<div v-if="grade >= 90">优</div>
<div v-else-if="grad" >= 80">良</div>
<div v-else-if="grade >= 70">中</div>
<div v-else-if="grade >= 60">差</div>
<div v-else>不及格</div>
```

```js
data () {
    return {
        grade: 66
    }
}
```

> `v-else` 和 `v-else-if` 也可以在 `<template>` 上使用。
>
> 想要切换不止一个元素,在这种情况下我们可以在一个 `<template>` 元素上使用 `v-if`，这只是一个不可见的包装器元素，最后渲染的结果并不会包含这个 `<template>` 元素。---- tempalte是一个空标签

另一个可以用来按条件显示一个元素的指令是 `v-show`。其用法基本一样：

不同之处在于 `v-show` 会在 DOM 渲染中保留该元素；`v-show` 仅切换了该元素上名为 `display` 的 CSS 属性。

> `v-show` 不支持在 `<template>` 元素上使用，也不能和 `v-else` 搭配使用。

```
<!-- v-show -->
<div v-show="grade >= 90 && grade < 100">优</div>
<div v-show="grade >= 80 && grade < 90">良</div>
<div v-show="grade >= 70 && grade < 80">中</div>
<div v-show="grade >= 60 && grade < 70">差</div>
<div v-show="grade < 60">不及格</div>
```



完整案例：`10_condition.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>10_条件渲染</title>
</head>
<body>
  <div id="app">
    <button @click="grade++"> 加</button> {{ grade }}<button @click="grade--">减</button>
    <!-- v-if v-else-if v-else -->
    <div v-if="grade >=90">优</div>
    <div v-else-if="grade >=80">良</div>
    <div v-else-if="grade >=70">中</div>
    <div v-else-if="grade >=60">差</div>
    <div v-else>不及格</div>

    <hr />
    <!-- v-show -->
    <div v-show="grade >= 90">优</div>
    <div v-show="grade >= 80 && grade < 90">良</div>
    <div v-show="grade >= 70 && grade < 80">中</div>
    <div v-show="grade >= 60 && grade < 70">差</div>
    <div v-show="grade < 60">不及格</div>

    <!-- 假如不同条件下需要同时控制多个元素的显示和不显示 -->
    <div v-if="flag">1</div>
    <div v-if="flag">2</div>
    <div v-if="flag">3</div>
    <!-- template 属于vue框架中的空标签，审查元素时不会被渲染出来 -->
    <template v-if="flag">
      <div>4</div>
      <div>5</div>
      <div>6</div>
    </template>

  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>

  const { createApp } = Vue
  const app = createApp({
    data () {
      return {
        grade: 61,
        flag: true
      }
    }
  })

  app.mount('#app')
  
</script>

</html>
```

> `v-if` vs `v-show`
>
> `v-if` 是“真实的”按条件渲染，因为它确保了在切换时，条件区块内的事件监听器和子组件都会被销毁与重建。
>
> `v-if` 也是**惰性**的：如果在初次渲染时条件值为 false，则不会做任何事。条件区块只有当条件首次变为 true 时才被渲染。
>
> 相比之下，`v-show` 简单许多，元素无论初始条件如何，始终会被渲染，只有 CSS `display` 属性会被切换。
>
> 总的来说，`v-if` 有更高的切换开销，而 `v-show` 有更高的初始渲染开销。因此，如果需要频繁切换，则使用 `v-show` 较好；如果在运行时绑定条件很少改变，则 `v-if` 会更合适。

### 2.6列表渲染

学习：v-for 以及key属性

我们可以使用 `v-for` 指令基于一个数组(对象、字符串)来渲染一个列表。`v-for` 指令的值需要使用 `item in items` 形式的特殊语法，其中 `items` 是源数据的数组，而 `item` 是迭代项的**别名**

```
data() {
  return {
    items: [{ message: 'Foo' }, { message: 'Bar' }]
  }
}

<li v-for="item in items">
  {{ item.message }}
</li>
```

在 `v-for` 块中可以完整地访问父作用域内的属性和变量。`v-for` 也支持使用可选的第二个参数表示当前项的位置索引。

```
data() {
  return {
    parentMessage: 'Parent',
    items: [{ message: 'Foo' }, { message: 'Bar' }]
  }
}

<li v-for="(item, index) in items">
  {{ parentMessage }} - {{ index }} - {{ item.message }}
</li>
```

列表渲染需要添加key值，对于多层嵌套的 `v-for`，作用域的工作方式和函数的作用域很类似

完整案例： `11_list.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>11_列表渲染</title>
</head>
<body>
  <div id="app">
    <!-- 列表渲染中的 key 需要使用列表数据的唯一值
      实在不得已情况下，可以使用 数组 的索引值作为key值
      v-for="(item, index) of list" :key="index"
    --4>
    <!-- 没有key -->
    <ul>
      <li v-for="item of arr">
        {{ item }}
      </li>
    </ul>
    <button @click="beforeArrAdd">arr前追加数据</button>
    <button @click="afterArrAdd">arr后追加数据</button>
    <!-- 添加key -->
    <ul>
      <li v-for="item of arr" :key="item">
        {{ item }}
      </li>
    </ul>
    <ul>
      <li v-for="item of items">
        {{ item.message }}
      </li>
    </ul>
    <button @click="getData">获取数据</button>
    <ul>
      <li v-for="item of list" :key="item.proid">
        {{ item.proname }}
      </li>
    </ul>
    <ul>
      <li v-for="item of carList">
        <div>{{ item.brand }}</div>
        <ol>
          <li v-for="itm in item.list">
            {{ itm }}
          </li>
        </ol>
      </li>
    </ul>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script src="lib/jquery.js"></script>
<script>

  const { createApp } = Vue
  const app = createApp({
    data () {
      return {
        arr: ['a', 'b', 'c', 'd'],
        items: [
          { message: 'Foo' },
          { message: 'Bar' }
        ],
        list: [],
        carList: [
          {
            brand: '宝马',
            list: ['X5', 'X6']
          },
          {
            brand: '雷克萨斯',
            list: ['XT5', 'AT6']
          }
        ]
      }
    },
    methods: {
      getData () {
        // http://121.89.205.189:3001/apidoc/#api-Pro-GetProList
        // 原生js ajax
        // jQuery ajax --- 不要再vue以及react中使用jQuery，因为 jQuery 大量的api都是基于DOM的
        // $.ajax({
        //   url: 'http://121.89.205.189:3001/api/pro/list',
        //   method: 'GET',
        //   success: res => { // ? 为什么要使用 箭头函数
        //     console.log(res)
        //     this.list = res.data
        //   }
        // })
        // fetch -- 需要先转换为 json 的格式，再使用
        fetch('http://121.89.205.189:3001/api/pro/list').then(res => res.json()).then(res=> {
          console.log(res)
          this.list = res.data
        })
      }
    },
    methods: {
      beforeArrAdd () {
        this.arr.unshift('e')
      },
      afterArrAdd () {
        this.arr.push('f')
      }
    }
  })

  app.mount('#app')
  
</script>

</html>
```

> v-if 与 v-for 同时存在于一个元素上，会发生什么？
>
> `12_vue3_if_for.html`
>
> ```html
> <!DOCTYPE html>
> <html lang="en">
> <head>
>   <meta charset="UTF-8">
>   <meta http-equiv="X-UA-Compatible" content="IE=edge">
>   <meta name="viewport" content="width=device-width, initial-scale=1.0">
>   <title>12_vue3的v-if与v-for优先级</title>
> </head>
> <body>
>   <div id="app">
>     <ul>
>       <li v-for="(item, index) of arr" :key="index" v-if="flag">
>         {{ item }}
>       </li>
>     </ul>
>   </div>
> </body>
> <script src="lib/vue.global.js"></script>
> <script>
>   Vue.createApp({
>     data () {
>       return {
>         arr: ['a', 'b', 'c', 'd'],
>         flag: false
>       }
>     }
>   }).mount('#app')
> </script>
> </html>
> ```
>
> `13_vue2_if_for.html`
>
> ```html
> <!DOCTYPE html>
> <html lang="en">
> <head>
>   <meta charset="UTF-8">
>   <meta http-equiv="X-UA-Compatible" content="IE=edge">
>   <meta name="viewport" content="width=device-width, initial-scale=1.0">
>   <title>13_vue2的v-if与v-for优先级</title>
> </head>
> <body>
>   <div id="app">
>     <ul>
>       <li v-for="(item, index) of arr" :key="index" v-if="flag">
>         {{ item }}
>       </li>
>     </ul>
>   </div>
> </body>
> <script src="lib/vue.js"></script>
> <script>
>   new Vue({
>     data: {
>       arr: ['a', 'b', 'c', 'd'],
>       flag: false
>     }
>   }).$mount('#app')
>   
> </script>
> </html>
> ```
>
> > 通过`审查元素`得知：
> >
> > vue3中，v-if的优先级高于v-for
> >
> > vue2中，v-for的优先级高于v-if

### 2.7 表单输入绑定

学习：v-model

在前端处理表单时，我们常常需要将表单输入框的内容同步给 JavaScript 中相应的变量

```
<input
  :value="text"
  @input="event => text = event.target.value">
```

`v-model` 指令帮我们简化了这一步骤：

```
<input v-model="text">
```

`v-model` 还可以用于各种不同类型的输入，`<textarea>`、`<select>` 元素。它会根据所使用的元素自动使用对应的 DOM 属性和事件组合：

> - 文本类型的 `<input>` 和 `<textarea>` 元素会绑定 `value` property 并侦听 `input` 事件；
> - `<input type="checkbox">` 和 `<input type="radio">` 会绑定 `checked` property 并侦听 `change` 事件；
> - `<select>` 会绑定 `value` property 并侦听 `change` 事件 

> `v-model` 会忽略任何表单元素上初始的 `value`、`checked` 或 `selected` attribute。它将始终将当前绑定的 JavaScript 状态视为数据的正确来源。你应该在 JavaScript 中使用`data` 选项来声明该初始值。

完整案例： `14_model.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>14_表单输入绑定</title>
</head>
<body>
  <div id="app">
    <div>
      用户名: <input type="text" v-model="userName" /> {{ userName }}
    </div>
    <div>
      密  码: <input type="password" v-model="password" /> {{ password }}
    </div>
    <div>
      性  别: <input type="radio" name="sex" v-model="sex" value="男"/>男<input type="radio" name="sex" v-model="sex" value="女"/>女 --- {{ sex }}
    </div>
    <div>
      阶  段: <select v-model="lesson">
        <option disabled value="">请选择</option>
        <option :value="1">一阶段</option>
        <option :value="2">二阶段</option>
        <option :value="3">三阶段</option>
      </select> ---- {{ lesson === 1 ? '一阶段' : lesson === 2 ? '二阶段' : '三阶段' }}
    </div>
    <div>
      爱  好:
      <input type="checkbox" name="hobby" value="🏀" v-model="hobby"/>🏀
      <input type="checkbox" name="hobby" value="⚽" v-model="hobby"/>⚽
      <input type="checkbox" name="hobby" value="🏓" v-model="hobby"/>🏓
      <input type="checkbox" name="hobby" value="⚾" v-model="hobby"/>⚾
      <input type="checkbox" name="hobby" value="🌏" v-model="hobby"/>🌏
    </div> ---- {{ hobby }}
    <div>
      备  注: <textarea v-model="note"></textarea> {{ note }}
    </div>
    <div>
      <input type="checkbox" v-model="flag"/> 同意*******协议 --- {{ flag }}
    </div>
    <div>
      <input type="button" value="提交" @click="submit" /> 
    </div>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  Vue.createApp({
    data () {
      return {
        userName: '',
        password: '',
        sex: '男',
        lesson: '',
        note: '',
        hobby: ['🌏'], // 表示多选-数组
        flag: false // 表示开关 - boolean
      }
    },
    methods: {
      submit () {
        if (this.flag) {
          console.log({
            userName: this.userName,
            password: this.password,
            sex: this.sex,
            hobby: this.hobby,
            lesson: this.lesson,
            note: this.note
          })
        } else {
          alert('请先勾选用户协议')
        }
      }
    }
  }).mount('#app')
</script>
</html>
```

> 如果 `v-model` 表达式的初始值不匹配任何一个选择项，`<select>` 元素会渲染成一个“未选择”的状态。在 iOS 上，这将导致用户无法选择第一项，因为 iOS 在这种情况下不会触发一个 change 事件。因此，我们建议提供一个空值的禁用选项

> 修饰符
>
> .lazy   input --- change
>
> .number --- 输出的为数字
>
> .trim ---- 去除两端的空格

### 2.8 类与样式绑定

数据绑定的一个常见需求场景是操纵元素的 CSS class 列表和内联样式。因为 `class` 和 `style` 都是 attribute，我们可以和其他 attribute 一样使用 `v-bind` 将它们和动态的字符串绑定。但是，在处理比较复杂的绑定时，通过拼接生成字符串是麻烦且易出错的。因此，Vue 专门为 `class` 和 `style` 的 `v-bind` 用法提供了特殊的功能增强。除了字符串外，表达式的值也可以是对象或数组。

> 不管是类class还是样式style，都有对象和数组的写法

完整案例： `15_style.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>15_类与样式绑定</title>
  <style>
    .a {
      font-size: 30px;
    }
    .b {
      color: #f66;
    }
  </style>
</head>
<body>
  <div id="app">
    <input type="checkbox" v-model="flag" />
    <div class="a b">群组选择器</div>
    <!-- 在flag字段为真时，才显示红色的30px的字体 -->
    <!-- class对象写法,key为定义的样式，value为条件 -->
    <div :class="{ a: flag, b: flag }">class的对象写法</div>
    <!-- class数组写法 -->
    <div :class="[{a: flag}, {b: flag}]">class数组写法</div>
    <!-- 三元运算符 -->
    <div :class="flag ? 'a b' : ''">三元运算符 class写法</div>

    <!-- style对象写法,小驼峰式以及短横线连接方式  -->
    <div :style="{ color: color, fontSize: fontSize }">style对象写法</div>
    <!-- style数组写法 -->
    <div :style="[{color: color}, {fontSize: fontSize}]">style数组写法</div>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  Vue.createApp({
    data () {
      return {
        flag: false,
        color: '#00f',
        fontSize: '50px'
      }
    }
  }).mount('#app')
</script>
</html>
```

## 3.生命周期

学习：常见的8个生命周期钩子函数

### 3.1 vue2生命周期

每个 Vue 实例在被创建之前都要经过一系列的初始化过程。例如需要设置数据监听、编译模板、挂载实例到 DOM，在数据变化时更新 DOM 等。同时在这个过程中也会运行一些叫做**生命周期钩子**的函数，目的是给予用户在一些特定的场景下添加他们自己代码的机会。

Vue生命周期的**主要阶段**：4个before， 4个ed，创建，装载，更新，销毁

- 挂载（初始化相关属性）

  - beforeCreate ---- 备孕

    **注意点**：在此时不能获取data中的数据，也就是说 this.msg 得到的是

  - created ---- 怀上了

  - beforeMount ---- 怀胎十月

  - mounted【页面加载完毕的时候就是此时】 ---- 生下来了

    **注意点**：默认情况下，在组件的生命周期中只会触发一次

- 更新（元素或组件的变更操作）

  - beforeUpdate

  - updated

    **注意点**：可以重复触发的

- 销毁（销毁相关属性）

  - beforeDestroy --- game over前
  - destroyed --- game over

> 销毁（手动）使用 this.$destroy()

关于8个生命周期涉及到的方法，可以参考Vue官网API：

![Vue 实例生命周期](assets/lifecycle.png)

### 3.2 vue3生命周期

选项式API中将 beforeDestroy 以及 destroyed  修改为 beforeUnmount 和 unmounted，其余一致

https://cn.vuejs.org/guide/essentials/lifecycle.html#lifecycle-diagram

![组件生命周期图示](assets/lifecycle.16e4c08e.png)

如果是vue2的生命周期钩子函数

完整案例： `16_lifeCycle_vue2.html` [官方解释](https://v2.cn.vuejs.org/v2/api/#beforeCreate)

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>13_vue2的v-if与v-for优先级</title>
</head>
<body>
  <div id="app">
    {{ count }}
    <button @click="add">加1</button>
  </div>
</body>
<script src="lib/vue.js"></script>
<script>
  new Vue({
    data: {
      count: 10
    },
    methods: {
      add () {
        this.count++
        if (this.count === 15) {
          this.$destroy()
        }
      }
    },
    beforeCreate () {
      console.log('beforeCreate')
    },
    created () {
      // 有的人在此处请求数据，修改状态 ---- 不太建议 （请求数据-教育 - 胎教）
      console.log('created')
    },
    beforeMount () {
      console.log('beforeMount')
    },
    mounted () {
      // 在此处请求数据 - （请求数据-教育 - 早教）
      // DOM操作 （揍）
      // 实例化 new Swiper('.container', {})
      // 计时器，延时器等 （年龄从生下来才开始计算）
      // 订阅
      console.log('mounted')
    },
    beforeUpdate () {
      console.log('beforeUpdate')
    },
    updated () {
      // DOM操作 
      // 实例化
      // 不要请求数据 - 死循环
      console.log('updated')
    },
    beforeDestroy () {
      // 消除定时器，延时器，取消订阅
      console.log('beforeDestroy')
    },
    destroyed () {
      console.log('destroyed')
    }
  }).$mount('#app')
  
</script>
</html>
```

`15_lifeCycle_vue3.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>17_vue3生命周期</title>
</head>
<body>
  <div id="app">
    {{ count }}
    <button @click="add">加1</button>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const app = Vue.createApp({
    data () {
      return {
        count: 10
      }
    },
    methods: {
      add () {
        this.count++
        if (this.count === 15) {
          app.unmount()
        }
      }
    },
    beforeCreate () {
      console.log('beforeCreate')
    },
    created () {
      // 有的人在此处请求数据，修改状态 ---- 不太建议 （请求数据-教育 - 胎教）
      console.log('created')
    },
    beforeMount () {
      console.log('beforeMount')
    },
    mounted () {
      // 在此处请求数据 - （请求数据-教育 - 早教）
      // DOM操作 （揍）
      // 实例化 new Swiper('.container', {})
      // 计时器，延时器等 （年龄从生下来才开始计算）
      // 订阅
      console.log('mounted')
    },
    beforeUpdate () {
      console.log('beforeUpdate')
    },
    updated () {
      // DOM操作 
      // 实例化
      // 不要请求数据 - 死循环
      console.log('updated')
    },
    beforeUnmount () {
      // 消除定时器，延时器，取消订阅
      console.log('beforeUnmount')
    },
    unmounted () {
      console.log('unmounted')
    }
  })
  
  app.mount('#app')
</script>
</html>
```



## 4.响应式

### 4.1 响应式基础

学习：状态选项data，$data

选用选项式 API 时，会用 `data` 选项来声明组件的响应式状态。此选项的值应为返回一个对象的函数。Vue 将在创建新组件实例的时候调用此函数，并将函数返回的对象用响应式系统进行包装。此对象的所有顶层属性都会被代理到组件实例 (即方法和生命周期钩子中的 `this`) 上。

Vue 在组件实例上暴露的内置 API 使用 `$` 作为前缀。它同时也为内部属性保留 `_` 前缀。因此，你应该避免在顶层 `data` 上使用任何以这些字符作前缀的属性。

从 [`data`](https://cn.vuejs.org/api/options-state.html#data) 选项函数中返回的对象，会被组件赋为响应式。组件实例将会代理对其数据对象的属性访问。

完整案例`18_data.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>vue3解决dom问题</title>
  <script src="lib/vue.global.js"></script>
</head>
<body>
  <div id="app">
    {{ count }}
  </div>
</body>
<script>
  const { createApp } = window.Vue
  const app = createApp({
    data () {
      return {
        count: 100
      }
    },
    created () {
      console.log(this)
      console.log(this.count) // 100
      this.count = 200
      console.log(this.$data.count) // 100 // 200
      this.$data.count = 300
      console.log(this._.data.count) // 100 // 300
      this._.data.count = 400

    }
  })

  // 挂载应用
  app.mount('#app')

</script>
</html>
```

### 4.2 计算属性

学习computed

模板中的表达式虽然方便，但也只能用来做简单的操作。如果在模板中写太多逻辑，会让模板变得臃肿，难以维护

推荐使用**计算属性**来描述依赖响应式状态的复杂逻辑

计算属性是基于它们的响应式**依赖**进行**缓存**的，计算属性比较适合对多个变量或者对象进行处理后返回一个结果值，也就是说多个变量中的某一个值发生了变化则我们监控的这个值也就会发生变化。

计算属性定义在Vue对象中，通过关键词 **computed** 属性对象中定义一个个函数，并返回一个值，使用计算属性时和 data 中的数据使用方式一致。

##### 4.2.1 一般计算属性以及方法对比

完整案例：`19_computed.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>vue3解决dom问题</title>
  <script src="lib/vue.global.js"></script>
</head>
<body>
  <div id="app">
    <!-- js表达式 -->
    {{ msg.split('').reverse().join('') }} - {{ msg.split('').reverse().join('') }} - {{ msg.split('').reverse().join('') }}
    <!-- 方法 -->
    {{ reverseMsgFn() }} -  {{ reverseMsgFn() }} - {{ reverseMsgFn() }} 
    <!-- 计算属性 -->
    {{ reverseMsg }} - {{ reverseMsg }} -  {{ reverseMsg }}
  </div>
</body>
<script>
  const { createApp } = window.Vue
  const app = createApp({
    data () {
      return {
        msg: 'hello vue'
      }
    },
    methods: {
      reverseMsgFn () {
        console.log(1)
        return this.msg.split('').reverse().join('')
      }
    },
    computed: {
      reverseMsg () {
        console.log(2)
        return this.msg.split('').reverse().join('')
      }
    }
    
  })

  // 挂载应用
  app.mount('#app')

</script>
</html>
```

> 计算属性具有依赖性，只有当依赖的值发生改变，才会重新计算
>
> 同等条件下，计算属性优于 方法 以及 js表达式。

##### 4.2.2 可写计算属性

计算属性默认仅能通过计算函数得出结果。当你尝试修改一个计算属性时，你会收到一个运行时警告。只在某些特殊场景中你可能才需要用到“可写”的属性，你可以通过同时提供 getter 和 setter 来创建：

完整案例：`20_computed.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>20_计算属性 setter</title>
  <script src="lib/vue.global.js"></script>
</head>
<body>
  <div id="app">
    <input v-model="firstName" />   +  <input v-model="lastName" /> = {{ fullName }}
    <button @click="reset">重置</button>
  </div>
</body>
<script>
  const { createApp } = window.Vue
  const app = createApp({
    data () {
      return {
        firstName: 'John',
        lastName: 'Doe'
      }
    },
    computed: {
      // fullName () {
      //   return this.firstName + ' ' + this.lastName
      // }
      fullName: {
        get () {
          return this.firstName + ' ' + this.lastName
        },
        set (newValue) {
          // 注意：我们这里使用的是解构赋值语法
          // [this.firstName, this.lastName] = newValue.split(' ')
          this.firstName = newValue.split(' ')[0]
          this.lastName = newValue.split(' ')[1]
        }
      }
    },
    methods: {
      reset () {
        console.log('111')
        // this.firstName = ''
        // this.lastName = ''
        this.fullName = 'wu daxun'
      }
    },

  })



  // 挂载应用
  app.mount('#app')

</script>
</html>
```



### 4.3 侦听器

学习：watch以及实例方法$watch

使用watch来侦听data中数据的变化，watch中的属性一定是data 中已经存在的数据。

> watch 只能监听data中的数据变化吗？
>
> watch可以监听路由中的数据的变化

**使用场景：**数据变化时执行异步或开销比较大的操作。

![img](assets/20.png)

完整案例：`21_watch.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>21_侦听属性</title>
  <script src="lib/vue.global.js"></script>
</head>
<body>
  <div id="app">
    <input type="text" v-model="firstName" /> + <input type="text" v-model="lastName" /> = {{ fullName }}
  </div>
</body>
<script>
  const { createApp } = window.Vue
  const app = createApp({
    data () {
      return {
        firstName: '',
        lastName: '',
        fullName: ''
      }
    },
    watch: {
      firstName (newVal, oldVal) {
        this.fullName = newVal + this.lastName
      },
      lastName (newVal, oldVal) {
        this.fullName = this.firstName + newVal
      }
    }

  })



  // 挂载应用
  app.mount('#app')

</script>
</html>
```

使用计算属性可以简化

完整案例：`22_computed.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>22_计算属性PK侦听属性</title>
  <script src="lib/vue.global.js"></script>
</head>
<body>
  <div id="app">
    <input type="text" v-model="firstName" /> + <input type="text" v-model="lastName" /> = {{ fullName }}
  </div>
</body>
<script>
  const { createApp } = window.Vue
  const app = createApp({
    data () {
      return {
        firstName: '',
        lastName: ''
      }
    },
    computed: {
      fullName () {
        return this.firstName + this.lastName
      }
    }
  })

  // 挂载应用
  app.mount('#app')

</script>
</html>
```

> 如何监听一个对象下的属性的变化？
>
> `watch` 默认是浅层的：被侦听的属性，仅在被赋新值时，才会触发回调函数——而嵌套属性的变化不会触发。如果想侦听所有嵌套的变更，你需要深层侦听器：
>
> 如果一开始就需要监听数据，建议直接在options Api中添加 watch选项
>
> 如果在达到某一个条件下再开启监听，需要使用 this.$watch()手动添加侦听器
>
> 如果不使用深度侦听，如何监听对象下的属性的变化，可以通过 监听 `对象.属性`的变化（vue2 + vue3）,注意this指向
>
> 完整案例：`23_deep_watch.html`
>
> ```html
> <!DOCTYPE html>
> <html lang="en">
> <head>
>     <meta charset="UTF-8">
>     <meta http-equiv="X-UA-Compatible" content="IE=edge">
>     <meta name="viewport" content="width=device-width, initial-scale=1.0">
>     <title>23_深度侦听</title>
>     <script src="lib/vue.global.js"></script>
> </head>
> <body>
>     <div id="app">
>        <input type="text" v-model="user.firstName" /> + <input type="text" v-model="user.lastName" /> = {{ user.fullName }} | {{ full }}
>        <hr />
>        <button @click="count++">加1</button>{{ count }}
>        <button @click="startWatch">开始监听</button>
>        <button @click="stopWatch">停止监听</button>
>     </div>
> </body>
> <script>
>     const { createApp } = Vue
>     const app = createApp({
>        data () {
>          return {
>            user: {
>              firstName: '1',
>              lastName: '2',
>              fullName: ''
>            },
>            count: 100,
>            unwatch: null
>          }
>        },
>        computed: { // 计算属性更优
>          full () {
>            return this.user.firstName + this.user.lastName
>          }
>        },
>        watch: {
>          // 监听失败
>          // user (newVal, oldVal) {
>          //   this.user.fullName = newVal.firstName + newVal.lastName
>          // }
>          // 深度侦听
>          // user: {
>          //   deep: true,
>          //   handler (newVal, oldVal) {
>          //     this.user.fullName = newVal.firstName + newVal.lastName
>          //   },
>          //   // 强制立即执行回调 --- 自动执行一次监听数据
>          //   immediate: true,
>          //   // 默认情况下，用户创建的侦听器回调，都会在 Vue 组件更新之前被调用。
>          //   // 这意味着你在侦听器回调中访问的 DOM 将是被 Vue 更新之前的状态。
>          //   // 在侦听器回调中能访问被 Vue 更新之后的DOM，你需要指明 flush: 'post' 选项
>          //   flush: 'post' // vue3中新增的
>          // }
>          // 以下的写法同样适用于 Vue2
>          'user.firstName': function (newVal, oldVal)  {
>            this.user.fullName = newVal + this.user.lastName
>          },
>          'user.lastName': function(newVal, oldVal)  {
>            this.user.fullName = this.user.firstName + newVal
>          }
>        },
>        methods: {
>          startWatch () {
>            // 开始监听  赋值给一个函数，用于停止监听
>            this.unwatch = this.$watch('count', (newVal, oldVal) => {
>              console.log(newVal, oldVal)
>            })
>          },
>          stopWatch () {
>            this.unwatch() // 停止监听
>          }
>        }
>     })
> 
>     // 挂载应用
>     app.mount('#app')
> 
> </script>
> </html>
> ```
>
> 

### 4.4 深入响应式系统

学习：renderTracked 以及 renderTraggered

https://cn.vuejs.org/guide/extras/reactivity-in-depth.html

Vue 最标志性的功能就是其低侵入性的响应式系统。组件状态都是由响应式的 JavaScript 对象组成的。当更改它们时，视图会随即自动更新。这让状态管理更加简单直观，但理解它是如何工作的也是很重要的，这可以帮助我们避免一些常见的陷阱。

#### 4.4.1 什么是响应性

这个术语在今天的各种编程讨论中经常出现，但人们说它的时候究竟是想表达什么意思呢？本质上，响应性是一种可以使我们声明式地处理变化的编程范式。一个经常被拿来当作典型例子的用例即是 Excel 表格：

![image-20220915004948125](assets/image-20220915004948125.png)

这里单元格 A2 中的值是通过公式 `= A0 + A1` 来定义的 (你可以在 A2 上点击来查看或编辑该公式)，因此最终得到的值为 3，正如所料。但如果你试着更改 A0 或 A1，你会注意到 A2 也随即自动更新了。

而 JavaScript 默认并不是这样的。如果我们用 JavaScript 写类似的逻辑：

```
let A0 = 1
let A1 = 2
let A2 = A0 + A1

console.log(A2) // 3

A0 = 2
console.log(A2) // 仍然是 3
```

当我们更改 `A0` 后，`A2` 不会自动更新。

那么我们如何在 JavaScript 中做到这一点呢？首先，为了能重新运行计算的代码来更新 `A2`，我们需要将其包装为一个函数：

```
let A2
function update() {
  A2 = A0 + A1
}
```

然后，我们需要定义几个术语：

- 这个 `update()` 函数会产生一个**副作用**，或者就简称为**作用** (effect)，因为它会更改程序里的状态。
- `A0` 和 `A1` 被视为这个作用的**依赖** (dependency)，因为它们的值被用来执行这个作用。因此这次作用也可以说是一个它依赖的**订阅者** (subscriber)。

我们需要一个魔法函数，能够在 `A0` 或 `A1` (这两个**依赖**) 变化时调用 `update()` (产生**作用**)。

```
whenDepsChange(update)
```

这个 `whenDepsChange()` 函数有如下的任务：

1. 当一个变量被读取时进行追踪。例如我们执行了表达式 `A0 + A1` 的计算，则 `A0` 和 `A1` 都被读取到了。
2. 如果一个变量在当前运行的副作用中被读取了，就将该副作用设为此变量的一个订阅者。例如由于 `A0` 和 `A1` 在 `update()` 执行时被访问到了，则 `update()` 需要在第一次调用之后成为 `A0` 和 `A1` 的订阅者。
3. 探测一个变量的变化。例如当我们给 `A0` 赋了一个新的值后，应该通知其所有订阅了的副作用重新执行。

#### 4.4.2 Vue 中的响应性是如何工作的

后续单独讲解，刨析vue2的响应式和vue3的响应式的区别以及实现原理

## 5.组件化

### 5.1 什么是组件化？理解组件化

组件允许我们将 UI 划分为独立的、可重用的部分，并且可以对每个部分进行单独的思考。在实际应用中，组件常常被组织成层层嵌套的树状结构：

![组件树](assets/components.7fbb3771.png)

这和我们嵌套 HTML 元素的方式类似，Vue 实现了自己的组件模型，使我们可以在每个组件内封装自定义内容与逻辑。

### 5.2 如何封装一个vue组件

目标：理解一般思路即可

一个 Vue 组件在使用前需要先被“注册”，这样 Vue 才能在渲染模板时找到其对应的实现。

* 先构建组件的模板
* 定义组件
* 注册组件
* 使用组件

### 5.3 vue3组件注册和使用

学习：app.component()、components选项、template选项

组件注册有两种方式：全局注册和局部注册。

#### 5.3.1 全局注册组件

我们可以使用 [Vue 应用实例](https://cn.vuejs.org/guide/essentials/application.html)的 `app.component()` 方法，让组件在当前 Vue 应用中全局可用

```js
import { createApp } from 'vue'

const app = createApp({})

app.component(
  // 注册的名字
  'MyComponent',
  // 组件的实现
  {
    /* ... */
  }
)
```

`app.component()` 方法可以被链式调用：

```
app
  .component('ComponentA', ComponentA)
  .component('ComponentB', ComponentB)
  .component('ComponentC', ComponentC)
```

全局注册的组件可以在此应用的任意组件的模板中使用

```
<!-- 这在当前应用的任意组件中都可用 -->
<ComponentA/>
<ComponentB/>
<ComponentC/>
```

所有的子组件也可以使用全局注册的组件，这意味着这三个组件也都可以在*彼此内部*使用

完整案例：`24_component_vue3.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>24_全局注册组件</title>
</head>
<body>
  <div id="app">
    <!-- 04 使用组件 大驼峰式 短横线式  在html文件中只能使用 短横线式  -->
    <!-- 为了方便，Vue 支持将模板中使用 kebab-case 的标签解析为使用 PascalCase 注册的组件。
      这意味着一个以 `MyComponent` 为名注册的组件，在模板中可以通过 `<MyComponent>` 或 `<my-component>` 引用。
        这让我们能够使用同样的 JavaScript 组件注册代码来配合不同来源的模板。 -->
    <my-header></my-header>
    <!-- <MyHeader></MyHeader> -->
  </div>
</body>
<!-- 01 定义组件的模板 -->
<template id="header">
  <header>头部-{{msg}} - {{ reverseMsg }}</header>
</template>
<script src="lib/vue.global.js"></script>
<script>
  // 02.定义组件 - 首字母大写
  const Header = {
    template: '#header', // 绑定页面的模板 --- 必不可少
    // 可以写任意的属于vue的选项
    data () {
      return {
        msg: 'hello header'
      }
    },
    computed: {
      reverseMsg () {
        return this.msg.split('').reverse().join('')
      }
    }
  }
  

  const { createApp } = Vue

  const app = createApp({
  })
  // 03.全局注册组件 --- app.mount('#app') 之前
  // app.component('MyHeader', Header) // 大驼峰式
  app.component('my-header', Header) // 短横线式

  app.mount('#app')
</script>
</html>
```

vue2全局注册组件

完整案例:`25_component_vue2.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>25_vue2全局注册组件</title>
</head>
<body>
  <div id="app">
    <!-- 04 使用组件 大驼峰式 短横线式  在html文件中只能使用 短横线式  -->
    <!-- 为了方便，Vue 支持将模板中使用 kebab-case 的标签解析为使用 PascalCase 注册的组件。
      这意味着一个以 `MyComponent` 为名注册的组件，在模板中可以通过 `<MyComponent>` 或 `<my-component>` 引用。
        这让我们能够使用同样的 JavaScript 组件注册代码来配合不同来源的模板。 -->
    <my-header></my-header>
    <!-- <MyHeader></MyHeader> -->
  </div>
</body>
<!-- 01 定义组件的模板 -->
<template id="header">
  <header>头部-{{msg}} - {{ reverseMsg }}</header>
</template>
<script src="lib/vue.js"></script>
<script>
  // 02.定义组件 - 首字母大写
  const Header = {
    template: '#header', // 绑定页面的模板 --- 必不可少
    // 可以写任意的属于vue的选项
    data () { // vue2中的所有的组件的 data 必须是函数
      return {
        msg: 'hello header'
      }
    },
    computed: {
      reverseMsg () {
        return this.msg.split('').reverse().join('')
      }
    }
  }
  // 03.全局注册组件 --- new Vue 实例 之前
  // Vue.component('MyHeader', Header) // 大驼峰式
  Vue.component('my-header', Header) // 短横线式

  new Vue({}).$mount('#app')
</script>
</html>
```



#### 5.3.2 局部注册组件

全局注册虽然很方便，但有以下几个问题：

1. 全局注册，但并没有被使用的组件无法在生产打包时被自动移除 (也叫“tree-shaking”)。如果你全局注册了一个组件，即使它并没有被实际使用，它仍然会出现在打包后的 JS 文件中。
2. 全局注册在大型项目中使项目的依赖关系变得不那么明确。在父组件中使用子组件时，不太容易定位子组件的实现。和使用过多的全局变量一样，这可能会影响应用长期的可维护性。

相比之下，局部注册的组件需要在使用它的父组件中显式导入，并且只能在该父组件中使用。它的优点是使组件之间的依赖关系更加明确，并且对 tree-shaking 更加友好。

局部注册需要使用 `components` 选项

对于每个 `components` 对象里的属性，它们的 key 名就是注册的组件名，而值就是相应组件的实现。

完整案例:`26_components_vue3.html`vue3局部注册组件

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>26_vue3 局部注册组件</title>
</head>
<body>
  <div id="app">
    <!-- 04 使用组件 大驼峰式 短横线式  在html文件中只能使用 短横线式  -->
    <!-- 为了方便，Vue 支持将模板中使用 kebab-case 的标签解析为使用 PascalCase 注册的组件。
      这意味着一个以 `MyComponent` 为名注册的组件，在模板中可以通过 `<MyComponent>` 或 `<my-component>` 引用。
        这让我们能够使用同样的 JavaScript 组件注册代码来配合不同来源的模板。 -->
    <my-header></my-header>
    <!-- <MyHeader></MyHeader> -->
  </div>
</body>
<!-- 01 定义组件的模板 -->
<template id="header">
  <header>头部-{{msg}} - {{ reverseMsg }}</header>
</template>
<script src="lib/vue.global.js"></script>
<script>
  // 02.定义组件 - 首字母大写
  const Header = {
    template: '#header', // 绑定页面的模板 --- 必不可少
    // 可以写任意的属于vue的选项
    data () {
      return {
        msg: 'hello header'
      }
    },
    computed: {
      reverseMsg () {
        return this.msg.split('').reverse().join('')
      }
    }
  }
  

  const { createApp } = Vue

  const app = createApp({
    components: { // 03 局部注册组件
      // 'my-header': Header
      MyHeader: Header
    }
  })
  

  app.mount('#app')
</script>
</html>
```

完整案例:`27_components_vue2.html`vue2局部注册组件

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>27_vue2局部注册组件</title>
</head>
<body>
  <div id="app">
    <!-- 04 使用组件 大驼峰式 短横线式  在html文件中只能使用 短横线式  -->
    <!-- 为了方便，Vue 支持将模板中使用 kebab-case 的标签解析为使用 PascalCase 注册的组件。
      这意味着一个以 `MyComponent` 为名注册的组件，在模板中可以通过 `<MyComponent>` 或 `<my-component>` 引用。
        这让我们能够使用同样的 JavaScript 组件注册代码来配合不同来源的模板。 -->
    <my-header></my-header>
    <!-- <MyHeader></MyHeader> -->
  </div>
</body>
<!-- 01 定义组件的模板 -->
<template id="header">
  <header>头部-{{msg}} - {{ reverseMsg }}</header>
</template>
<script src="lib/vue.js"></script>
<script>
  // 02.定义组件 - 首字母大写
  const Header = {
    template: '#header', // 绑定页面的模板 --- 必不可少
    // 可以写任意的属于vue的选项
    data () { // vue2中的所有的组件的 data 必须是函数
      return {
        msg: 'hello header'
      }
    },
    computed: {
      reverseMsg () {
        return this.msg.split('').reverse().join('')
      }
    }
  }

  new Vue({
    components: { // 03 局部注册组件
      // MyHeader: Header
      'my-header': Header
    }
  }).$mount('#app')
</script>
</html>
```



> **局部注册的组件在后代组件中并\*不\*可用**

#### 5.3.3 组件使用注意事项

以上案例使用 PascalCase 作为组件名的注册格式，这是因为：

1. PascalCase 是合法的 JavaScript 标识符。这使得在 JavaScript 中导入和注册组件都很容易，同时 IDE 也能提供较好的自动补全。
2. `<PascalCase />` 在模板中更明显地表明了这是一个 Vue 组件，而不是原生 HTML 元素。同时也能够将 Vue 组件和自定义元素 (web components) 区分开来

在单文件组件和内联字符串模板中，我们都推荐这样做。但是，PascalCase 的标签名在 DOM 模板中是不可用的

为了方便，Vue 支持将模板中使用 kebab-case 的标签解析为使用 PascalCase 注册的组件。这意味着一个以 `MyComponent` 为名注册的组件，在模板中可以通过 `<MyComponent>` 或 `<my-component>` 引用。这让我们能够使用同样的 JavaScript 组件注册代码来配合不同来源的模板。

## 6.组件间通信

组件有 分治 的特点，每个组件之间具有一定的独立性，但是在实际工作中使用组件的时候有互相之间传递数据的需求，此时就得考虑如何进行 组件间传值 的问题了。

完整案例:`28_parent_child_component.html`父子组件

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>28_父子组件</title>
</head>
<body>
  <div id="app">
    <my-parent></my-parent>
  </div>
</body>
<template id="parent">
  <div>
    我是父组件
    <my-child></my-child>
  </div>
</template>
<template id="child">
  <div>
    我是子组件
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const Child = {
    template: '#child'
  }
  const Parent = {
    template: '#parent',
    components: {
      MyChild: Child
    }
  }

  const { createApp } = Vue

  const app = createApp({
    components: {
      MyParent: Parent
    }
  })

  app.mount('#app')

</script>
</html>
```

### 6.1 Prop

学习：状态选项props 以及 实例属性 $attrs

一个组件需要显式声明它所接受的 props，这样 Vue 才能知道外部传入的哪些是 props，哪些是透传 attribute

props 需要使用 [`props`](https://cn.vuejs.org/api/options-state.html#props) 选项来定义：

```js
{
  props: ['foo'],
  created() {
    // props 会暴露到 `this` 上
    console.log(this.foo)
  }
}
```

除了使用字符串数组来声明 prop 外，还可以使用对象的形式：

```js
{
  props: {
    title: String,
    likes: Number
  }
}
```

对于以对象形式声明中的每个属性，key 是 prop 的名称，而值则是该 prop 预期类型的构造函数。比如，如果要求一个 prop 的值是 `number` 类型，则可使用 `Number` 构造函数作为其声明的值。

> 如果一个 prop 的名字很长，应使用 camelCase 形式，因为它们是合法的 JavaScript 标识符，可以直接在模板的表达式中使用，也可以避免在作为属性 key 名时必须加上引号。
>
> 虽然理论上你也可以在向子组件传递 props 时使用 camelCase 形式 (使用 [DOM 模板](https://cn.vuejs.org/guide/essentials/component-basics.html#dom-template-parsing-caveats)时例外)，但实际上为了和 HTML attribute 对齐，我们通常会将其写为 kebab-case 形式
>
> `<my-com :likeNum="100"></my-com>` ===> `<my-com :like-num="100"></my-com>`
>
> 对于组件名我们推荐使用 [PascalCase](https://cn.vuejs.org/guide/components/registration.html#component-name-casing)，因为这提高了模板的可读性，能帮助我们区分 Vue 组件和原生 HTML 元素。然而对于传递 props 来说，使用 camelCase 并没有太多优势，因此我们推荐更贴近 HTML 的书写风格-短横线。

> 所有的 props 都遵循着**单向绑定**原则(`单项数据流`)，props 因父组件的更新而变化，自然地将新的状态向下流往子组件，而不会逆向传递。这避免了子组件意外修改父组件的状态的情况，不然应用的数据流将很容易变得混乱而难以理解。
>
> 另外，每次父组件更新后，所有的子组件中的 props 都会被更新到最新值，这意味着你**不应该**在子组件中去更改一个 prop。

#### 6.1.1 父组件给子组件传值1

完整案例：`29_parent_child_component_value1.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>29_父组件给子组件传值 方式1 - 数组</title>
</head>
<body>
  <div id="app">
    <my-parent></my-parent>
  </div>
</body>
<template id="parent">
  <div>
    我是父组件
    <!-- 父组件调用子组件的地方，添加自定义的属性,如果属性的值是变量，boolean类型，number类型，对象，数组，null，undefined，需要使用绑定属性 -->
    <my-child :msg="msg" :flag="true" :num="100" :obj="{a: 1, b: 2}" :arr="['a', 'b', 'c']"></my-child>
  </div>
</template>
<template id="child">
  <div>
    我是子组件 - {{ msg }} - {{ flag }} - {{ num }} - {{ obj }} - {{ arr }}
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  // 在定义子组件的地方，添加props选项，
  // props的数据类型为数组，数组的元素即为 自定义的属性名, 之后就可以直接在子组件中通过 自定义的属性名 使用 传递的值
  const Child = {
    props: ['msg', 'flag', 'num', 'obj', 'arr'],
    template: '#child'
  }
  const Parent = {
    template: '#parent',
    components: {
      MyChild: Child
    },
    data () {
      return {
        msg: 'hello parent'
      }
    }
  }

  const { createApp } = Vue

  const app = createApp({
    components: {
      MyParent: Parent
    }
  })

  app.mount('#app')

</script>
</html>
```

> 虽然上述案例已经完成了父组件给子组件传值，但是不够严谨
>
> 可能A负责父组件的编写，B负责了子组件的编写，容易造成 不知道 自定义的属性名的 数据类型

#### 6.1.2 父组件给子组件传值2

完整案例:`30_parent_child_component_value2.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>30_父组件给子组件传值 方式2 - 对象-数据类型</title>
</head>
<body>
  <div id="app">
    <my-parent></my-parent>
  </div>
</body>
<template id="parent">
  <div>
    我是父组件
    <!-- 父组件调用子组件的地方，添加自定义的属性,如果属性的值是变量，boolean类型，number类型，对象，数组，null，undefined，需要使用绑定属性 -->
    <my-child :msg="msg" :flag="true" :num="100" :obj="{a: 1, b: 2}" :arr="['a', 'b', 'c']"></my-child>
  </div>
</template>
<template id="child">
  <div>
    我是子组件 - {{ msg }} - {{ flag }} - {{ num }} - {{ obj }} - {{ arr }}
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  // 在定义子组件的地方，添加props选项，
  // 方式1：props的数据类型为数组，数组的元素即为 自定义的属性名, 之后就可以直接在子组件中通过 自定义的属性名 使用 传递的值
  // 方式2：props的数据类型为对象，对象的key值为自定义的属性名，value值为数据类型(不会阻碍程序运行，警告代码不严谨)
  //      如果一个自定义的属性的值既可以是 String 类型，也可以是 Number类型  key: String | Number
  const Child = {
    // props: ['msg', 'flag', 'num', 'obj', 'arr'],
    props: {
      msg: String,
      flag: Boolean,
      num: String | Number,
      obj: Object,
      arr: Array
    },
    template: '#child'
  }
  const Parent = {
    template: '#parent',
    components: {
      MyChild: Child
    },
    data () {
      return {
        msg: 'hello parent'
      }
    }
  }

  const { createApp } = Vue

  const app = createApp({
    components: {
      MyParent: Parent
    }
  })

  app.mount('#app')

</script>
</html>
```

> 现在只能知道哪一个属性是哪一种数据类型，但是有时候我们可以不需要设置 自定义的属性（. ）
>
> `<input />` <===> `<input type="text" />`

#### 6.1.3 父组件给子组件传值3

完整案例: `31_parent_child_component_value3.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>31_父组件给子组件传值 方式3 - 对象-数据类型-默认值</title>
</head>
<body>
  <div id="app">
    <my-parent></my-parent>
  </div>
</body>
<template id="parent">
  <div>
    我是父组件
    <!-- 父组件调用子组件的地方，添加自定义的属性,如果属性的值是变量，boolean类型，number类型，对象，数组，null，undefined，需要使用绑定属性 -->
    <my-child :msg="msg" :flag="true" :num="100" :obj="{a: 1, b: 2}" :arr="['a', 'b', 'c']"></my-child>
    <my-child :flag="true" :num="2000" :arr="['aa']"></my-child>
  </div>
</template>
<template id="child">
  <div>
    我是子组件 - {{ msg }} - {{ flag }} - {{ num }} - {{ obj }} - {{ arr }}
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  // 在定义子组件的地方，添加props选项，
  // 方式1：props的数据类型为数组，数组的元素即为 自定义的属性名, 之后就可以直接在子组件中通过 自定义的属性名 使用 传递的值
  // 方式2：props的数据类型为对象，对象的key值为自定义的属性名，value值为数据类型(不会阻碍程序运行，警告代码不严谨)
  //      如果一个自定义的属性的值既可以是 String 类型，也可以是 Number类型  key: String | Number
  // 方式3： props的数据类型为对象，对象的key值为自定义的属性名，value值为 一个新的对象
  //      对象的key值可以为 type， 那么value值就为 数据类型
  //      对象的key值可以为 required， 那么value值就为 true / false --- 该属性是不是必须传递,即使有默认值如果未传递还是要报警告
  //      对象的key值可以为 default， 那么value值就为 默认值 -- 如果属性的值是对象和数组，那么默认值设置为函数返回即可
  //      对象的key值可以为 validator 函数 ，可以自定义验证规则，但是不能验证多属性
  const Child = {
    // props: ['msg', 'flag', 'num', 'obj', 'arr'],
    // props: {
    //   msg: String,
    //   flag: Boolean,
    //   num: String | Number,
    //   obj: Object,
    //   arr: Array
    // },
    props: {
      msg: {
        type: String,
        required: true,
        default: 'hello vue',
        validator (val) { 
          return val.length > 20
        }
      },
      flag: Boolean,
      num: {
        // type: Number | String,
        type: Number,
        default: 3000,
        validator (val) { // 猜测：不能验证多类型数据 Right-hand side of 'instanceof' is not an object
          return val > 2000
        }
      },
      obj: {
        type: Object,
        default () { return { a: 3, b: 4}}
      },
      arr: {
        type: Array,
        default () { return ['1', '2', '3', '4']},
        validator (val) {
          return val.length > 2
        }
      }
    },
    template: '#child'
  }
  const Parent = {
    template: '#parent',
    components: {
      MyChild: Child
    },
    data () {
      return {
        msg: 'hello parent'
      }
    }
  }

  const { createApp } = Vue

  const app = createApp({
    components: {
      MyParent: Parent
    }
  })

  app.mount('#app')

</script>
</html>
```

#### 6.1.4 $attrs

一个包含了组件所有透传 attributes 的对象。

[透传 Attributes](https://cn.vuejs.org/guide/components/attrs.html) 是指由父组件传入，且没有被子组件声明为 props 或是组件自定义事件的 attributes 和事件处理函数。

`<my-button class="btn"></my-button>`

子组件模板只有button，那么my-button的class直接透传给子组件

`<button class="btn"></button>`

默认情况下，若是单一根节点组件，`$attrs` 中的所有属性都是直接自动继承自组件的根元素。而多根节点组件则不会如此，同时你也可以通过配置 [`inheritAttrs`](https://cn.vuejs.org/api/options-misc.html#inheritattrs) 选项来显式地关闭该行为。

### 6.2 监听事件

学习：状态选项emits、实例方法 $emit

#### 6.2.1 子组件给父组件传值- $emit

在组件的模板表达式中，可以直接使用 `$emit` 方法触发自定义事件

`$emit()` 方法在组件实例上也同样以 `this.$emit()` 的形式可用

父组件可以通过 `v-on` (缩写为 `@`) 来监听事件

同样，组件的事件监听器也支持 `.once` 修饰符

> 像组件与 prop 一样，事件的名字也提供了自动的格式转换。注意这里我们触发了一个以 camelCase 形式命名的事件，但在父组件中可以使用 kebab-case 形式来监听。与 [prop 大小写格式](https://cn.vuejs.org/guide/components/props.html#prop-name-casing)一样，在模板中我们也推荐使用 kebab-case 形式来编写监听器。

完整案例:`32_child_parent_component_value1.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>32_子组件给父组件传值</title>
</head>
<body>
  <div id="app">
    <my-parent></my-parent>
  </div>
</body>
<template id="parent">
  <div>
    我是父组件
    <!-- 在父组件调用子组件的地方，绑定一个自定义的事件，该事件由父组件实现，默认参数即为子组件传递给父组件的值 -->
    <my-child @my-event="getData"></my-child>
  </div>
</template>
<template id="child">
  <div>
    我是子组件
    <button @click="$emit('my-event', 2000)">传2000</button>
    <button @click="sendData(3000)">传3000</button>
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  // 在子组件的某一个事件内部，通过 this.$emit('自定义的事件名',传递的参数)完成子组件给父组件传值,内联事件处理器中不需要写this
  const Child = {
    template: '#child',
    mounted () {
      this.$emit('my-event', 1000)
    },
    methods: {
      sendData (num) {
        this.$emit('my-event', num)
      }
    }
  }
  const Parent = {
    template: '#parent',
    components: {
      MyChild: Child
    },
    methods: {
      getData (val) {
        console.log('接收到子组件的数据:' + val)
      }
    }
  }

  const { createApp } = Vue

  const app = createApp({
    components: {
      MyParent: Parent
    }
  })

  app.mount('#app')

</script>
</html>
```



#### 6.2.2 子组件给父组件传值-声明触发的事件

组件要触发的事件可以显式地通过 [`emits`](https://cn.vuejs.org/api/options-state.html#emits) 选项来声明：

```js
{
	emits: ['inFocus', 'submit']
}
```

这个 `emits` 选项还支持对象语法，它允许我们对触发事件的参数进行验证：

```
{
	 emits: {
        submit(payload) {
          // 通过返回值为 `true` 还是为 `false` 来判断
          // 验证是否通过
        }
      }
}
```

要为事件添加校验，那么事件可以被赋值为一个函数，接受的参数就是抛出事件时传入 `this.$emit` 的内容，返回一个布尔值来表明事件是否合法。

```js
{
  emits: {
    // 没有校验
    click: null,

    // 校验 submit 事件
    submit: ({ email, password }) => {
      if (email && password) {
        return true
      } else {
        console.warn('Invalid submit event payload!')
        return false
      }
    }
  },
  methods: {
    submitForm(email, password) {
      this.$emit('submit', { email, password })
    }
  }
}
```

完整案例：`33_child_parent_component_value2.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>33_子组件给父组件传值-声明触发的事件并且验证</title>
</head>
<body>
  <div id="app">
    <my-parent></my-parent>
  </div>
</body>
<template id="parent">
  <div>
    我是父组件
    <!-- 在父组件调用子组件的地方，绑定一个自定义的事件，该事件由父组件实现，默认参数即为子组件传递给父组件的值 -->
    <my-child @my-event="getData"></my-child>
  </div>
</template>
<template id="child">
  <div>
    我是子组件
    <button @click="$emit('my-event', 2000)">传2000</button>
    <button @click="sendData(3000)">传3000</button>
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  // 在子组件的某一个事件内部，通过 this.$emit('自定义的事件名',传递的参数)完成子组件给父组件传值,内联事件处理器中不需要写this
  const Child = {
    template: '#child',
    // emits: ['my-event'],
    emits: {
      'my-event': function (payload) {
        console.log(payload)
        return payload > 2000
      }
    },
    mounted () {
      this.$emit('my-event', 1000)
    },
    methods: {
      sendData (num) {
        this.$emit('my-event', num)
      }
    }
  }
  const Parent = {
    template: '#parent',
    components: {
      MyChild: Child
    },
    methods: {
      getData (val) {
        console.log('接收到子组件的数据:' + val)
      }
    }
  }

  const { createApp } = Vue

  const app = createApp({
    components: {
      MyParent: Parent
    }
  })

  app.mount('#app')

</script>
</html>
```

#### 6.2.3 自定义表单组件使用v-model

自定义事件可以用于开发支持 `v-model` 的自定义表单组件

完整案例：`34_custom_form_v-model.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>34_自定义表单组件配合 v-model</title>
</head>
<body>
  <div id="app">
    <my-input v-model="userName" placeholder="用户名"></my-input> {{ userName }}
    <my-input type="password" v-model="password" placeholder="密码"></my-input>{{ password }}
    <my-button @my-click="submit"></my-button>
  </div>
</body>
<template id="input">
  <input :type="type" :placeholder="placeholder" :value="modelValue" @input="$emit('update:modelValue', $event.target.value)" />
</template>
<template id="btn">
  <!-- 实际上执行2次，因为子组件模板中只有button -->
  <!-- <button @click="$emit('click')">提交</button> --> 
  <button @click="$emit('my-click')">提交</button>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const Input = {
    template: '#input',
    props: {
      type: {
        type: String,
        default: 'text'
      },
      placeholder: String,
      modelValue: String  // 自定义表单组件 v-model 内部 为 modelValue 属性和 input事件
    },
    emits: ['update:modelValue']
  }

  const Button = {
    template: '#btn'
  }

  const { createApp } = Vue

  const app = createApp({
    components: {
      MyInput: Input,
      MyButton: Button
    },
    data () {
      return {
        userName: '',
        password: ''
      }
    },
    methods: {
      submit () {
        console.log({
          userName: this.userName,
          password: this.password
        })
      }
    }
  })

  app.mount('#app')
</script>
</html>
```

另一种在组件内实现 `v-model` 的方式是使用一个可写的，同时具有 getter 和 setter 的计算属性。`get` 方法需返回 `modelValue` prop，而 `set` 方法需触发相应的事件：

```js
const Input = {
  template: `#input`,
  props: ['modelValue'],
  emits: ['update:modelValue'],
  computed: {
    value: {
      get() {
        return this.modelValue
      },
      set(value) {
        this.$emit('update:modelValue', value)
      }
    }
  }
}
```

#### 6.2.4 多个v-model的绑定

> 仅限于 vue3

完整案例：`35_custom_form_v-model_params.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>35_多个 v-model 绑定</title>
</head>
<body>
  <div id="app">
    <!-- <my-form v-model:username="username" v-model:password="password"></my-form> -->
    <my-custom v-model:username="username" v-model:password="password"></my-custom>
    <my-button @my-click="submit"></my-button>
  </div>
</body>
<template id="custom">
  <div>
    <input type="text" placeholder="用户名" :value="username" @input="$emit('update:username', $event.target.value)">
    <input type="password" placeholder="密码" :value="password" @input="$emit('update:password', $event.target.value)">
  </div>
</template>
<template id="btn">
  <!-- 实际上执行2次，因为子组件模板中只有button -->
  <!-- <button @click="$emit('click')">提交</button> --> 
  <button @click="$emit('my-click')">提交</button>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const Custom = {
    template: '#custom',
    props: {
      username: String,
      password: String
    },
    emits: ['update:username', 'update:password']
  }

  const Button = {
    template: '#btn'
  }

  const { createApp } = Vue

  const app = createApp({
    components: {
      MyCustom: Custom,
      MyButton: Button
    },
    data () {
      return {
        username: '',
        password: ''
      }
    },
    methods: {
      submit () {
        console.log({
          username: this.username,
          password: this.password
        })
      }
    }
  })

  app.mount('#app')
</script>
</html>
```



### 6.3 透传Attribute

“透传 attribute”指的是传递给一个组件，却没有被该组件声明为 [props](https://cn.vuejs.org/guide/components/props.html) 或 [emits](https://cn.vuejs.org/guide/components/events.html#defining-custom-events) 的 attribute 或者 `v-on` 事件监听器。最常见的例子就是 `class`、`style` 和 `id`。

#### 6.3.1 attribute继承

当一个组件以单个元素为根作渲染时，透传的 attribute 会自动被添加到根元素上。举例来说，假如我们有一个 `<MyButton>` 组件，它的模板长这样：

```html
<!-- <MyButton> 的模板 -->
<button>click me</button>
```

一个父组件使用了这个组件，并且传入了 `class`：

```
<MyButton class="large" />
```

最后渲染出的 DOM 结果是：

```
<button class="large">click me</button>
```

这里，`<MyButton>` 并没有将 `class` 声明为一个它所接受的 prop，所以 `class` 被视作透传 attribute，自动透传到了 `<MyButton>` 的根元素上。

#### 6.3.2 对 `class` 和 `style` 的合并

如果一个子组件的根元素已经有了 `class` 或 `style` attribute，它会和从父组件上继承的值合并。如果我们将之前的 `<MyButton>` 组件的模板改成这样：

```
<!-- <MyButton> 的模板 -->
<button class="btn">click me</button>
```

则最后渲染出的 DOM 结果会变成：

```
<button class="btn large">click me</button>
```

#### 6.3.3 `v-on` 监听器继承

同样的规则也适用于 `v-on` 事件监听器：

```
<MyButton @click="onClick" />
```

`click` 监听器会被添加到 `<MyButton>` 的根元素，即那个原生的 `<button>` 元素之上。当原生的 `<button>` 被点击，会触发父组件的 `onClick` 方法。同样的，如果原生 `button` 元素自身也通过 `v-on` 绑定了一个事件监听器，则这个监听器和从父组件继承的监听器都会被触发。

#### 6.3.4 禁用 Attributes 继承

如果你**不想要**一个组件自动地继承 attribute，你可以在组件选项中设置 `inheritAttrs: false`。

最常见的需要禁用 attribute 继承的场景就是 attribute 需要应用在根节点以外的其他元素上。通过设置 `inheritAttrs` 选项为 `false`，你可以完全控制透传进来的 attribute 被如何使用。

#### 6.3.5 多根节点的 Attributes 继承

`$attrs` 被显式绑定

完整案例: `36_attribute_transmission.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>36_透传attribute</title>
  <style>
    .btn {
      border: 0;
      padding: 10px 20px;
    }
    .btn-success {
      background-color: rgb(29, 198, 29);
      color: #fff;
    }
    .btn-danger {
      background-color: rgb(218, 23, 39);
      color: #fff;
    }
    .btn-primary {
      background-color: rgb(75, 104, 236);
      color: #fff;
    }
  </style>
</head>
<body>
  <div id="app">
    <my-button type="a" class="btn-success" @click="print('success')" @my-event="myalert(1)"></my-button>
    <my-button type="b" class="btn-danger"  @click="print('danger')" @my-event="myalert(2)"></my-button>
    <my-button type="c" class="btn-primary"  @click="print('primary')" @my-event="myalert(3)"></my-button>
  </div>
</body>

<template id="button">
  <!-- Attributes 继承(class 与style合并， v-on事件继承) -->
  <!--  <button class="btn">按钮</button> -->
  <!-- 深层组件继承 -->
  <base-button></base-button>
</template>
<template id="base">
  <button class="btn" v-bind="$attrs" >按钮</button>
  <div >测试</button>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const Base = {
    template: '#base',
    mounted () { // 在js中访问透传的 attributes
      console.log('2', this.$attrs)
    },
    // inheritAttrs: false // 不想要一个组件自动地继承 attribute，你可以在组件选项中设置
  }
  const Button = {
    template: '#button',
    components: {
      BaseButton: Base
    },
    mounted () {
      console.log('1', this.$attrs)
    },
   
  }
  

  const { createApp } = Vue

  const app = createApp({
    components: {
      MyButton: Button
    },
    methods: {
      print (msg) {
        console.log(msg)
      }
    },
    myalert (num) {
      alert(num)
    }
  })
  

  app.mount('#app')
</script>
</html>
```





### 6.4 特殊Attribute -ref

学习：实例属性refs

用于注册[模板引用](https://cn.vuejs.org/guide/essentials/template-refs.html)。

`ref` 用于注册元素或子组件的引用。

使用选项式 API，引用将被注册在组件的 `this.$refs` 对象里

放在DOM元素上，获取DOM节点，放到组件上，获取子组件的实例，可以直接使用子组件的属性和方法

完整案例:`37_attribute_ref.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>37_特殊的attribute——ref</title>
</head>
<body>
  <div id="app">
    <!-- ref用到自定义组件，可以获取到子组件的实例 -->
    <my-child ref="childRef"></my-child>
    <!-- 原生jsDOM操作 -->
    <input type="text" id="userName" @input="getJsData">
    <!-- ref用到DOM元素上，可以获取到当前的DOM元素 -->
    <input type="text" ref="password" @input="getRefData">
  </div>
</body>
<template id="child">
  <div>
    <h1>使用ref获取子组件的实例</h1>
  </div>
</template>

<script src="lib/vue.global.js"></script>
<script>
  const Child = {
    template: '#child',
    data () {
      return {
        count: 10
      }
    },
    computed: {
      doubleCount () {
        return this.count * 2
      }
    },
    methods: {
      print () {
        console.log(this.count)
      }
    }
  }

  const { createApp } = Vue

  const app = createApp({
    components: {
      MyChild: Child
    },
    mounted () {
      console.log(this)
      // 父组件可以通过 ref 直接获取到子组件的实例的属性和方法等
      console.log(this.$refs.childRef.count)
      console.log(this.$refs.childRef.doubleCount)
      this.$refs.childRef.print()
    },
    methods: {
      getJsData () {
        console.log(document.getElementById('userName').value)
      },
      getRefData () {
        console.log(this.$refs.password.value)
      }
    }
  })
  

  app.mount('#app')
</script>
</html>
```

### 6.5 $parent

当前组件可能存在的父组件实例，如果当前组件是顶层组件，则为 `null`。

完整案例`38_parent.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>38_parent</title>
</head>
<body>
  <div id="app">
    <my-parent></my-parent>
  </div>
</body>
<template id="parent">
  <div>
    我是父组件
    <my-child></my-child>
  </div>
</template>
<template id="child">
  <div>
    我是子组件 - {{ $parent.msg }}
    <button @click="$parent.print()">执行</button>
    <button @click="test">执行</button>
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const Child = {
    template: '#child',
    methods: {
      test () {
        this.$parent.print()
      }
    }
  }
  const Parent = {
    template: '#parent',
    components: {
      MyChild: Child
    },
    data () {
      return {
        msg: 'hello parent'
      }
    },
    methods: {
      print () {
        console.log(this.msg)
      }
    }
  }

  const { createApp } = Vue

  const app = createApp({
    components: {
      MyParent: Parent
    }
  })

  app.mount('#app')

</script>
</html>
```



### 6.6$root

当前组件树的根组件实例。如果当前实例没有父组件，那么这个值就是它自己。

完整案例:`39_root.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>39_root </title>
</head>
<body>
  <div id="app">
    <my-parent></my-parent>
  </div>
</body>
<template id="parent">
  <div>
    我是父组件 --- {{ $root.count }}
    <my-child></my-child>
  </div>
</template>
<template id="child">
  <div>
    我是子组件 --- {{ $root.count }}
    <button @click="$root.add()">加</button>
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const Child = {
    template: '#child',
    created () {
      console.log('child', this.$root)
    }
  }
  const Parent = {
    template: '#parent',
    components: {
      MyChild: Child
    },
    created () {
      console.log('parent', this.$root)
    }
  }

  const { createApp } = Vue

  const app = createApp({
    components: {
      MyParent: Parent
    },
    data () {
      return {
        count: 100
      }
    },
    methods: {
      add () {
        this.count++
      }
    }
  })

  app.mount('#app')

</script>
</html>
```

### 6.7 非父子组件传值

兄弟组件传值 - 中央事件总线传值 ---- vue2

![image-20220916160921781](assets/image-20220916160921781.png)

完整案例：`40_brother_value-vue2.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>40_vue2 兄弟组件传值</title>
</head>
<body>
  <div id="app">
    <my-content></my-content>
    <my-footer></my-footer>
  </div>
</body>
<script src="lib/vue.js"></script>
<script>
  const bus = new Vue() // 中央事件总线传值

  const Content = {
    template: `<div>{{ type }}</div>`,
    data () {
      return {
        type: '首页'
      }
    },
    mounted () {
      bus.$on('change-type', (val) => {
        this.type = val
      })
    }
  }

  const Footer = {
    template: `
      <footer>
        <ul>
          <li @click="changeType('首页')">首页</li>  
          <li @click="changeType('分类')">分类</li>  
          <li @click="changeType('购物车')">购物车</li>  
          <li @click="changeType('我的')">我的</li>  
        </ul>
      </footer>
    `,
    methods: {
      changeType (type) {
        bus.$emit('change-type', type)
      }
    }
  }

  new Vue({
    components: {
      MyContent: Content,
      MyFooter: Footer
    }
  }).$mount('#app')
  
</script>
</html>
```

vue3中没有明确的兄弟组件传值的方案，可以使用状态提升（找到这两个组件共同的父级组件，然后通过父与子之间的传值实现）

完整案例：`41_brother_value-vue3.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>41_vue3 兄弟组件传值-状态提升</title>
</head>
<body>
  <div id="app">
    <my-content :type="type" ></my-content>
    <my-footer @change-type="getVal"></my-footer>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  

  const Content = {
    props: ['type'],
    template: `<div>{{ type }}</div>`,
  }

  const Footer = {
    template: `
      <footer>
        <ul>
          <li @click="changeType('首页')">首页</li>  
          <li @click="changeType('分类')">分类</li>  
          <li @click="changeType('购物车')">购物车</li>  
          <li @click="changeType('我的')">我的</li>  
        </ul>
      </footer>
    `,
    methods: {
      changeType (type) {
       this.$emit('change-type', type)
      }
    }
  }

  Vue.createApp({
    components: {
      MyContent: Content,
      MyFooter: Footer
    },
    data () {
      return {
        type: '首页'
      }
    },
    methods: {
      getVal (type) {
        this.type = type
      }
    }
  }).mount('#app')
  
</script>
</html>
```



## 7.插槽

组件的最大特性就是 重用 ，而用好插槽能大大提高组件的可重用能力。

**插槽的作用：**父组件向子组件传递内容。

![img](assets/34.png)

通俗的来讲，**插槽无非就是在** 子组件 **中挖个坑，坑里面放什么东西由** 父组件 **决定。**

插槽类型有：

- 单个（匿名）插槽
- 具名插槽
- 作用域插槽

### 7.1 - 插槽内容与插口（<slot>）

在某些场景中，我们可能想要为子组件传递一些模板片段，让子组件在它们的组件中渲染这些片段。

这里有一个 `<FancyButton>` 组件，可以像这样使用：

```
<FancyButton>
  Click me! <!-- 插槽内容 -->
</FancyButton>
```

而 `<FancyButton>` 的模板是这样的：

```
<button class="fancy-btn">
  <slot></slot> <!-- 插槽出口 -->
</button>
```

`<slot>` 元素是一个**插槽出口** (slot outlet)，标示了父元素提供的**插槽内容** (slot content) 将在哪里被渲染。

![插槽图示](assets/slots.dbdaf1e8.png)

最终渲染出的 DOM 是这样：

```
<button class="fancy-btn">Click me!</button>
```

完整案例：`42_slot.html`	

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>42_插槽</title>
</head>
<body>
  <div id="app">
    <fancy-button>
      click me <!-- 插槽内容 -->
    </fancy-button>
    <fancy-button>
      登录 <!-- 插槽内容 -->
    </fancy-button>
    <fancy-button>
      注册 <!-- 插槽内容 -->
    </fancy-button>
  </div>
</body>
<template id="btn">
  <button>
    <slot></slot> <!-- 插槽出口 -->
  </button>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp } = Vue

  const Button = {
    template: '#btn'
  }

  const app = createApp({
    components: {
      FancyButton: Button
    }
  })

  app.mount('#app')
</script>
</html>
```



### 7.2渲染作用域

插槽内容可以访问到父组件的数据作用域，因为插槽内容本身是在父组件模板中定义的。举例来说：

```
<span>{{ message }}</span>
<FancyButton>{{ message }}</FancyButton>
```

这里的两个 `{{ message }}` 插值表达式渲染的内容都是一样的。

插槽内容**无法访问**子组件的数据。Vue 模板中的表达式只能访问其定义时所处的作用域，这和 JavaScript 的词法作用域规则是一致的。换言之：

> 父组件模板中的表达式只能访问父组件的作用域；子组件模板中的表达式只能访问子组件的作用域。

完整案例：`43_slot_render_scope.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>43_插槽渲染作用域</title>
</head>
<body>
  <div id="app">
    <div>{{ message }}</div>
    <fancy-button>
      {{ message }}<!-- 插槽内容 -->
    </fancy-button>
    
  </div>
</body>
<template id="btn">
  <button>
    <slot></slot> <!-- 插槽出口 -->
  </button>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp } = Vue

  const Button = {
    template: '#btn'
  }

  const app = createApp({
    components: {
      FancyButton: Button
    },
    data () {
      return {
        message: 'hello slot render scope!'
      }
    }
  })

  app.mount('#app')
</script>
</html>
```



### 7.3默认内容

在外部没有提供任何内容的情况下，可以为插槽指定默认内容。比如有这样一个 `<SubmitButton>` 组件：

```
<button type="submit">
  <slot></slot>
</button>
```

如果我们想在父组件没有提供任何插槽内容时在 `<button>` 内渲染“Submit”，只需要将“Submit”写在 `<slot>` 标签之间来作为默认内容：

```
<button type="submit">
  <slot>
    Submit <!-- 默认内容 -->
  </slot>
</button>
```

现在，当我们在父组件中使用 `<SubmitButton>` 且没有提供任何插槽内容时：

```
<SubmitButton />
```

“Submit”将会被作为默认内容渲染：

```
<button type="submit">Submit</button>
```

但如果我们提供了插槽内容：

```
<SubmitButton>Save</SubmitButton>
```

那么被显式提供的内容会取代默认内容：

```
<button type="submit">Save</button>
```

完整案例：`44_slot_default.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>44_插槽默认内容</title>
</head>
<body>
  <div id="app">
    <fancy-button>
      click me <!-- 插槽内容 -->
    </fancy-button>
    <fancy-button>
      登录 <!-- 插槽内容 -->
    </fancy-button>
    <fancy-button></fancy-button>
  </div>
</body>
<template id="btn">
  <button>
    <slot>按钮</slot> <!-- 插槽出口，给slot内写入内容作为插槽的默认值 -->
  </button>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp } = Vue

  const Button = {
    template: '#btn'
  }

  const app = createApp({
    components: {
      FancyButton: Button
    }
  })

  app.mount('#app')
</script>
</html>
```



### 7.4具名插槽（v-slot属性，#简写）

有时在一个组件中包含多个插槽出口是很有用的。举例来说，在一个 `<BaseLayout>` 组件中，有如下模板：

```html
<div class="container">
  <header>
    <!-- 标题内容放这里 -->
  </header>
  <main>
    <!-- 主要内容放这里 -->
  </main>
  <footer>
    <!-- 底部内容放这里 -->
  </footer>
</div>
```

对于这种场景，`<slot>` 元素可以有一个特殊的 attribute `name`，用来给各个插槽分配唯一的 ID，以确定每一处要渲染的内容：

```html
<div class="container">
  <header>
    <slot name="header"></slot>
  </header>
  <main>
    <slot></slot>
  </main>
  <footer>
    <slot name="footer"></slot>
  </footer>
</div>
```

这类带 `name` 的插槽被称为具名插槽 (named slots)。没有提供 `name` 的 `<slot>` 出口会隐式地命名为“default”。

在父组件中使用 `<BaseLayout>` 时，我们需要一种方式将多个插槽内容传入到各自目标插槽的出口。此时就需要用到**具名插槽**了：

要为具名插槽传入内容，我们需要使用一个含 `v-slot` 指令的 `<template>` 元素，并将目标插槽的名字传给该指令：

```html
<BaseLayout>
  <template v-slot:header>
    <!-- header 插槽的内容放这里 -->
  </template>
</BaseLayout>
```

>  `v-slot` 有对应的简写 `#`，因此 `<template v-slot:header>` 可以简写为 `<template #header>`。其意思就是“将这部分模板片段传入子组件的 header 插槽中”。

![具名插槽图示](assets/named-slots.ebb7b207.png)

完整案例：`45_slot_name.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>45_具名插槽</title>
</head>
<body>
  <div id="app">
    <base-layout>
      <template v-slot:header>首页头部</template>
      <template v-slot:default>首页内容</template>
      <template v-slot:footer>首页底部</template>
    </base-layout>
    <hr />
    <base-layout>
      <template v-slot:default>分类内容</template>
    </base-layout>
    <hr />
    <base-layout>
      <template #header>我的头部</template>
      <template #default>我的内容</template>
      <template #footer>我的底部</template>
    </base-layout>

    <!-- vue2.7 版本以前,现在不会走具名插槽 -->
    <base-layout>
      <div slot="header">vue2.6头部</div>
      <div slot="default">vue2.6内容</div>
      <div slot="footer">vue2.6底部</div>
    </base-layout>

  </div>
</body>
<template id="layout">
  <div class="container">
    <header class="header">
      <slot name="header">header</slot>
    </header>
    <div class="content">
      <!-- 不写name时，相当于 写了name="default" -->
      <slot>content</slot>
    </div>
    <footer class="footer">
      <slot name="footer">footer</slot>
    </footer>
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp } = Vue

  const Layout = {
    template: '#layout'
  }

  const app = createApp({
    components: {
      BaseLayout: Layout
    }
  })

  app.mount('#app')
</script>
</html>
```



### 7.5动态插槽名

[动态指令参数](https://cn.vuejs.org/guide/essentials/template-syntax.html#dynamic-arguments)在 `v-slot` 上也是有效的，即可以定义下面这样的动态插槽名：

```
<base-layout>
  <template v-slot:[dynamicSlotName]>
    ...
  </template>

  <!-- 缩写为 -->
  <template #[dynamicSlotName]>
    ...
  </template>
</base-layout>
```

注意这里的表达式和动态指令参数受相同的[语法限制](https://cn.vuejs.org/guide/essentials/template-syntax.html#directives)。

完整案例：`46_dynamic_slot_name.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>46_动态插槽名</title>
</head>
<body>
  <div id="app">
    <button @click="count++">{{ count }}</button>
    <my-com>
      <template v-slot:[type]>
        {{ type }} 父组件默认值
      </template>
    </my-com>
    
  </div>
</body>
<template id="com">
  <div >
    <slot name="dynFirst"> 1 动态插槽名   子组件默认值 1</slot>
    <br />
    <slot name="dynLast"> 2 动态插槽名   子组件默认值 2</slot>
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp } = Vue

  const Com = {
    template: '#com'
  }

  const app = createApp({
    components: {
      MyCom: Com
    },
    data () {
      return {
        count: 0
      }
    },
    computed: {
      type () {
        return this.count % 2 === 0 ? 'dynLast': 'dynFirst'
      }
    }
  })

  app.mount('#app')
</script>
</html>
```

### 7.6作用域插槽

在某些场景下插槽的内容可能想要同时使用父组件域内和子组件域内的数据。要做到这一点，我们需要一种方法来让子组件在渲染时将一部分数据提供给插槽。

![scoped slots diagram](https://cn.vuejs.org/assets/scoped-slots.1c6d5876.svg)

完整案例：`47_scope_slot.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>47_作用域插槽(配合具名插槽)</title>
</head>
<body>
  <div id="app">
    <my-com>
      <!-- <template v-slot:test="testProps">作用域插槽以及具名插槽-{{testProps.message}}- {{ testProps.count }}</template> -->
      <!-- <template #test="testProps">作用域插槽以及具名插槽-{{testProps.message}}- {{ testProps.count }}</template> -->
      <template #test="{ message, count }">作用域插槽以及具名插槽-{{message}}- {{ count }}</template>
    </my-com>
    <my-com1>
      <!-- <template v-slot="slotProps">作用域插槽-{{slotProps.message}}- {{ slotProps.count }}</template> -->
      <!-- <template v-slot="{ message, count }">作用域插槽-{{message}}- {{ count }}</template> -->
      <!-- <template #="{ message, count }">作用域插槽-{{message}}- {{ count }}</template> -->
      <template #default="{ message, count }">作用域插槽-{{message}}- {{ count }}</template>
    </my-com1>
  </div>
</body>
<template id="com">
  <div>
    <!-- 一旦使用了具名插槽，在使用作用域插槽时 v-slot:具名="属性名" -->
    <slot name="test" :message="message" :count="1"></slot>
  </div>
</template>
<template id="com1">
  <div>
     <!-- 没有使用具名插槽，在使用作用域插槽时 v-slot="属性名" -->
    <slot :message="message" :count="1"></slot>
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp } = Vue

  const Com = {
    template: '#com',
    data () {
      return {
        message: 'hello msg'
      }
    }
  }
  const Com1 = {
    template: '#com1',
    data () {
      return {
        message: 'hello msg1'
      }
    }
  }
  

  const app = createApp({
    components: {
      MyCom: Com,
      MyCom1: Com1
    }
  })

  app.mount('#app')
</script>
</html>
```



### 7.7$slots

一个表示父组件所传入[插槽](https://cn.vuejs.org/guide/components/slots.html)的对象。

通常用于手写[渲染函数](https://cn.vuejs.org/guide/extras/render-function.html)，但也可用于检测是否存在插槽。

每一个插槽都在 `this.$slots` 上暴露为一个函数，返回一个 vnode 数组，同时 key 名对应着插槽名。默认插槽暴露为 `this.$slots.default`。

如果插槽是一个[作用域插槽](https://cn.vuejs.org/guide/components/slots.html#scoped-slots)，传递给该插槽函数的参数可以作为插槽的 prop 提供给插槽。

在渲染函数中，可以通过 [this.$slots](https://cn.vuejs.org/api/component-instance.html#slots) 来访问插槽：

完整案例：`48_$slot.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>48_$slots渲染函数</title>
</head>
<body>
  <div id="app">
    <my-com>
      <template #default>1111</template>
      <template #footer>2222</template>
    </my-com>
  </div>
</body>
<template id="com">
  <div><slot>默认值</slot></div>
  <div><slot name="footer">底部默认值</slot></div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, h } = Vue // h 代表创建一个元素 createElement

  const Com = {
    // template: '#com'
    render () {
      console.log(this.$slots)
      return [
        h('div', { class: 'content'}, this.$slots.default()), // <div calss="content"><slot></slot></div>
        h('div', { class: 'footer'}, this.$slots.footer()) // <div class="footer"><slot name="footer"></slot></div>
      ]
    }
  }

  const app = createApp({
    components: {
      MyCom: Com
    }
  })

  app.mount('#app')
</script>
</html>
```

## 8.依赖注入

通常情况下，当我们需要从父组件向子组件传递数据时，会使用 [props](https://cn.vuejs.org/guide/components/props.html)。想象一下这样的结构：有一些多层级嵌套的组件，形成了一颗巨大的组件树，而某个深层的子组件需要一个较远的祖先组件中的部分数据。在这种情况下，如果仅使用 props 则必须将其沿着组件链逐级传递下去，这会非常麻烦：

![Prop 逐级透传的过程图示](assets/prop-drilling.11201220.png)

注意，虽然这里的 `<Footer>` 组件可能根本不关心这些 props，但为了使 `<DeepChild>` 能访问到它们，仍然需要定义并向下传递。如果组件链路非常长，可能会影响到更多这条路上的组件。这一问题被称为“prop 逐级透传”，显然是我们希望尽量避免的情况。

`provide` 和 `inject` 可以帮助我们解决这一问题。 [[1\]](https://cn.vuejs.org/guide/components/provide-inject.html#footnote-1) 一个父组件相对于其所有的后代组件，会作为**依赖提供者**。任何后代的组件树，无论层级有多深，都可以**注入**由父组件提供给整条链路的依赖。

![Provide/inject 模式](assets/provide-inject.3e0505e4.png)

### 8.1 provide

要为组件后代提供数据，需要使用到 [`provide`](https://cn.vuejs.org/api/options-composition.html#provide) 选项：

```
{
  provide: {
    message: 'hello!'
  }
}
```

对于 `provide` 对象上的每一个属性，后代组件会用其 key 为注入名查找期望注入的值，属性的值就是要提供的数据。

如果我们需要提供依赖当前组件实例的状态 (比如那些由 `data()` 定义的数据属性)，那么可以以函数形式使用 `provide`：

```
{
  data() {
    return {
      message: 'hello!'
    }
  },
  provide() {
    // 使用函数的形式，可以访问到 `this`
    return {
      message: this.message
    }
  }
}
```

这**不会**使注入保持响应性（比如祖先组件中有一个count的状态，祖先组件修改完状态，后代组件默认的值没有响应式的改变）

### 8.2 inject

要注入上层组件提供的数据，需使用 [`inject`](https://cn.vuejs.org/api/options-composition.html#inject) 选项来声明：

```
{
  inject: ['message'],
  created() {
    console.log(this.message) // injected value
  }
}
```

注入会在组件自身的状态**之前**被解析，因此你可以在 `data()` 中访问到注入的属性：

```
{
  inject: ['message'],
  data() {
    return {
      // 基于注入值的初始数据
      fullMessage: this.message
    }
  }
}
```

> 当以数组形式使用 `inject`，注入的属性会以同名的 key 暴露到组件实例上。在上面的例子中，提供的属性名为 `"message"`，注入后以 `this.message` 的形式暴露。访问的本地属性名和注入名是相同的。
>
> 如果我们想要用一个不同的本地属性名注入该属性，我们需要在 `inject` 选项的属性上使用对象的形式：
>
> ```
> {
>   inject: {
>     /* 本地属性名 */ localMessage: {
>       from: /* 注入来源名 */ 'message'
>     }
>   }
> }
> ```
>
> 这里，组件本地化了原注入名 `"message"` 所提供的的属性，并将其暴露为 `this.localMessage`。
>
> 默认情况下，`inject` 假设传入的注入名会被某个祖先链上的组件提供。如果该注入名的确没有任何组件提供，则会抛出一个运行时警告。
>
> 如果在注入一个值时不要求必须有提供者，那么我们应该声明一个默认值，和 props 类似：
>
> ```
> {
>   // 当声明注入的默认值时
>   // 必须使用对象形式
>   inject: {
>     message: {
>       from: 'message', // 当与原注入名同名时，这个属性是可选的
>       default: 'default value'
>     },
>     user: {
>       // 对于非基础类型数据，如果创建开销比较大，或是需要确保每个组件实例
>       // 需要独立数据的，请使用工厂函数
>       default: () => ({ name: 'John' })
>     }
>   }
> }
> ```

完整案例：`49_provide_inject.html`

```
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>49_依赖注入</title>
</head>
<body>
  <div id="app">
    <button @click="count++">加1</button> {{ count }}
    <my-parent></my-parent>
  </div>
</body>
<template id="parent">
  <div>
    我是父组件 - {{ message }}
    <my-child></my-child>
  </div>
</template>
<template id="child">
  <div>
    我是子组件 - {{ myMessage }} - {{ count }}
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const Child = {
    template: '#child',
    inject: {
      myMessage: { // 替换本地名字，
        from: 'message'
      },
      count: {
        default: 100
      }
    }
  }
  const Parent = {
    template: '#parent',
    inject: ['message'],
    components: {
      MyChild: Child
    }
  }

  const { createApp } = Vue

  const app = createApp({
    components: {
      MyParent: Parent
    },
    data () {
      return {
        message: '传家宝',
        count: 1
      }
    },
    provide () {
      return {
        message: this.message,
        count: this.count
      }
    }
  })

  app.mount('#app')

</script>
</html>
```

> 发现以上案例在count值发生改变时没有更新后代数据

### 8.3 配合响应性 computed()

为保证注入方和供给方之间的响应性链接，我们需要使用 [computed()](https://cn.vuejs.org/api/reactivity-core.html#computed) 函数提供一个计算属性

完整案例：`50_provide_inject_computed_vue3.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>50_依赖注入配合响应式</title>
</head>
<body>
  <div id="app">
    <button @click="count++">加1</button> {{ count }}
    <my-parent></my-parent>
  </div>
</body>
<template id="parent">
  <div>
    我是父组件 - {{ message }}
    <my-child></my-child>
  </div>
</template>
<template id="child">
  <div>
    我是子组件 - {{ myMessage }} - {{ count }}
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const Child = {
    template: '#child',
    inject: {
      myMessage: { // 替换本地名字，
        from: 'message'
      },
      count: {
        default: 100
      }
    }
  }
  const Parent = {
    template: '#parent',
    inject: ['message'],
    components: {
      MyChild: Child
    }
  }

  const { createApp, computed } = Vue

  const app = createApp({
    components: {
      MyParent: Parent
    },
    data () {
      return {
        message: '传家宝',
        count: 1
      }
    },
    provide () {
      return {
        message: this.message,
        // count: this.count
        count: computed(() => this.count) // 响应式关键
      }
    }
  })

  app.mount('#app')

</script>
</html>
```

> 测试得知vue2中也是如此处理数据
>
> `50_provide_inject_computed_vue2.html`
>
> ```
> <!DOCTYPE html>
> <html lang="en">
> <head>
>   <meta charset="UTF-8">
>   <meta http-equiv="X-UA-Compatible" content="IE=edge">
>   <meta name="viewport" content="width=device-width, initial-scale=1.0">
>   <title>50_依赖注入配合响应式</title>
> </head>
> <body>
>   <div id="app">
>     <button @click="count++">加1</button> {{ count }}
>     <my-parent></my-parent>
>   </div>
> </body>
> <template id="parent">
>   <div>
>     我是父组件 - {{ message }}
>     <my-child></my-child>
>   </div>
> </template>
> <template id="child">
>   <div>
>     我是子组件 - {{ myMessage }} - {{ count }}
>   </div>
> </template>
> <script src="lib/vue.js"></script>
> <script>
>   const Child = {
>     template: '#child',
>     inject: {
>       myMessage: { // 替换本地名字，
>         from: 'message'
>       },
>       count: {
>         default: 100
>       }
>     }
>   }
>   const Parent = {
>     template: '#parent',
>     inject: ['message'],
>     components: {
>       MyChild: Child
>     }
>   }
>   // console.log(Vue.computed)
>   const { computed } = Vue
> 
>   new Vue({
>     components: {
>       MyParent: Parent
>     },
>     data: {
>       message: '传家宝',
>       count: 1
>     },
>     provide () {
>       return {
>         message: this.message,
>         // count: this.count
>         count: computed(() => this.count) // 响应式关键
>       }
>     }
>   }).$mount('#app')
> 
> </script>
> </html>
> ```
>
> 

## 9.动态组件

有些场景会需要在两个组件间来回切换(Tab切换)

![image-20220919113443278](assets/image-20220919113443278.png)

### 9.1特殊 Attribute—is

用于绑定[动态组件](https://cn.vuejs.org/guide/essentials/component-basics.html#dynamic-components)。

```
<!-- currentTab 改变时组件也改变 --> 
<component :is="currentTab"></component>
```

完整案例：`51_dynamic_component.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>51_动态组件</title>
</head>
<body>
  <div id="app">
    <ul>
      <li @click="currentTab='Home'">首页</li>
      <li @click="currentTab='Kind'">分类</li>
      <li @click="currentTab='Cart'">购物车</li>
      <li @click="currentTab='User'">我的</li>
    </ul>

    <!-- 动态组件 -->
    <component :is="currentTab"></component>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const Home = {
    template: `<div>
        首页 <input />
      </div>`
  }
  const Kind = {
    template: `<div>
        分类 <input />
      </div>`
  }
  const Cart = {
    template: `<div>
        购物车 <input />
      </div>`
  }
  const User = {
    template: `<div>
        我的 <input />
      </div>`
  }

  Vue.createApp({
    components: {
      Home,
      Kind,
      Cart,
      User
    },
    data () {
      return {
        currentTab: 'Home'
      }
    }
  }).mount('#app')
</script>
</html>
```

> 如果此时给每个组件加入一个输入框，输入内容切换组件查看效果，发现切换回来数据不在

### 9.2 `<KeepAlive>`组件

缓存包裹在其中的动态切换组件

`<KeepAlive>` 包裹动态组件时，会缓存不活跃的组件实例，而不是销毁它们。

任何时候都只能有一个活跃组件实例作为 `<KeepAlive>` 的直接子节点。

完整案例：`52_keep-alive.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>52_keep-alive</title>
</head>
<body>
  <div id="app">
    <ul>
      <li @click="currentTab='Home'">首页</li>
      <li @click="currentTab='Kind'">分类</li>
      <li @click="currentTab='Cart'">购物车</li>
      <li @click="currentTab='User'">我的</li>
    </ul>

    <!-- 动态组件 -->
    <!-- 实际上is属性的值为组件的名字 -->
    <!-- 可以通过 keep-alive 保留组件的状态，避免组件的销毁和重建 -->
    <keep-alive>
      <component :is="currentTab"></component>
    </keep-alive>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const Home = {
    template: `<div>
        首页 <input />
      </div>`
  }
  const Kind = {
    template: `<div>
        分类 <input />
      </div>`
  }
  const Cart = {
    template: `<div>
        购物车 <input />
      </div>`
  }
  const User = {
    template: `<div>
        我的 <input />
      </div>`
  }

  Vue.createApp({
    components: {
      Home,
      Kind,
      Cart,
      User
    },
    data () {
      return {
        currentTab: 'Home'
      }
    }
  }).mount('#app')
</script>
</html>
```

> 当一个组件在 `<KeepAlive>` 中被切换时，它的 `activated` 和 `deactivated` 生命周期钩子将被调用，用来替代 `mounted` 和 `unmounted`。这适用于 `<KeepAlive>` 的直接子节点及其所有子孙节点。

### 9.3activated、deactivated钩子

完整案例：`53_activated_deacvidated.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>53_keep-alive钩子函数</title>
</head>
<body>
  <div id="app">
    <ul>
      <li @click="currentTab='Home'">首页</li>
      <li @click="currentTab='Kind'">分类</li>
      <li @click="currentTab='Cart'">购物车</li>
      <li @click="currentTab='User'">我的</li>
    </ul>

    <!-- 动态组件 -->
    <!-- 实际上is属性的值为组件的名字 -->
    <!-- 可以通过 keep-alive 保留组件的状态，避免组件的销毁和重建 -->
    <keep-alive>
      <component :is="currentTab"></component>
    </keep-alive>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  // 假设首页面需要保留组件的状态，跳转到新的页面再回到首页时，要确保数据的请求的及时性时效性
  // 以前 mounted 中请求了数据，使用keepalive时再回到首页 mounted 并没有执行
  // 如果要获取最新的数据，请在activated中获取

  // 假设首页长列表，点击某一项可以进入详情，返回首页之后，还希望滚动条在原来的位置
  // 以前销毁组件时记录滚动条位置，是应用keep-alive之后，在deacvidated中记录滚动条位置
  const Home = {
    template: `<div>
        首页 <input />
      </div>`,
      created () {
        console.log('首页 created')
      },
      mounted () {
        console.log('首页 mounted')
      },
      unmounted () {
        console.log('首页 unmounted')
      },
      activated () {
        console.log('首页 activated')
      },
      deactivated () {
        console.log('首页 deactivated')
      }
  }
  const Kind = {
    template: `<div>
        分类 <input />
      </div>`,
      created () {
        console.log('分类 created')
      },
      mounted () {
        console.log('分类 mounted')
      },
      unmounted () {
        console.log('分类 unmounted')
      },
      activated () {
        console.log('分类 activated')
      },
      deactivated () {
        console.log('分类 deactivated')
      }
  }
  const Cart = {
    template: `<div>
        购物车 <input />
      </div>`,
      created () {
        console.log('购物车 created')
      },
      mounted () {
        console.log('购物车 mounted')
      },
      unmounted () {
        console.log('购物车 unmounted')
      },
      activated () {
        console.log('购物车 activated')
      },
      deactivated () {
        console.log('购物车 deactivated')
      }
  }
  const User = {
    template: `<div>
        我的 <input />
      </div>`,
      created () {
        console.log('我的 created')
      },
      mounted () {
        console.log('我的 mounted')
      },
      unmounted () {
        console.log('我的 unmounted')
      },
      activated () {
        console.log('我的 activated')
      },
      deactivated () {
        console.log('我的 deactivated')
      }
  }

  Vue.createApp({
    components: {
      Home,
      Kind,
      Cart,
      User
    },
    data () {
      return {
        currentTab: 'Home'
      }
    }
  }).mount('#app')
</script>
</html>
```

> 要不不缓存，要缓存都缓存了，这样不好
>
> 使用 `include` / `exclude`可以设置哪些组件被缓存，使用 `max`可以设定最多缓存多少个
>
> ```
> <!-- 用逗号分隔的字符串 -->
> <KeepAlive include="a,b">
> <component :is="view"></component>
> </KeepAlive>
> 
> <!-- 正则表达式 (使用 `v-bind`) -->
> <KeepAlive :include="/a|b/">
> <component :is="view"></component>
> </KeepAlive>
> 
> <!-- 数组 (使用 `v-bind`) -->
> <KeepAlive :include="['a', 'b']">
> <component :is="view"></component>
> </KeepAlive>
> ```
>
> 组件如果想要条件性地被 `KeepAlive` 缓存，就必须显式声明一个 `name` 选项。
>
> 完整案例：`54_keep_alive_include.html`
>
> ```html
> <!DOCTYPE html>
> <html lang="en">
> <head>
>   <meta charset="UTF-8">
>   <meta http-equiv="X-UA-Compatible" content="IE=edge">
>   <meta name="viewport" content="width=device-width, initial-scale=1.0">
>   <title>54_选择性缓存</title>
> </head>
> <body>
>   <div id="app">
>     <ul>
>       <li @click="currentTab='Home'">首页</li>
>       <li @click="currentTab='Kind'">分类</li>
>       <li @click="currentTab='Cart'">购物车</li>
>       <li @click="currentTab='User'">我的</li>
>     </ul>
> 
>     <!-- 动态组件 -->
>     <!-- 实际上is属性的值为组件的名字 -->
>     <!-- 可以通过 keep-alive 保留组件的状态，避免组件的销毁和重建 -->
> 
>     <!-- 用逗号分隔的字符串 注意不要加空格 -->
>     <!-- <keep-alive include="home,kind"> -->
> 
>     <!-- 正则表达式 (使用 `v-bind`) 使用绑定属性 -->
>     <!-- <keep-alive :include="/home|kind/"> -->
> 
>     <!-- 数组 (使用 `v-bind`) -->
>     <keep-alive :include="['home', 'kind']">
>       <component :is="currentTab"></component>
>     </keep-alive>
>   </div>
> </body>
> <script src="lib/vue.global.js"></script>
> <script>
>   // 假设首页面需要保留组件的状态，跳转到新的页面再回到首页时，要确保数据的请求的及时性时效性
>   // 以前 mounted 中请求了数据，使用keepalive时再回到首页 mounted 并没有执行
>   // 如果要获取最新的数据，请在activated中获取
> 
>   // 假设首页长列表，点击某一项可以进入详情，返回首页之后，还希望滚动条在原来的位置
>   // 以前销毁组件时记录滚动条位置，是应用keep-alive之后，在deacvidated中记录滚动条位置
>   const Home = {
>     name: 'home',
>     template: `<div>
>         首页 <input />
>       </div>`,
>       created () {
>         console.log('首页 created')
>       },
>       mounted () {
>         console.log('首页 mounted')
>       },
>       unmounted () {
>         console.log('首页 unmounted')
>       },
>       activated () {
>         console.log('首页 activated')
>       },
>       deactivated () {
>         console.log('首页 deactivated')
>       }
>   }
>   const Kind = {
>     name: 'kind',
>     template: `<div>
>         分类 <input />
>       </div>`,
>       created () {
>         console.log('分类 created')
>       },
>       mounted () {
>         console.log('分类 mounted')
>       },
>       unmounted () {
>         console.log('分类 unmounted')
>       },
>       activated () {
>         console.log('分类 activated')
>       },
>       deactivated () {
>         console.log('分类 deactivated')
>       }
>   }
>   const Cart = {
>     name: 'cart',
>     template: `<div>
>         购物车 <input />
>       </div>`,
>       created () {
>         console.log('购物车 created')
>       },
>       mounted () {
>         console.log('购物车 mounted')
>       },
>       unmounted () {
>         console.log('购物车 unmounted')
>       },
>       activated () {
>         console.log('购物车 activated')
>       },
>       deactivated () {
>         console.log('购物车 deactivated')
>       }
>   }
>   const User = {
>     name: 'user',
>     template: `<div>
>         我的 <input />
>       </div>`,
>       created () {
>         console.log('我的 created')
>       },
>       mounted () {
>         console.log('我的 mounted')
>       },
>       unmounted () {
>         console.log('我的 unmounted')
>       },
>       activated () {
>         console.log('我的 activated')
>       },
>       deactivated () {
>         console.log('我的 deactivated')
>       }
>   }
> 
>   Vue.createApp({
>     components: {
>       Home,
>       Kind,
>       Cart,
>       User
>     },
>     data () {
>       return {
>         currentTab: 'Home'
>       }
>     }
>   }).mount('#app')
> </script>
> </html>
> ```
>
> 

### 9.4`<component>`元素

一个用于渲染动态组件或元素的“元组件”

完整案例:`55_component_element.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>55_component元素</title>
</head>
<body>
  <div id="app">
    <input type="checkbox" v-model="flag" />
    <!-- 条件为真渲染为 a 标签，否则为 span 标签 -->
    <component :is="flag ? 'a' : 'span'">你好</component>

    <component :is="flag ? 'my-com1' : 'my-com2'"></component>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const Com1 = {
    template: `<div>com1</div>`
  }
  const Com2 = {
    template: `<div>com2</div>`
  }
  Vue.createApp({
    components: {
      MyCom1: Com1,
      MyCom2: Com2
    },
    data () {
      return {
        flag: false
      }
    }
  }).mount('#app')
</script>
</html>
```

> 也可以渲染组件

### 9.5 DOM 模板解析注意事项（is="vue:xxx"）

当 `is` attribute 用于原生 HTML 元素时，它将被当作 [Customized built-in element](https://html.spec.whatwg.org/multipage/custom-elements.html#custom-elements-customized-builtin-example)，其为原生 web 平台的特性。

但是，在这种用例中，你可能需要 Vue 用其组件来替换原生元素，如 [DOM 模板解析注意事项](https://cn.vuejs.org/guide/essentials/component-basics.html#dom-template-parsing-caveats)所述。你可以在 `is` attribute 的值中加上 `vue:` 前缀，这样 Vue 就会把该元素渲染为 Vue 组件(`my-row-component`为自定义组件)：

```
<table>
  <tr is="vue:my-row-component"></tr>
</table>
```

完整案例：`56_DOM模板解析注意事项.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>56_DOM模板解析注意事项</title>
</head>
<body>
  <div id="app">
    <table>
      <tr>
        <td>序号</td>
        <td>姓名</td>
        <td>密码</td>
      </tr>
      <!-- <my-tr :list="list"></my-tr> -->
      <!-- Vue 用其组件来替换原生元素 -->
      <tr is="vue:my-tr" :list="list"></tr>
    </table>
  </div>
</body>
<template id="tr">
  <tr v-for="item of list" :key="item.id">
    <td>{{ item.id }}</td>
    <td>{{ item.name }}</td>
    <td>{{ item.password }}</td>
  </tr>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const Tr = {
    props: ['list'],
    template: '#tr'
  }
  Vue.createApp({
    components: {
      MyTr: Tr
    },
    data () {
      return {
        list: [
          {
            id: 1,
            name: '张三',
            password: '123'
          },
          {
            id: 2,
            name: '李四',
            password: '234'
          },
          {
            id: 3,
            name: '王五',
            password: '345'
          }
        ]
      }
    }
  }).mount('#app')
</script>
</html>
```

> 注意不要使用绑定属性

## 10.异步组件

在大型项目中，我们可能需要拆分应用为更小的块，并仅在需要时再从服务器加载相关组件。Vue 提供了 [`defineAsyncComponent`](https://cn.vuejs.org/api/general.html#defineasynccomponent) 方法来实现此功能

### 10.1  全局API

学习：defineAsyncComponent()

```
import { defineAsyncComponent } from 'vue'

const AsyncComp = defineAsyncComponent(() => {
  return new Promise((resolve, reject) => {
    // ...从服务器获取组件
    resolve(/* 获取到的组件 */)
  })
})
// ... 像使用其他一般组件一样使用 `AsyncComp`
```

完整案例：`57_defineAsyncComponent.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>57_定义异步组件</title>
</head>
<body>
  <div id="app">
    <my-test></my-test>
    <hr/>
    <my-com></my-com>
  </div>
</body>
<template id="com">
  <div>异步加载组件</div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, defineAsyncComponent } = Vue
  const Com = {
    template: '#com'
  }
  const MyCom = defineAsyncComponent(() => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Com)
      }, 3000)
    })
  })

  createApp({
    components: {
      MyCom,
      MyTest: Com
    }
  }).mount('#app')
</script>
</html>
```



### 10.2加载函数

学习：() => import()

[ES 模块动态导入](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import#dynamic_imports)也会返回一个 Promise，所以多数情况下我们会将它和 `defineAsyncComponent` 搭配使用。类似 Vite 和 Webpack 这样的构建工具也支持此语法 (并且会将它们作为打包时的代码分割点)，因此我们也可以用它来导入 Vue 单文件组件

```
import { defineAsyncComponent } from 'vue'

const AsyncComp = defineAsyncComponent(() =>
  import('./components/MyComponent.vue')
)
```

以后讲解项目时可以用到，需要在脚手架环境中使用（`单文件组件中使用`）

### 10.3 `<Suspense>`组件

`<Suspense>` 是一个内置组件，用来在组件树中协调对异步依赖的处理。它让我们可以在组件树上层等待下层的多个嵌套异步依赖项解析完成，并可以在等待时渲染一个加载状态。

> vue3中新增的

完整案例：`58_Suspense.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>58_Suspense</title>
</head>
<body>
  <div id="app">
    <my-test></my-test>
    <hr/>
    <!-- Suspense内部需要一个根组件 -->
    <Suspense>
      <!-- 异步加载组件 -->
      <my-com></my-com>

      <!-- 加载状态 -->
      <template #fallback>
        加载中...
      </template>
    </Suspense>
  </div>
</body>
<template id="com">
  <div>异步加载组件</div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, defineAsyncComponent } = Vue
  const Com = {
    template: '#com'
  }
  const MyCom = defineAsyncComponent(() => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Com)
      }, 3000)
    })
  })

  createApp({
    components: {
      MyCom,
      MyTest: Com
    }
  }).mount('#app')
</script>
</html>
```

> 后期可以和 `Transition`,`KeepAlive`,`路由`等结合使用

## 11.自定义指令

除了 Vue 内置的一系列指令 (比如 `v-model` 或 `v-show`) 之外，Vue 还允许你注册自定义的指令 (Custom Directives)。

一个自定义指令由一个包含类似组件生命周期钩子的对象来定义。钩子函数会接收到指令所绑定元素作为其参数。

### 11.1 自定义指令定义和使用

学习：app.directive()、directives 选项

当一个 input 元素被 Vue 插入到 DOM 中后，它会被自动聚焦：

完整案例：`59_自定义指令.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>59_自定义指令</title>
</head>
<body>
  <div id="app">
    <input type="text" v-focus/>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const app = Vue.createApp({
    directives: {
      focus: {
        mounted (el) {
          el.focus()
        }
      }
    }
  })

  // 全局自定义指令
  // app.directive('focus', { // 一个自定义指令由一个包含类似组件生命周期钩子的对象来定义。
  //   mounted (el) { // el 就是当前指令对应的DOM节点
  //     el.focus()
  //   }
  // })

  app.mount('#app')
</script>
</html>
```

### 11.2自定义指令钩子

一个指令的定义对象可以提供几种钩子函数 (都是可选的)：

> vue3相比 vue2，钩子函数做了更新
>
> vue2中一个指令定义对象可以提供如下几个钩子函数 (均为可选)：
>
> - `bind`：只调用一次，指令第一次绑定到元素时调用。在这里可以进行一次性的初始化设置。
> - `inserted`：被绑定元素插入父节点时调用 (仅保证父节点存在，但不一定已被插入文档中)。
> - `update`：所在组件的 VNode 更新时调用，**但是可能发生在其子 VNode 更新之前**。指令的值可能发生了改变，也可能没有。但是你可以通过比较更新前后的值来忽略不必要的模板更新 (详细的钩子函数参数见下)。
>
> - `componentUpdated`：指令所在组件的 VNode **及其子 VNode** 全部更新后调用。
> - `unbind`：只调用一次，指令与元素解绑时调用。

```js
// vue3钩子函数
const myDirective = {
  // 在绑定元素的 attribute 前
  // 或事件监听器应用前调用
  created(el, binding, vnode, prevVnode) {
    // 下面会介绍各个参数的细节
  },
  // 在元素被插入到 DOM 前调用
  beforeMount(el, binding, vnode, prevVnode) {},
  // 在绑定元素的父组件
  // 及他自己的所有子节点都挂载完成后调用
  mounted(el, binding, vnode, prevVnode) {},
  // 绑定元素的父组件更新前调用
  beforeUpdate(el, binding, vnode, prevVnode) {},
  // 在绑定元素的父组件
  // 及他自己的所有子节点都更新后调用
  updated(el, binding, vnode, prevVnode) {},
  // 绑定元素的父组件卸载前调用
  beforeUnmount(el, binding, vnode, prevVnode) {},
  // 绑定元素的父组件卸载后调用
  unmounted(el, binding, vnode, prevVnode) {}
}
```

指令的钩子会传递以下几种参数：

- `el`：指令绑定到的元素。这可以用于直接操作 DOM。
- `binding`：一个对象，包含以下属性。
  - `value`：传递给指令的值。例如在 `v-my-directive="1 + 1"` 中，值是 `2`。
  - `oldValue`：之前的值，仅在 `beforeUpdate` 和 `updated` 中可用。无论值是否更改，它都可用。
  - `arg`：传递给指令的参数 (如果有的话)。例如在 `v-my-directive:foo` 中，参数是 `"foo"`。
  - `modifiers`：一个包含修饰符的对象 (如果有的话)。例如在 `v-my-directive.foo.bar` 中，修饰符对象是 `{ foo: true, bar: true }`。
  - `instance`：使用该指令的组件实例。
  - `dir`：指令的定义对象。
- `vnode`：代表绑定元素的底层 VNode。
- `prevNode`：之前的渲染中代表指令所绑定元素的 VNode。仅在 `beforeUpdate` 和 `updated` 钩子中可用。

给一个元素设置颜色 v-red v-color="green",设置手机号正确为绿色，不正确为红色

完整案例`60_directives_demo.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>60_自定义指令demo</title>
</head>
<body>
  <div id="app">
    <div v-red>自定义指令 无参数 设置为红色</div>
    <!-- green需要添加 '' 否则被视为 变量 -->
    <div v-color="'green'">自定义指令 有参数 设置为绿色</div>
    <div v-color="'blue'">自定义指令 有参数 设置为蓝色</div>
    <input type="text" v-model="tel" v-tel/>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const app = Vue.createApp({
    data () {
      return {
        tel: ''
      }
    },
    directives: {
      tel: { // 手机号输入时判断是否符合规则，如果符合 显示为绿色 否则为红色
        // 确保输入 使用 updated 钩子
        updated (el) {
          if (/^(?:(?:\+|00)86)?1[3-9]\d{9}$/.test(el.value)) {
            el.style.color = 'green'
          } else {
            el.style.color = 'red'
          }
        }
      }
    }
  })

  app.directive('red', {
    mounted (el) {
      el.style.color = 'red'
    }
  })
  app.directive('color', {
    mounted (el, binding) { // binding.value 指令传递的值
      el.style.color = binding.value
    }
  })

  app.mount('#app')
</script>
</html>
```

> 自定义指令更多的用来实现DOM相关操作

> 上述案例中如果在vue2中，使用 inserted 代替 mounted，使用 update代替updated

## 12.插件

学习：插件开发与原理（app.use()）

插件 (Plugins) 是一种能为 Vue 添加全局功能的工具代码。下面是如何安装一个插件的示例：

```
import { createApp } from 'vue'

const app = createApp({})

app.use(myPlugin, {
  /* 可选的选项 */
})
```

一个插件可以是一个拥有 `install()` 方法的对象，也可以直接是一个安装函数本身。安装函数会接收到安装它的[应用实例](https://cn.vuejs.org/api/application.html)和传递给 `app.use()` 的额外选项作为参数：

```
const myPlugin = {
  install(app, options) {
    // 配置此应用
  }
}
```

插件没有严格定义的使用范围，但是插件发挥作用的常见场景主要包括以下几种：

1. 通过 [`app.component()`](https://cn.vuejs.org/api/application.html#app-component) 和 [`app.directive()`](https://cn.vuejs.org/api/application.html#app-directive) 注册一到多个全局组件或自定义指令。
2. 通过 [`app.provide()`](https://cn.vuejs.org/api/application.html#app-provide) 使一个资源[可被注入](https://cn.vuejs.org/guide/components/provide-inject.html)进整个应用。
3. 向 [`app.config.globalProperties`](https://cn.vuejs.org/api/application.html#app-config-globalproperties) 中添加一些全局实例属性或方法（`区别vue2的场景`）
4. 一个可能上述三种都包含了的功能库 (例如 [vue-router](https://github.com/vuejs/vue-router-next))。

完整案例：`61_plugin_vue3.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>61_插件的自定义vue3</title>
</head>
<body>
  <div id="app">
    {{ $t('welcome') }} - {{ $t('bye') }}
    <button @click="lang='zh'">中文</button>
    <button @click="lang='en'">英文</button>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>

  const { createApp } = Vue
  // 定义插件
  const i18nPlugin = {
    install (app, options) {
      console.log(options)
      app.config.globalProperties.$t = function (item)  { // item welcome
        return options[this.$root.lang][item]
      }
    }
  }

  const app = createApp({
    data () {
      return {
        lang: 'en'
      }
    }
  })

  // 使用插件
  app.use(i18nPlugin, {
    'en': { welcome: 'welcome', bye: 'goodbye' },
    'zh': { welcome: '欢迎', bye: '拜拜' }
  })

  app.mount('#app')
</script>
</html>
```

`62_plugin_vue2.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>62_插件的自定义vue2</title>
</head>
<body>
  <div id="app">
    {{ $t('welcome') }} - {{ $t('bye') }}
    <button @click="lang='zh'">中文</button>
    <button @click="lang='en'">英文</button>
  </div>
</body>
<script src="lib/vue.js"></script>
<script>

  // 定义插件
  const i18nPlugin = {
    install (app, options) {
      console.log(options)
      Vue.prototype.$t = function (item)  { // item welcome
        return options[this.$root.lang][item]
      }
    }
  }
  // 使用插件
  Vue.use(i18nPlugin, {
    'en': { welcome: 'welcome', bye: 'goodbye' },
    'zh': { welcome: '欢迎', bye: '拜拜' }
  })

  new Vue({
    data () {
      return {
        lang: 'en'
      }
    }
  }).$mount('#app')
</script>
</html>
```



## 13.vue3过渡效果

学习：Vue3过渡效果开发（内置组件 <Transition>、内置组件 <TransitionGroup>）

Vue 提供了两个内置组件，可以帮助你制作基于状态变化的过渡和动画：

- `<Transition>` 会在一个元素或组件进入和离开 DOM 时应用动画。本章节会介绍如何使用它。
- `<TransitionGroup>` 会在一个 `v-for` 列表中的元素或组件被插入，移动，或移除时应用动画。

除了这两个组件，我们也可以通过其他技术手段来应用动画，比如切换 CSS class 或用状态绑定样式来驱动动画。

### 13.1 `<Transition>` 组件

`<Transition>` 是一个内置组件，这意味着它在任意别的组件中都可以被使用，无需注册。它可以将进入和离开动画应用到通过默认插槽传递给它的元素或组件上。进入或离开可以由以下的条件之一触发：

- 由 `v-if` 所触发的切换
- 由 `v-show` 所触发的切换
- 由特殊元素 `<component>` 切换的动态组件

以下是最基本用法的示例：

```html
<button @click="show = !show">Toggle</button>
<Transition>
  <p v-if="show">hello</p>
</Transition>
```

```css
/* 下面我们会解释这些 class 是做什么的 */
.v-enter-active,
.v-leave-active {
  transition: opacity 0.5s ease;
}

.v-enter-from,
.v-leave-to {
  opacity: 0;
}
```

> `<Transition>` 仅支持单个元素或组件作为其插槽内容。如果内容是一个组件，这个组件必须仅有一个根元素。

`63_transition.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>63_过渡效果</title>
  <style>
    .v-enter-from { opacity: 0; transform: translateX(100px);}
    .v-enter-active { transition: all 5s; }
    .v-enter-to { opacity: 1; transform: translateX(0); }
    .v-leave-from { opacity: 1; transform: translateX(0); }
    .v-leave-active { transition: all 5s; }
    .v-leave-to { opacity: 0; transform: translateX(-100px); }
  </style>
</head>
<body>
  <div id="app">
    <button @click="show = !show">切换</button>
    <!-- 默认需要实现6个样式
    v-enter-from
    v-enter-active
    v-enter-to
    v-leave-from
    v-leave-active
    v-leave-to
    -->
    <Transition>
      <h1 v-if="show">显示</h1>
    </Transition>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  Vue.createApp({
    data () {
      return {
        show: false
      }
    }
  }).mount('#app')
</script>
</html>
```



### 13.2 CSS 过渡 class

一共有 6 个应用于进入与离开过渡效果的 CSS class。

![过渡图示](assets/transition-classes.f0f7b3c9.png)

1. `v-enter-from`：进入动画的起始状态。在元素插入之前添加，在元素插入完成后的下一帧移除。
2. `v-enter-active`：进入动画的生效状态。应用于整个进入动画阶段。在元素被插入之前添加，在过渡或动画完成之后移除。这个 class 可以被用来定义进入动画的持续时间、延迟与速度曲线类型。
3. `v-enter-to`：进入动画的结束状态。在元素插入完成后的下一帧被添加 (也就是 `v-enter-from` 被移除的同时)，在过渡或动画完成之后移除。
4. `v-leave-from`：离开动画的起始状态。在离开过渡效果被触发时立即添加，在一帧后被移除。
5. `v-leave-active`：离开动画的生效状态。应用于整个离开动画阶段。在离开过渡效果被触发时立即添加，在过渡或动画完成之后移除。这个 class 可以被用来定义离开动画的持续时间、延迟与速度曲线类型。
6. `v-leave-to`：离开动画的结束状态。在一个离开动画被触发后的下一帧被添加 (也就是 `v-leave-from` 被移除的同时)，在过渡或动画完成之后移除。

`v-enter-active` 和 `v-leave-active` 给我们提供了为进入和离开动画指定不同速度曲线的能力

### 13.3 为过渡效果命名

我们可以给 `<Transition>` 组件传一个 `name` prop 来声明一个过渡效果名：

```
<Transition name="fade">
  ...
</Transition>
```

对于一个有名字的过渡效果，对它起作用的过渡 class 会以其名字而不是 `v` 作为前缀。比如，上方例子中被应用的 class 将会是 `fade-enter-active` 而不是 `v-enter-active`。这个“fade”过渡的 class 应该是这样：

```
.fade-enter-active,
.fade-leave-active {
  transition: opacity 0.5s ease;
}

.fade-enter-from,
.fade-leave-to {
  opacity: 0;
}
```

> 可以结合 css3中的 `transition`以及`animation`实现动画效果

`64_transition_name.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>64_过渡效果_name</title>
  <style>
    .slide-enter-from { opacity: 0; transform: translateX(100px);}
    .slide-enter-active { transition: all 5s; }
    .slide-enter-to { opacity: 1; transform: translateX(0); }
    .slide-leave-from { opacity: 1; transform: translateX(0); }
    .slide-leave-active { transition: all 5s; }
    .slide-leave-to { opacity: 0; transform: translateX(-100px); }

    /* .fade-enter-from { opacity: 0;}
    .fade-enter-active { transition: all 5s; }
    .fade-enter-to { opacity: 1;  }
    .fade-leave-from { opacity: 1; }
    .fade-leave-active { transition: all 5s; }
    .fade-leave-to { opacity: 0;  } */
    .fade-enter-from, .fade-leave-to { opacity: 0; }
    .fade-enter-active, .fade-leave-active { transition: all 5s; }
    .fade-enter-to, .fade-leave-from { opacity: 1; }

  </style>
</head>
<body>
  <div id="app">
    <button @click="show = !show">切换</button>
    <!-- 默认需要实现6个样式
    v-enter-from
    v-enter-active
    v-enter-to
    v-leave-from
    v-leave-active
    v-leave-to
    -->
    <Transition name="slide">
      <h1 v-if="show">显示</h1>
    </Transition>
    <Transition name="fade">
      <h1 v-if="show">显示</h1>
    </Transition>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  Vue.createApp({
    data () {
      return {
        show: false
      }
    }
  }).mount('#app')
</script>
</html>
```

> 更多信息请参考：https://cn.vuejs.org/guide/built-ins/transition.html#the-transition-component

## 14.Teleport传送门

学习：开发modal组件（<Teleport />内置组件）

> vue3 新增，模仿了 react中 Portal

`<Teleport>` 是一个内置组件，它可以将一个组件内部的一部分模板“传送”到该组件的 DOM 结构外层的位置去。

有时我们可能会遇到这样的场景：一个组件模板的一部分在逻辑上从属于该组件，但从整个应用视图的角度来看，它在 DOM 中应该被渲染在整个 Vue 应用外部的其他地方。

这类场景最常见的例子就是全屏的模态框。理想情况下，我们希望触发模态框的按钮和模态框本身是在同一个组件中，因为它们都与组件的开关状态有关。但这意味着该模态框将与按钮一起渲染在应用 DOM 结构里很深的地方。这会导致该模态框的 CSS 布局代码很难写。

`65_model.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>65_teleport传送门</title>
  <style>
    .modal {
      position: fixed;
      left: 0;
      right: 0;
      bottom: 0;
      top: 0;
      background-color: rgba(0,0,0,0.4);
    }
    .modal .box {
      width: 50%;
      min-height: 300px;
      background-color: #fff;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
    }
  </style>
</head>
<body>
  <div id="app">
    <button @click="open=true">打开模态框</button>
    <!-- 审查元素还在 div#app 内部 -->
    <!-- <my-modal v-if="open" @close="close"></my-modal> -->

    <!-- 审查元素得知 位置发生了改变，放大了 body 标签的最底下 -->
    <teleport to="body">
      <my-modal v-if="open" @close="close"></my-modal>
    </teleport>
  </div>
</body>
<template id="modal">
  <div class="modal" @click.self="$emit('close')">
    <div class="box">
      <p>Hello from the modal!</p>
      <button @click="$emit('close')">Close</button>
    </div>
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const Modal = {
    template: '#modal'
  }
  Vue.createApp({
    components: {
      MyModal: Modal
    },
    data () {
      return {
        open: false
      }
    },
    methods: {
      close () {
        this.open = false
      }
    }
  }).mount('#app')
</script>
</html>
```

> 通过给组件模板添加 `<teleport >`标签审查元素查看结果

## 15.两个重要的实例API

### 15.1 $forceUpdate()

强制该组件重新渲染。

鉴于 Vue 的全自动响应性系统，这个功能应该很少会被用到。唯一可能需要它的情况是，你使用高阶响应式 API 显式创建了一个非响应式的组件状态。

> vue2中对象以及数组的操作 数据改变视图没有更新，可以使用 forceUpdate 进行强制更新视图

`66_forceupdate_vue2.html` 不添加 forceUpdate 和添加之后对比查看效果

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>66_forceupdate_vue2</title>
</head>
<body>
  <div id="app">
    <button @click="change(2)">改变3的数据</button>
    <button @click="clear">清空数组</button>
    <button @click="test">改变obj</button>
    <ul>
      <li v-for="(item, index) of list" :key="index">{{item}}</li>
    </ul>
    {{ arr[2].c }}
    <ul>
      <li v-for="(item, index) of arr" :key="index">{{item.a}} - {{ item.b }}</li>
    </ul>
    {{ obj.foo }} - {{ obj.bar }}
  </div>
</body>
<script src="lib/vue.js"></script>
<script>
  new Vue({
    data: {
      list: ['a', 'b', 'c'],
      arr: [
        { a: 1, b:2 },
        { a: 10, b: 20 },
        { a: 100, b: 200 }
      ],
      obj: {
        foo: 'foo'
      }
    },
    methods: {
      change (index) {
        this.arr[index].c = 300 // 给对象数组添加额外的属性
        this.$forceUpdate()
      }, 
      clear () {
        this.list.length = 0 // 数组长度设置为0 清空数组
        this.$forceUpdate()
      },
      test () {
        this.obj.bar = 'bar' // 对象添加额外的属性
        console.log(this.obj)
        this.$forceUpdate()
      }
    }
  }).$mount('#app')
</script>
</html>
```

> 在vue2中如果只是为了数据改变更新视图，还可以使用 $set 将响应式数据中添加额外的属性也变为响应式
>
> `66_set_vue2.html`
>
> ```html
> <!DOCTYPE html>
> <html lang="en">
> <head>
>   <meta charset="UTF-8">
>   <meta http-equiv="X-UA-Compatible" content="IE=edge">
>   <meta name="viewport" content="width=device-width, initial-scale=1.0">
>   <title>66_vue $set</title>
> </head>
> <body>
>   <div id="app">
>     <button @click="change(2)">改变3的数据</button>
>     <button @click="clear">清空数组</button>
>     <button @click="test">改变obj</button>
>     <ul>
>       <li v-for="(item, index) of list" :key="index">{{item}}</li>
>     </ul>
>     {{ arr[2].c }}
>     <ul>
>       <li v-for="(item, index) of arr" :key="index">{{item.a}} - {{ item.b }}</li>
>     </ul>
>     {{ obj.foo }} - {{ obj.bar }}
>   </div>
> </body>
> <script src="lib/vue.js"></script>
> <script>
>   new Vue({
>     data: {
>       list: ['a', 'b', 'c'],
>       arr: [
>         { a: 1, b:2 },
>         { a: 10, b: 20 },
>         { a: 100, b: 200 }
>       ],
>       obj: {
>         foo: 'foo'
>       }
>     },
>     methods: {
>       change (index) {
>         // this.arr[index].c = 300 // 给对象数组添加额外的属性
>         this.$set(this.arr[index], 'c', 300) // 给对象数组添加额外的属性
>       }, 
>       clear () {
>         // this.list.length = 0 // 数组长度设置为0 清空数组
>         this.list = []
>       },
>       test () {
>         // this.obj.bar = 'bar' // 对象添加额外的属性
>         this.$set(this.obj, 'bar', 'bar')
>         console.log(this.obj)
>       }
>     }
>   }).$mount('#app')
> </script>
> </html>
> ```
>
> 

> vue3中的forceUpdate 一般都用不到，在vue2中实现不了的以上案例，vue3全部支持
>
> `66_forceupdate_vue3.html`
>
> ```html
> <!DOCTYPE html>
> <html lang="en">
> <head>
>   <meta charset="UTF-8">
>   <meta http-equiv="X-UA-Compatible" content="IE=edge">
>   <meta name="viewport" content="width=device-width, initial-scale=1.0">
>   <title>66_forceupdate_vue3</title>
> </head>
> <body>
>   <div id="app">
>     <button @click="change(2)">改变3的数据</button>
>     <button @click="clear">清空数组</button>
>     <button @click="test">改变obj</button>
>     <ul>
>       <li v-for="(item, index) of list" :key="index">{{item}}</li>
>     </ul>
>     {{ arr[2].c }}
>     <ul>
>       <li v-for="(item, index) of arr" :key="index">{{item.a}} - {{ item.b }}</li>
>     </ul>
>     {{ obj.foo }} - {{ obj.bar }}
>   </div>
> </body>
> <script src="lib/vue.global.js"></script>
> <script>
>   const app = Vue.createApp({
>     data () {
>       return {
>         list: ['a', 'b', 'c'],
>         arr: [
>           { a: 1, b:2 },
>           { a: 10, b: 20 },
>           { a: 100, b: 200 }
>         ],
>         obj: {
>           foo: 'foo'
>         }
>       }
>     },
>     methods: {
>       change (index) {
>         this.arr[index].c = 300 // 给对象数组添加额外的属性
>       }, 
>       clear () {
>         this.list.length = 0 // 数组长度设置为0 清空数组
>       },
>       test () {
>         this.obj.bar = 'bar' // 对象添加额外的属性
>         console.log(this.obj)
>       }
>     }
>   })
> 
>   app.mount('#app')
> </script>
> </html>
> ```
>
> > vue3中没有了 $set

### 15.2 $nextTick()

绑定在实例上的 [`nextTick()`](https://cn.vuejs.org/api/general.html#nexttick) 函数。

和全局版本的 `nextTick()` 的唯一区别就是组件传递给 `this.$nextTick()` 的回调函数会带上 `this` 上下文，其绑定了当前组件实例。

`67_nextTick.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>67_nextTick</title>
</head>
<body>
  <div id="app">
    <div ref="oDiv">{{ count }}</div>
    <button @click="add">加1</button>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const app = Vue.createApp({
    data () {
      return {
        count: 1
      }
    },
    methods: {
      add () {
        this.count++
        console.log(this.count) // 2
        console.log(this.$refs.oDiv.innerHTML) // 1
        this.$nextTick(() => { // 可以获取真实的DOM节点的值，瀑布流布局
          console.log(this.$refs.oDiv.innerHTML) // 2
        })
      }
    }
  })

  app.mount('#app')
</script>
</html>
```



## 16.渲染函数

### 16.1 h()

创建虚拟 DOM 节点 (vnode)。

Vue 提供了一个 `h()` 函数用于创建 vnodes：

```
import { h } from 'vue'

const vnode = h(
  'div', // type
  { id: 'foo', class: 'bar' }, // props
  [
    /* children */
  ]
)
```

`h()` 是 **hyperscript** 的简称——意思是“能生成 HTML (超文本标记语言) 的 JavaScript”。这个名字来源于许多虚拟 DOM 实现默认形成的约定。一个更准确的名称应该是 `createVnode()`，但当你需要多次使用渲染函数时，一个简短的名字会更省力。

`h()` 函数的使用方式非常的灵活：

创建原生元素：

```
// 除了类型必填以外，其他的参数都是可选的
h('div')
h('div', { id: 'foo' })

// attribute 和 property 都能在 prop 中书写
// Vue 会自动将它们分配到正确的位置
h('div', { class: 'bar', innerHTML: 'hello' })

// 类与样式可以像在模板中一样
// 用数组或对象的形式书写
h('div', { class: [this.foo], style: { color: 'red' } })

// 事件监听器应以 onXxx 的形式书写
h('div', { onClick: () => {} })

// children 可以是一个字符串
h('div', { id: 'foo' }, 'hello')

// 没有 props 时可以省略不写
h('div', 'hello')
h('div', [h('span', 'hello')])

// children 数组可以同时包含 vnodes 与字符串
h('div', ['hello', h('span', 'hello')])
```

创建组件：

```js
  const Header = {
    template: `<header>1头部</header>`
  }
  const Content = {
    template: `<div>1内容</div>`
  }
  const Footer = {
    template: `<footer>1底部</footer>`
  }
  // h也可以创建组件
  const Container = {
    render () {
      return [
        h('div', { class: 'box' }, [
          h(Header, { class: 'header' }),
          h(Content, { class: 'content' }),
          h(Footer, { class: 'footer' }),
        ])
      ]
    }
  }

```

`68_h.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>68_h</title>
</head>
<body>
  <div id="app">
    <my-com>11</my-com>

    <my-container>
      <my-header></my-header>
      <my-content></my-content>
      <my-footer></my-footer>
    </my-container>
  </div>
</body>
<template>
  <div class="container">
    <!-- <header class="header">头部</header> -->
    <header class="header">
      <slot></slot>
    </header>
    <div class="content" id="content">内容</div>
    <footer class="footer">
      <ul>
        <li @click="myAlert(1)">首页</li>
        <li @click="myAlert(2)">分类</li>
        <li @click="myAlert(3)">购物车</li>
        <li @click="myAlert(4)">我的</li>
      </ul>
    </footer>
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, h } = Vue

  // h 创建HTML元素
  const Com = {
    render () {
      return [ // h 第一个参数是必须的，第二表示属性，如果没有，可以省略，如果有写为对象，第三个参数 子元素
        h('div', { class: 'container' }, [
          // h('header', { class: 'header' }, '头部'),
          h('header', { class: 'header' }, this.$slots.default()),
          h('div', { class: 'content', id: 'content' }, '内容'),
          h('footer', { class: 'footer' }, [
            h('ul', [
              h('li', { onClick: () => { this.myAlert(1) } }, '首页'),
              h('li', { onClick: () => { this.myAlert(2) } }, '分类'),
              h('li', { onClick: () => { this.myAlert(3) } }, '购物车'),
              h('li', { onClick: () => { this.myAlert(4) } }, '我的')
            ])
          ])
        ])
      ]
    },
    methods: {
      myAlert (num) {
        console.log(num)
      }
    }
  }
  const Header = {
    template: `<header>1头部</header>`
  }
  const Content = {
    template: `<div>1内容</div>`
  }
  const Footer = {
    template: `<footer>1底部</footer>`
  }
  // h也可以创建组件
  const Container = {
    render () {
      return [
        h('div', { class: 'box' }, [
          h(Header, { class: 'header' }),
          h(Content, { class: 'content' }),
          h(Footer, { class: 'footer' }),
        ])
      ]
    }
  }

  const app = createApp({
    components: {
      MyCom: Com,
      MyContainer: Container
    }
  })
  
  app.mount('#app')
</script>
</html>
```



### 16.2 mergeProps()

合并多个 props 对象，用于处理含有特定的 props 参数的情况。

`mergeProps()` 支持以下特定 props 参数的处理，将它们合并成一个对象。

- `class`
- `style`
- `onXxx` 事件监听器——多个同名的事件监听器将被合并到一个数组。

如果你不需要合并行为而是简单覆盖，可以使用原生 `object spread` 语法来代替

```js
import { mergeProps } from 'vue'

const one = {
  class: 'foo',
  onClick: handlerA
}

const two = {
  class: { bar: true },
  onClick: handlerB
}

const merged = mergeProps(one, two)
/**
 {
   class: 'foo bar',
   onClick: [handlerA, handlerB]
 }
 */
```

`69_mergeProps.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>69_mergeProps</title>
</head>
<body>
  <div id="app">
    <div :class="test.class" @click="test.onClick">测试</div>
  </div>
</body>

<script src="lib/vue.global.js"></script>
<script>
  const { createApp, mergeProps } = Vue

  const one = {
    class: 'foo',
    onClick: () => {
      console.log(1)
    }
  }

  const two = {
    class: { bar: true },
    onClick: () => {
      console.log(2)
    }
  }
  const test = mergeProps(one, two)
  console.log(test) // { class: 'foo bar', onClick: [() => {console.log(1)}, () => {console.log(2)}]}
  const app = createApp({
    data () {
      return {
        test
      }
    }
  })
  
  app.mount('#app')
</script>
</html>
```



### 16.3 cloneVNode()

克隆一个 vnode。

返回一个克隆的 vnode，可在原有基础上添加一些额外的 prop。

Vnode 被认为是一旦创建就不能修改的，你不应该修改已创建的 vnode 的 prop，而应该附带不同的/额外的 prop 来克隆它。

Vnode 具有特殊的内部属性，因此克隆它并不像 object spread 一样简单。`cloneVNode()` 处理了大部分这样的内部逻辑。

```
import { h, cloneVNode } from 'vue'

const original = h('div')
const cloned = cloneVNode(original, { id: 'foo' })
```

`70_cloneVNode.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>70_cloneVNode</title>
</head>
<body>
  <div id="app">
    <my-header></my-header>
    <hr />
    <!-- Invalid VNode type: undefined (undefined)  -->
    <new-header></new-header>
  </div>
</body>

<script src="lib/vue.global.js"></script>
<script>
  const { createApp, cloneVNode, h } = Vue

  const old = h('div')
  // 克隆元素
  const cloned = cloneVNode(old, { id: 'box'})

  console.log('原始的', old) //{ props: null}
  console.log('克隆的元素', cloned) // {props: { id: 'box' }}


  // 克隆组件--失败 --- cloneVNode 克隆组件的返回值不是一个组件
  const Header = {
    template: `<header>1头部</header>`
  }
  console.log('原始组件', Header)
  const NewHeader = cloneVNode(Header)
  console.log('克隆组件', NewHeader)

  const app = createApp({
    components: {
      MyHeader: Header,
      NewHeader: NewHeader
    }
  })
  
  app.mount('#app')
</script>
</html>
```

> cloneVNode 函数不可以克隆组件，返回值不是一个组件

## 17.自定义过滤器

vue3移除了了自定义过滤器 https://v2.cn.vuejs.org/v2/guide/filters.html

> 页面展示 男 女  数据库中存 1 0，接口返回为 1 和 0,如何展示
>
> 国际化，中国 ￥  美国 $

`71_vue2_filters.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>71_vue2_过滤器</title>
</head>
<body>
  <div id="app">
    {{ sex }} - {{ sex | sexFilter }} - {{ price }} - {{ price | priceFilter('$') }} - {{ price | priceFilter('￥') }}
  </div>
</body>
<script src="lib/vue.js"></script>
<script>
  // 全局过滤器
  Vue.filter('sexFilter', (val) => {
    return val === 1 ? '男' : '女'
  })
  new Vue({
    data: {
      sex: 1, // 1 男  0 女
      price: 100
    },
    filters: {
      priceFilter (val, type) {
        return type + val
      }
    }
  }).$mount('#app')
</script>
</html>
```

> vue3 没有过滤器
>
> `72_change_filters.html`
>
> ```html
> <!DOCTYPE html>
> <html lang="en">
> <head>
>   <meta charset="UTF-8">
>   <meta http-equiv="X-UA-Compatible" content="IE=edge">
>   <meta name="viewport" content="width=device-width, initial-scale=1.0">
>   <title>72_vue3_代替过滤器</title>
> </head>
> <body>
>   <div id="app">
>     {{ sex }} - {{ newSex }} - {{ price }} - {{zhPrice }} - {{ enPrice }}
>   </div>
> </body>
> <script src="lib/vue.global.js"></script>
> <script>
>   Vue.createApp({
>     data(){
>       return  {
>         sex: 1, // 1 男  0 女
>         price: 100
>       }
>     },
>     computed: {
>       newSex () {
>         return this.sex === 1 ? '男' : '女'
>       },
>       zhPrice () {
>         return '￥' + this.price
>       },
>       enPrice () {
>         return '$' + this.price
>       }
>     }
>   }).mount('#app')
> </script>
> </html>
> ```
>
> 

## 18 混入mixins

一个包含组件选项对象的数组，这些选项都将被混入到当前组件的实例中。

`mixins` 选项接受一个 mixin 对象数组。这些 mixin 对象可以像普通的实例对象一样包含实例选项，它们将使用一定的选项合并逻辑与最终的选项进行合并。举例来说，如果你的 mixin 包含了一个 `created` 钩子，而组件自身也有一个，那么这两个函数都会被调用。

Mixin 钩子的调用顺序与提供它们的选项顺序相同，且会在组件自身的钩子前被调用。

> 在 Vue 2 中，mixins 是创建可重用组件逻辑的主要方式。尽管在 Vue 3 中保留了 mixins 支持，但对于组件间的逻辑复用，[Composition API](https://cn.vuejs.org/guide/reusability/composables.html) 是现在更推荐的方式。

`73_mixins.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>73_mixins</title>
</head>
<body>
  <div id="app">
    <my-child1></my-child1>
    <hr/>
    <my-child2></my-child2>
  </div>
</body>
<template id="child1">
  <div>
    <h1>child1组件</h1>
    {{ count }} <button @click="add">加1</button>
  </div>
</template>
<template id="child2">
  <div>
    <h1>child2组件</h1>
    {{ count }} <button @click="add">加1</button>
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp } = Vue

  const myMixin = {
    data () {
      return {
        count: 10
      }
    },
    methods: {
      add () {
        this.count++
      }
    },
    mounted () {
      console.log('1', this.count)
    }
  }

  const Child1 = {
    // 局部混入
    mixins: [myMixin],
    template: '#child1',
    data () { // 初始化数据相同，组件的 优先级高于 混入
      return {
        count: 100
      }
    },
    mounted () {
      console.log('2', this.count)
    }
  }

  const Child2 = {
    // 局部混入
    mixins: [myMixin],
    template: '#child2',
    methods: {
      add () { // 函数名相同， 组件的优先级高于 混入
        this.count += 10
      }
    },
    mounted () { // 相同的生命周期的钩子函数，先执行混入的代码，再执行组件的代码
      console.log('3', this.count)
    }
  }

  const app = createApp({
    components: {
      MyChild1: Child1,
      MyChild2: Child2
    }
  })

  // 全局混入 // 不建议使用全局混入,会给所有的组件都加入部分代码
  // app.mixin(myMixin)

  app.mount('#app')
</script>
</html>
```

> 除了生命周期钩子函数外，所有的代码都以组件为优先
>
> 生命周期钩子函数先执行 混入的，后执行组件的

# 三、vue3组合式API

## 1、组合式API

### 1.1 什么是组合式API

组合式 API (Composition API) 是一系列 API 的集合，使我们可以使用函数而不是声明选项的方式书写 Vue 组件。它是一个概括性的术语，涵盖了以下方面的 API：

- [响应式 API](https://cn.vuejs.org/api/reactivity-core.html)：例如 `ref()` 和 `reactive()`，使我们可以直接创建响应式状态、计算属性和侦听器。
- [生命周期钩子](https://cn.vuejs.org/api/composition-api-lifecycle.html)：例如 `onMounted()` 和 `onUnmounted()`，使我们可以在组件各个生命周期阶段添加逻辑。
- [依赖注入](https://cn.vuejs.org/api/composition-api-dependency-injection.html)：例如 `provide()` 和 `inject()`，使我们可以在使用响应式 API 时，利用 Vue 的依赖注入系统。

组合式 API 是 Vue 3 及 [Vue 2.7](https://blog.vuejs.org/posts/vue-2-7-naruto.html) 的内置功能。对于更老的 Vue 2 版本，可以使用官方维护的插件 [`@vue/composition-api`](https://github.com/vuejs/composition-api)。在 Vue 3 中，组合式 API 基本上都会配合 [``](https://cn.vuejs.org/api/sfc-script-setup.html) 语法在单文件组件中使用。

### 1.2 为什么使用它

#### 1.2.1 更好的逻辑复用[#](https://cn.vuejs.org/guide/extras/composition-api-faq.html#better-logic-reuse)

组合式 API 最基本的优势是它使我们能够通过[组合函数](https://cn.vuejs.org/guide/reusability/composables.html)来实现更加简洁高效的逻辑复用。在选项式 API 中我们主要的逻辑复用机制是 mixins，而组合式 API 解决了 [mixins 的所有缺陷](https://cn.vuejs.org/guide/reusability/composables.html#vs-mixins)。

组合式 API 提供的逻辑复用能力孵化了一些非常棒的社区项目，比如 [VueUse](https://vueuse.org/)，一个不断成长的工具型组合式函数集合。组合式 API 还为其他第三方状态管理库与 Vue 的响应式系统之间的集成提供了一套简洁清晰的机制，例如 [RxJS](https://vueuse.org/rxjs/readme.html#vueuse-rxjs)。

#### 1.2.2更灵活的代码组织[#](https://cn.vuejs.org/guide/extras/composition-api-faq.html#more-flexible-code-organization)

许多用户喜欢选项式 API 的原因是因为它在默认情况下就能够让人写出有组织的代码：大部分代码都自然地被放进了对应的选项里。然而，选项式 API 在单个组件的逻辑复杂到一定程度时，会面临一些无法忽视的限制。这些限制主要体现在需要处理多个**逻辑关注点**的组件中，这是我们在许多 Vue 2 的实际案例中所观察到的。

我们以 Vue CLI GUI 中的文件浏览器组件为例：这个组件承担了以下几个逻辑关注点：

- 追踪当前文件夹的状态，展示其内容
- 处理文件夹的相关操作 (打开、关闭和刷新)
- 支持创建新文件夹
- 可以切换到只展示收藏的文件夹
- 可以开启对隐藏文件夹的展示
- 处理当前工作目录中的变更

这个组件[最原始的版本](https://github.com/vuejs/vue-cli/blob/a09407dd5b9f18ace7501ddb603b95e31d6d93c0/packages/@vue/cli-ui/src/components/folder/FolderExplorer.vue#L198-L404)是由选项式 API 写成的。如果我们为相同的逻辑关注点标上一种颜色，那将会是这样：

![folder component before](https://user-images.githubusercontent.com/499550/62783021-7ce24400-ba89-11e9-9dd3-36f4f6b1fae2.png)

你可以看到，处理相同逻辑关注点的代码被强制拆分在了不同的选项中，位于文件的不同部分。在一个几百行的大组件中，要读懂代码中的一个逻辑关注点，需要在文件中反复上下滚动，这并不理想。另外，如果我们想要将一个逻辑关注点抽取重构到一个可复用的工具函数中，需要从文件的多个不同部分找到所需的正确片段。

而如果[用组合式 API 重构](https://gist.github.com/yyx990803/8854f8f6a97631576c14b63c8acd8f2e)这个组件，将会变成下面右边这样：

![重构后的文件夹组件](https://user-images.githubusercontent.com/499550/62783026-810e6180-ba89-11e9-8774-e7771c8095d6.png)

现在与同一个逻辑关注点相关的代码被归为了一组：我们无需再为了一个逻辑关注点在不同的选项块间来回滚动切换。此外，我们现在可以很轻松地将这一组代码移动到一个外部文件中，不再需要为了抽象而重新组织代码，大大降低了重构成本，这在长期维护的大型项目中非常关键。

#### 1.2.3 更好的类型推导[#](https://cn.vuejs.org/guide/extras/composition-api-faq.html#better-type-inference)

近几年来，越来越多的开发者开始使用 [TypeScript](https://www.typescriptlang.org/) 书写更健壮可靠的代码，TypeScript 还提供了非常好的 IDE 开发支持。然而选项式 API 是在 2013 年被设计出来的，那时并没有把类型推导考虑进去，因此我们不得不做了一些[复杂到夸张的类型体操](https://github.com/vuejs/core/blob/44b95276f5c086e1d88fa3c686a5f39eb5bb7821/packages/runtime-core/src/componentPublicInstance.ts#L132-L165)才实现了对选项式 API 的类型推导。但尽管做了这么多的努力，选项式 API 的类型推导在处理 mixins 和依赖注入类型时依然不甚理想。

因此，很多想要搭配 TS 使用 Vue 的开发者采用了由 `vue-class-component` 提供的 Class API。然而，基于 Class 的 API 非常依赖 ES 装饰器，在 2019 年我们开始开发 Vue 3 时，它仍是一个仅处于 stage 2 的语言功能。我们认为基于一个不稳定的语言提案去设计框架的核心 API 风险实在太大了，因此没有继续向 Class API 的方向发展。在那之后装饰器提案果然又发生了很大的变动，在 2022 年才终于到达 stage 3。另一个问题是，基于 Class 的 API 和选项式 API 在逻辑复用和代码组织方面存在相同的限制。

相比之下，组合式 API 主要利用基本的变量和函数，它们本身就是类型友好的。用组合式 API 重写的代码可以享受到完整的类型推导，不需要书写太多类型标注。大多数时候，用 TypeScript 书写的组合式 API 代码和用 JavaScript 写都差不太多！这也让许多纯 JavaScript 用户也能从 IDE 中享受到部分类型推导功能。

#### 1.2.4 更小的生产包体积[#](https://cn.vuejs.org/guide/extras/composition-api-faq.html#smaller-production-bundle-and-less-overhead)

搭配 `<script setup>` 使用组合式 API 比等价情况下的选项式 API 更高效，对代码压缩也更友好。这是由于 `<script setup>` 形式书写的组件模板被编译为了一个内联函数，和 `<script setup>` 中的代码位于同一作用域。不像选项式 API 需要依赖 `this` 上下文对象访问属性，被编译的模板可以直接访问 `<script setup>` 中定义的变量，无需一个代码实例从中代理。这对代码压缩更友好，因为本地变量的名字可以被压缩，但对象的属性名则不能。

### 1.3 第一个组合式API的例子

`74_composition.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>74_体验组合式API</title>
</head>
<body>
  <div id="app">
    <button @click="increment">点击了{{ count }}次</button>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, onMounted, onUpdated } = Vue

  const app = createApp({
    setup () { // 组合式API

      const count = ref(10)

      const increment = () => {
        console.log(1)
        count.value += 1
      }

      onMounted(() => { // 生命周期钩子函数
        // console.log(count)
        // console.log(`初始点击次数为${count.value}`)
        document.title = `点击次数为${count.value}`
      })

      onUpdated(() => {
        document.title = `点击次数为${count.value}`
      })

      return {
        count,
        increment
      }

    }
  })

  app.mount('#app')
</script>
</html>
```

> 体验提取公共部分
>
> `75_composition_hooks.html`
>
> ```html
> <!DOCTYPE html>
> <html lang="en">
> <head>
>   <meta charset="UTF-8">
>   <meta http-equiv="X-UA-Compatible" content="IE=edge">
>   <meta name="viewport" content="width=device-width, initial-scale=1.0">
>   <title>75_提取复用代码</title>
> </head>
> <body>
>   <div id="app">
>     <button @click="increment">点击了{{ count }}次</button>
>   </div>
> </body>
> <script src="lib/vue.global.js"></script>
> <script>
>   const { createApp, ref, onMounted, onUpdated } = Vue
> 
>   const useCount = () => {
>     const count = ref(10)
> 
>     const increment = () => {
>       console.log(1)
>       count.value += 1
>     }
> 
>     return {
>       count,
>       increment
>     }
>   }
> 
>   const useTitle = (count) => {
>     onMounted(() => { // 生命周期钩子函数
>       // console.log(count)
>       // console.log(`初始点击次数为${count.value}`)
>       document.title = `点击次数为${count.value}`
>     })
> 
>     onUpdated(() => {
>       document.title = `点击次数为${count.value}`
>     })
>   }
>   const app = createApp({
>     setup () { // 组合式API
>       const { count, increment } = useCount() // 自定义hooks
>       
>       useTitle(count) // 自定义hooks
> 
>       return {
>         count,
>         increment
>       }
> 
>     }
>   })
> 
>   app.mount('#app')
> </script>
> </html>
> ```
>
> 

## 2、setup()函数

`setup()` 钩子是在组件中使用组合式 API 的入口，通常只在以下情况下使用：

1. 需要在非单文件组件中使用组合式 API 时。
2. 需要在基于选项式 API 的组件中集成基于组合式 API 的代码时。

**其他情况下，都应优先使用 [`<script setup>`](https://cn.vuejs.org/api/sfc-script-setup.html) 语法。**

### 2.1 基本使用

我们可以使用[响应式 API](https://cn.vuejs.org/api/reactivity-core.html) 来声明响应式的状态，在 `setup()` 函数中返回的对象会暴露给模板和组件实例。其它的选项也可以通过组件实例来获取 `setup()` 暴露的属性

```vue
<script>
import { ref } from 'vue'

export default {
  setup() {
    const count = ref(0)

    // 返回值会暴露给模板和其他的选项式 API 钩子
    return {
      count
    }
  },

  mounted() {
    console.log(this.count) // 0
  }
}
</script>

<template>
  <button @click="count++">{{ count }}</button>
</template>
```



请注意在模板中访问从 `setup` 返回的 [ref](https://cn.vuejs.org/api/reactivity-core.html#ref) 时，它会[自动浅层解包](https://cn.vuejs.org/guide/essentials/reactivity-fundamentals.html#deep-reactivity)，因此你无须再在模板中为它写 `.value`。当通过 `this` 访问时也会同样如此解包。

> `setup()` 自身并不含对组件实例的访问权，即在 `setup()` 中访问 `this` 会是 `undefined`。你可以在选项式 API 中访问组合式 API 暴露的值，但反过来则不行。

`76_composition_setup_base.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>76_setup的基本使用</title>
</head>
<body>
  <div id="app">
    <!-- 0 -->
    {{ count }}
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, onMounted, onUpdated } = Vue

  const app = createApp({
    setup () { // 组合式API
      // 创建了响应式变量
      const count = ref(0)
     
      // 返回值会暴露给模板和其他的选项式 API 钩子
      return {
        count
      }
    },
    data () { // 虽然设置了同名的变量，但是显示的是 组合式API中的数据
      return {
        count: 100
      }
    },
    mounted () {
      console.log(this.count) // 0
    }
  })

  app.mount('#app')
</script>
</html>
```



### 2.2 访问 Prop

`setup` 函数的第一个参数是组件的 `props`。和标准的组件一致，一个 `setup` 函数的 `props` 是响应式的，并且会在传入新的 props 时同步更新。

```js
{
  props: {
    title: String,
    count: Number
  },
  setup(props) {
    console.log(props.title)
    console.log(props.count)
  }
}
```

> 请注意如果你解构了 `props` 对象，解构出的变量将会丢失响应性。因此我们推荐通过 `props.xxx` 的形式来使用其中的 props。

如果你确实需要解构 `props` 对象，或者需要将某个 prop 传到一个外部函数中并保持响应性，那么你可以使用 [toRefs()](https://cn.vuejs.org/api/reactivity-utilities.html#torefs) 和 [toRef()](https://cn.vuejs.org/api/reactivity-utilities.html#toref) 这两个工具函数：

```js
{
  setup(props) {
    // 将 `props` 转为一个其中全是 ref 的对象，然后解构
    const { title } = toRefs(props)
    // `title` 是一个追踪着 `props.title` 的 ref
    console.log(title.value)

    // 或者，将 `props` 的单个属性转为一个 ref
    const title = toRef(props, 'title')
  }
}
```

`77_composition_setup_props.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>77_setup访问props</title>
</head>
<body>
  <div id="app">
    {{ count }} <button @click="count++">加1</button>
    <my-com :count="count"></my-com>
  </div>
</body>
<template id="com">
  <div>
    <h1>子组件</h1>
    {{ count }} - {{ doubleCount }}
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, computed, toRefs, toRef } = Vue

  const Com = {
    template: '#com',
    props: {
      count: Number
    },
    setup (props) {
      console.log(props)
      // 1.计算属性 - 响应式
      // const doubleCount = computed(() => props.count * 2)

      // 2.不要把props中的数据解构，解构会丢失响应式
      // const { count } = props // 模板中 doubleCount 始终为 0
      // const doubleCount = computed(() => count * 2)

      // 3.如果既需要解构，还要保持响应式 toRefs
      // const { count } = toRefs(props) // 将 `props` 转为一个其中全是 ref 的对象，然后解构
      // // console.log(count)
      // const doubleCount = computed(() => count.value * 2) // ref对象访问值通过 value 属性

      // 4.保持响应式 toRef
      const count = toRef(props, 'count') // 将 `props` 的单个属性转为一个 ref
      // console.log(count)
      const doubleCount = computed(() => count.value * 2)

      return {
        doubleCount
      }
    }
  }

  const app = createApp({
   components: {
    MyCom: Com
   },
   setup () {
    const count = ref(0)

    return {
      count
    }
   }
  })

  app.mount('#app')
</script>
</html>
```

### 2.3 Setup的上下文

传入 `setup` 函数的第二个参数是一个 **Setup 上下文**对象。上下文对象暴露了其他一些在 `setup` 中可能会用到的值：

```
{
  setup(props, context) {
    // 透传 Attributes（非响应式的对象，等价于 $attrs）
    console.log(context.attrs)

    // 插槽（非响应式的对象，等价于 $slots）
    console.log(context.slots)

    // 触发事件（函数，等价于 $emit）
    console.log(context.emit)

    // 暴露公共属性（函数）
    console.log(context.expose)
  }
}
```

该上下文对象是非响应式的，可以安全地解构：

```
{
  setup(props, { attrs, slots, emit, expose }) {
    ...
  }
}
```

`attrs` 和 `slots` 都是有状态的对象，它们总是会随着组件自身的更新而更新。这意味着你应当避免解构它们，并始终通过 `attrs.x` 或 `slots.x` 的形式使用其中的属性。此外还需注意，和 `props` 不同，`attrs` 和 `slots` 的属性都**不是**响应式的。如果你想要基于 `attrs` 或 `slots` 的改变来执行副作用，那么你应该在 `onBeforeUpdate` 生命周期钩子中编写相关逻辑。

`expose` 函数用于显式地限制该组件暴露出的属性，当父组件通过[模板引用](https://cn.vuejs.org/guide/essentials/template-refs.html#ref-on-component)访问该组件的实例时，将仅能访问 `expose` 函数暴露出的内容

`78_composition_setup_context.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>78_setup上下文对象</title>
</head>
<body>
  <div id="app">
    <!-- 1.验证 上下文对象中的 attrs 属性 -->
    <!-- <my-com class="active" style="color: red" id="box" @click="test"></my-com> -->
    <!-- 2.验证上下文对象中的 slots 属性 -->
    <my-com ref="child" class="active" style="color: red" id="box" @click="test" @my-event="getData">
      <template #default>
        111
      </template>
      <template #footer>
        222
      </template>
    </my-com>
  </div>
</body>
<template id="com">
  <div>
    <h1>子组件</h1>
    <button @click="sendData">传值给父组件</button>
    <slot></slot>
    <slot name="footer"></slot>
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, computed, toRefs, toRef, onMounted } = Vue

  const Com = {
    template: '#com',
    setup (props, context) {
      // 1.透传 Attributes（非响应式的对象，等价于 $attrs）
      // 调用组件时添加 属性
      console.log(context.attrs) // { class: 'active', id: 'box', style: {}, onClick: fn}

      // 2.插槽（非响应式的对象，等价于 $slots）
      console.log(context.slots) // { default: fn, footer: fn }

      // 3. 触发事件（函数，等价于 $emit）
      const sendData = () => {
        // 选项式组件 this.$emit('my-event', 1000)
        // 组合式组件 不可以再setup中访问this
        context.emit('my-event', 1000)
      }

      // 4.暴露公共属性（函数） 父组件通过 ref 获取到子组件的实例的属性和方法
      const msg = ref('hello composition api')
      const name = ref('吴大勋')
      const changeName = () => {
        name.value = '田叔'
      }
      context.expose({ // 父组件可以通过 ref 获取到暴露出去的数据和方法
        name, changeName
      })
      return {
        sendData
      }
    }
  }

  const app = createApp({
   components: {
    MyCom: Com
   },
   setup () {
    const test = () => {}

    const getData = (val) => {
      console.log(val)
    }

    // 组合式api 给组件定义ref
    const child = ref()
    onMounted(() => {
      // console.log('msg', this.$refs.child.msg) // ×
      // console.log('name', this.$refs.child.name) // ×
      console.log('child', child)

      console.log('msg', child.value.msg) // undefined 因为子组件没有 expose 这个值
      console.log('name', child.value.name) // 吴大勋
    })

    return {
      child,
      test,
      getData
    }
   },
  //  mounted () { // 选项式API获取子组件实例
  //   console.log('msg', this.$refs.child.msg) // undefined 因为子组件没有 expose 这个值
  //   console.log('name', this.$refs.child.name) // 吴大勋
  //  }
  })

  app.mount('#app')
</script>
</html>
```

> 在父组件通过ref获取子组件的实例的属性和方法的需求中，需要注意：
>
> 1.如果子组件是 选项式API组件，基本不需要做任何操作
>
> 2.如果子组件是 组合式API组件，需要通过 context.expose 暴露给父组件需要使用的属性和方法
>
> 3.如果父组件使用 选项式API, 可以通过 this.$refs.refName 访问到子组件想要你看到的属性和方法
>
> 4.如果父组件使用 组合式API,需要在setup中先创建 refName，然后再访问子组件想要你看到的属性和方法（const refName = ref()    refName.value.X）

### 2.4 与渲染函数一起使用

`setup` 也可以返回一个[渲染函数](https://cn.vuejs.org/guide/extras/render-function.html)，此时在渲染函数中可以直接使用在同一作用域下声明的响应式状态：

```
{
  setup() {
    const count = ref(0)
    return () => h('div', count.value)
  }
}
```

返回一个渲染函数将会阻止我们返回其他东西。对于组件内部来说，这样没有问题，但如果我们想通过模板引用将这个组件的方法暴露给父组件，那就有问题

我们可以通过调用 [`expose()`](https://cn.vuejs.org/api/composition-api-setup.html#exposing-public-properties) 解决这个问题：

```
{
  setup(props, { expose }) {
    const count = ref(0)
    const increment = () => ++count.value

    expose({
      increment
    })

    return () => h('div', count.value)
  }
}
```

`79_composition_setup_render_function.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>79_setup渲染函数</title>
</head>
<body>
  <div id="app">
    <button @click="add">加1</button>
    <my-com ref = "com"></my-com>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, h } = Vue
  const Com = {
    setup (props, { expose }) {
      const count = ref(0)

      const increment = () => {
        count.value += 1
      } 

      expose({ // 关键
        increment
      })

      return () => h('div', { class: 'box' }, count.value)
    }
  }
  const app = createApp({
    components: {
      MyCom: Com
    },
    setup () { // 组合式API
      const com = ref()

      const add = () => {
        com.value.increment()
      }
      return {
        com,
        add
      }

    }
  })

  app.mount('#app')
</script>
</html>
```



##  3、响应式核心

### 3.1ref()

接受一个内部值，返回一个响应式的、可更改的 ref 对象，此对象只有一个指向其内部值的属性 `.value`。

ref 对象是可更改的，也就是说你可以为 `.value` 赋予新的值。它也是响应式的，即所有对 `.value` 的操作都将被追踪，并且写操作会触发与之相关的副作用。

如果将一个对象赋值给 ref，那么这个对象将通过 [reactive()](https://cn.vuejs.org/api/reactivity-core.html#reactive) 转为具有深层次响应式的对象。

将一个 ref 赋值给为一个 reactive 属性时，该 ref 会被自动解包

```
const count = ref(0)
console.log(count.value) // 0

count.value++
console.log(count.value) // 1
```

`80_composition_ref.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>80_composition_ref</title>
</head>
<body>
  <div id="app">
    count: <button @click="add">加1</button>{{ count }} <br />
    state.count: <button @click="increment">加1</button> {{ state.count }} <br />
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, reactive } = Vue

  const app = createApp({
    setup () {
      // 1.ref 对象是可更改的，也就是说你可以为 `.value` 赋予新的值
      const count = ref(0)
      const add = () => {
        count.value += 1
      }

      // 2。如果将一个对象赋值给 ref，那么这个对象将通过 reactive() 转为具有深层次响应式的对象。
      const state = ref({ count: 10 })
      const increment = () => {
        state.value.count += 1
      }

      // 3.将一个 ref 赋值给为一个 reactive 属性时，该 ref 会被自动解包
      const obj = reactive({}) // reactive 属性
      obj.count = count // 一个对象赋值给 ref
      // 该 ref 会被自动解包
      console.log(obj.count) // 0
      console.log(obj.count === count.value) // true    

      // 一定要返回，暴露给页面模板使用
      return {
        count,
        add,
        state,
        increment
      }

    }
  })

  app.mount('#app')
</script>
</html>
```

> 以后创建 非 对象类型的数据 使用  ref， 创建对象类型的数据建议使用 reactive

### 3.2computed ()

接受一个 getter 函数，返回一个只读的响应式 [ref](https://cn.vuejs.org/api/reactivity-core.html#ref) 对象。该 ref 通过 `.value` 暴露 getter 函数的返回值。它也可以接受一个带有 `get` 和 `set` 函数的对象来创建一个可写的 ref 对象。

创建一个只读的计算属性 ref：

```
const count = ref(1)
const plusOne = computed(() => count.value + 1)

console.log(plusOne.value) // 2

plusOne.value++ // 错误
```

创建一个可写的计算属性 ref：

```
const count = ref(1)
const plusOne = computed({
  get: () => count.value + 1,
  set: (val) => {
    count.value = val - 1
  }
})

plusOne.value = 1
console.log(count.value) // 0
```

`81_composition_computed.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>81_composition_computed</title>
</head>
<body>
  <div id="app">
    count: <button @click="add">加1</button>{{ count }} - {{ doubleCount }} - {{ plusOne }}<br />
    <button @click="updateComputed">改变计算属性的值</button>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, computed } = Vue

  const app = createApp({
    setup () {
      // 1.基本计算属性 - 一个只读的计算属性 ref
      const count = ref(0)
      const add = () => {
        count.value += 1
      }
      const doubleCount = computed(() => count.value * 2)


      // 2.创建一个可写的计算属性 ref：
      const plusOne = computed({
        get () { return count.value + 1 },
        set (val) { count.value = val - 1 }
      })

      const updateComputed = () => {
        plusOne.value = 100
        console.log('count', count.value) // 99
      }
      return {
        count,
        add,
        doubleCount,
        plusOne,
        updateComputed
      }

    }
  })

  app.mount('#app')
</script>
</html>
```

### 3.3 reactive()

返回一个对象的响应式代理。

响应式转换是“深层”的：它会影响到所有嵌套的属性。一个响应式对象也将深层地解包任何 [ref](https://cn.vuejs.org/api/reactivity-core.html#ref) 属性，同时保持响应性。

值得注意的是，当访问到某个响应式数组或 `Map` 这样的原生集合类型中的 ref 元素时，不会执行 ref 的解包。

返回的对象以及其中嵌套的对象都会通过 [ES Proxy](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy) 包裹，因此**不等于**源对象，建议只使用响应式代理，避免使用原始对象。

创建一个响应式对象：

```
const obj = reactive({ count: 0 })
obj.count++
```

ref 的解包：

```
const count = ref(1)
const obj = reactive({ count })

// ref 会被解包
console.log(obj.count === count.value) // true

// 会更新 `obj.count`
count.value++
console.log(count.value) // 2
console.log(obj.count) // 2

// 也会更新 `count` ref
obj.count++
console.log(obj.count) // 3
console.log(count.value) // 3
```

注意当访问到某个响应式数组或 `Map` 这样的原生集合类型中的 ref 元素时，**不会**执行 ref 的解包：

```
const books = reactive([ref('Vue 3 Guide')])
// 这里需要 .value
console.log(books[0].value)

const map = reactive(new Map([['count', ref(0)]]))
// 这里需要 .value
console.log(map.get('count').value)
```

将一个 [ref](https://cn.vuejs.org/api/reactivity-core.html#ref) 赋值给为一个 `reactive` 属性时，该 ref 会被自动解包：（讲解ref时已经说明）

```
const count = ref(1)
const obj = reactive({})

obj.count = count

console.log(obj.count) // 1
console.log(obj.count === count.value) // true
```

`82_composition__reactive.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>82_composition_reactive</title>
</head>
<body>
  <div id="app">
    state.count: <button @click="add">加1</button>{{ state.count }}<br />
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, reactive } = Vue

  const app = createApp({
    setup () {
      const state = reactive({ count: 10})
      const add = () => {
        state.count++
      }

      // ref的解包
      const count = ref(1)
      const obj = reactive({ count })
      console.log(count.value === obj.count) // true

      count.value++
      console.log(count.value) // 2
      console.log(obj.count) // 2

      obj.count++
      console.log(count.value) // 3
      console.log(obj.count) // 3

      // 注意当访问到某个响应式数组或 `Map` 这样的原生集合类型中的 ref 元素时，**不会**执行 ref 的解包
      const books = reactive([ref('vue3 指南')])
      console.log(books[0].value) // vue3 指南
      const map = reactive(new Map([['count', ref(0)]]))
      console.log(map.get('count').value) // 0

      // 将一个 ref 赋值给为一个 `reactive` 属性时，该 ref 会被自动解包
      const num = ref(1)
      const newObj = reactive({})
      newObj.num = num
      console.log(newObj.num) // 1
      console.log(newObj.num === num.value) // true
      
      return {
        state,
        add
      }

    }
  })

  app.mount('#app')
</script>
</html>
```

> 初始值 对象 reactive  其余用ref

### 3.4 readonly()

接受一个对象 (不论是响应式还是普通的) 或是一个 [ref](https://cn.vuejs.org/api/reactivity-core.html#ref)，返回一个原值的只读代理。

只读代理是深层的：对任何嵌套属性的访问都将是只读的。它的 ref 解包行为与 `reactive()` 相同，但解包得到的值是只读的。

```
const original = reactive({ count: 0 })

const copy = readonly(original)

watchEffect(() => {
  // 用来做响应性追踪
  console.log(copy.count)
})

// 更改源属性会触发其依赖的侦听器
original.count++

// 更改该只读副本将会失败，并会得到一个警告
copy.count++ // warning
```



`83_composition_readonly.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>83_composition_readonly</title>
</head>
<body>
  <div id="app">
    {{ count }} <button @click="addCount">修改count</button> <br />
    {{ copy }} <button @click="addCopy">修改copy</button>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, reactive, readonly } = Vue

  const app = createApp({
    setup () {
      const count = ref(10)
      const addCount = () => {
        count.value += 1
      }

      const copy = readonly(count)
      const addCopy = () => { // Set operation on key "value" failed: target is readonly. 
        copy.value += 1
      }

      return {
        count,
        addCount,
        copy,
        addCopy
      }

    }
  })

  app.mount('#app')
</script>
</html>
```



### 3.5 watchEffect()

立即运行一个函数，同时响应式地追踪其依赖，并在依赖更改时重新执行。

第一个参数就是要运行的副作用函数。这个副作用函数的参数也是一个函数，用来注册清理回调。清理回调会在该副作用下一次执行前被调用，可以用来清理无效的副作用，例如等待中的异步请求。

返回值是一个用来停止该副作用的函数。

```
const count = ref(0)

watchEffect(() => console.log(count.value))
// -> 输出 0

count.value++
// -> 输出 1
```

副作用清除：

```js
watchEffect((onInvalidate) => {
    console.log(id.value) 
    const timer = setTimeout(() => {
        console.log('请求成功') // 2秒之内点击列表 只显示一次
        data.value = '数据' + id.value
    }, 2000)
    onInvalidate(() => {
        clearTimeout(timer)
    })
})
```

停止侦听器：

```js
 const stop = watchEffect(() => {
     console.log(count.value)
 })
 // 当不再需要此侦听器时:
 const stopWatch = () => {
     stop()
 }
```

`84_composition_watchEffect.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>84_composition_watchEffect</title>
</head>
<body>
  <div id="app">
    <button @click="increment">点击了{{ count }}次</button>
    <button @click="stopWatch">停止监听</button>

    <ul>
      <li @click="id=1">请求第一条数据</li>
      <li @click="id=2">请求第二条数据</li>
      <li @click="id=3">请求第三条数据</li>
    </ul>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, watchEffect } = Vue

  const app = createApp({
    setup () {
      const count = ref(0)

      const increment = () => {
        count.value += 1
      }

      const stop = watchEffect(() => {
        console.log(`监听到count的数据为${count.value}`)
      })

      const stopWatch = () => {
        stop()
      }
      const id = ref(1)
      watchEffect((onInvalidate) => {
        console.log(id.value) // 关键  ---  引起 当前 watchEffect 二次执行
        const timer = setTimeout(() => {
          console.log(`请求第${id.value}条数据`)
        }, 3000)
        onInvalidate(() => {
          console.log('清除')
          clearTimeout(timer)
        })

      })
      // onInvalidate
      return {
        count,
        increment,
        stopWatch,
        id
      }
    }
  })

  app.mount('#app')
</script>
</html>
```

> watchEffect没有具体监听哪一个值的变化，只要内部有某一个状态发生改变就会执行

### 3.6 watch()

侦听一个或多个响应式数据源，并在数据源变化时调用所给的回调函数。

- `watch()` 默认是懒侦听的，即仅在侦听源发生变化时才执行回调函数。

  第一个参数是侦听器的**源**。这个来源可以是以下几种：

  - 一个函数，返回一个值
  - 一个 ref
  - 一个响应式对象
  - ...或是由以上类型的值组成的数组

  第二个参数是在发生变化时要调用的回调函数。这个回调函数接受三个参数：新值、旧值，以及一个用于注册副作用清理的回调函数。该回调函数会在副作用下一次重新执行前调用，可以用来清除无效的副作用，例如等待中的异步请求。

  当侦听多个来源时，回调函数接受两个数组，分别对应来源数组中的新值和旧值。

  第三个可选的参数是一个对象，支持以下这些选项：

  - **`immediate`**：在侦听器创建时立即触发回调。第一次调用时旧值是 `undefined`。
  - **`deep`**：如果源是对象，强制深度遍历，以便在深层级变更时触发回调。参考[深层侦听器](https://cn.vuejs.org/guide/essentials/watchers.html#deep-watchers)。
  
  与 [`watchEffect()`](https://cn.vuejs.org/api/reactivity-core.html#watcheffect) 相比，`watch()` 使我们可以：

  - 懒执行副作用；
  - 更加明确是应该由哪个状态触发侦听器重新执行；
  - 可以访问所侦听状态的前一个值和当前值。
  
- **示例**

  侦听一个 getter 函数：

  ```
  const state = reactive({ count: 0 })
  watch(
    () => state.count,
    (count, prevCount) => {
      /* ... */
    }
  )
  ```

  侦听一个 ref：

  ```
  const count = ref(0)
  watch(count, (count, prevCount) => {
    /* ... */
  })
  ```

  当侦听多个来源时，回调函数接受两个数组，分别对应来源数组中的新值和旧值：

  ```
  watch([fooRef, barRef], ([foo, bar], [prevFoo, prevBar]) => {
    /* ... */
  })
  ```

  当使用 getter 函数作为源时，回调只在此函数的返回值变化时才会触发。如果你想让回调在深层级变更时也能触发，你需要使用 `{ deep: true }` 强制侦听器进入深层级模式。在深层级模式时，如果回调函数由于深层级的变更而被触发，那么新值和旧值将是同一个对象。

  ```
  const state = reactive({ count: 0 })
  watch(
    () => state,
    (newValue, oldValue) => {
      // newValue === oldValue
    },
    { deep: true }
  )
  ```

  当直接侦听一个响应式对象时，侦听器会自动启用深层模式：

  ```
  const state = reactive({ count: 0 })
  watch(state, () => {
    /* 深层级变更状态所触发的回调 */
  })
  ```

`85_composition_watch.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>85_composition_watch</title>
</head>
<body>
  <div id="app">
    <button @click="increment">点击了{{ count }}次</button>
    <button @click="foo='foo!!!'">改变foo</button>
    <button @click="bar='bar!!!'">改变bar</button>
    <button @click="state.obj.a=100">改变state</button>{{ state.obj.a }}

  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, watch, reactive } = Vue

  const app = createApp({
    setup () {
      const count = ref(0)
      const increment = () => {
        count.value += 1
      }
      // 函数作为监听源 --- 要的是那个数据 -- 需要value
      watch(() => count.value, (val, oldVal) => {
        console.log('fn', val, oldVal)
      })

      // 侦听ref 不要写value
      watch(count, (val, oldVal) => {
        console.log('ref', val, oldVal)
      })

      // 侦听多个来源
      const foo = ref('foo')
      const bar = ref('bar')
      watch([foo, bar], ([foo, bar], [oldFoo, oldBar]) => {
        console.log('f', foo, oldFoo)
        console.log('b', bar, oldBar)
      })

      // 侦听源为函数，默认无法深度侦听
      const state = reactive({ obj: { a: 10 } })
      // 无法深度侦听
      watch(() => state, (val, oldVal) => {
        console.log('s', val, oldVal)
      } )
      watch(() => state, (val, oldVal) => {
        console.log('d', val, oldVal)
      }, {
        deep: true,
        immediate: true
      } )

      // 直接侦听一个响应式对象时,自动开启深度侦听
      watch(state, (val, oldVal) => {
        console.log('auto', val, oldVal)
      }, {
        immediate: true // 此处不需要写 deep: true
      } )
      return {
        count,
        increment,
        foo,
        bar,
        state
      }

    }
  })

  app.mount('#app')
</script>
</html>
```



## 4、工具函数

### 4.1 toRef()

基于响应式对象上的一个属性，创建一个对应的 ref。这样创建的 ref 与其源属性保持同步：改变源属性的值将更新 ref 的值，反之亦然。

`86_composition_toRef.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>86_composition_toRef</title>
</head>
<body>
  <div id="app">
    
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, toRef, reactive } = Vue

  const app = createApp({
    setup () {
      // 基于响应式对象上的一个属性，创建一个对应的 ref。
      // 这样创建的 ref 与其源属性保持同步：改变源属性的值将更新 ref 的值，反之亦然。
      const state = reactive({
        foo: 1,
        bar: 2
      })

      const count = toRef(state, 'foo')

      console.log(count.value) // 1

      count.value++
      console.log(state.foo) // 2
      console.log(count.value) // 2

      state.foo++
      console.log(state.foo) // 3
      console.log(count.value) // 3
      return {
      }

    }
  })

  app.mount('#app')
</script>
</html>
```

### 4.2 toRefs()

将一个响应式对象转换为一个普通对象，这个普通对象的每个属性都是指向源对象相应属性的 ref。每个单独的 ref 都是使用 [`toRef()`](https://cn.vuejs.org/api/reactivity-utilities.html#toref) 创建的。

`87_composition_toRefs.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>87_composition_toRefs</title>
</head>
<body>
  <div id="app">
    
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, toRefs, reactive } = Vue

  const app = createApp({
    setup () {
      // 基于响应式对象上的一个属性，创建一个对应的 ref。
      // 这样创建的 ref 与其源属性保持同步：改变源属性的值将更新 ref 的值，反之亦然。
      const state = reactive({
        foo: 1,
        bar: 2
      })

      const newState = toRefs(state)

      console.log(newState.foo.value) // 1

      newState.foo.value += 1

      console.log(newState.foo.value) // 2
      console.log(state.foo) // 2

      state.foo++
      console.log(newState.foo.value) // 3
      console.log(state.foo) // 3


      return {
      }

    }
  })

  app.mount('#app')
</script>
</html>
```



> `toRefs` 在调用时只会为源对象上可以枚举的属性创建 ref。如果要为可能还不存在的属性创建 ref，请改用 [`toRef`](https://cn.vuejs.org/api/reactivity-utilities.html#toref)。

## 5、生命周期钩子

### 5.1 onMounted()

注册一个回调函数，在组件挂载完成后执行。

组件在以下情况下被视为已挂载：

- 其所有同步子组件都已经被挂载 (不包含异步组件或 `<Suspense>` 树内的组件)。
- 其自身的 DOM 树已经创建完成并插入了父容器中。注意仅当根容器在文档中时，才可以保证组件 DOM 树也在文档中。

这个钩子通常用于执行需要访问组件所渲染的 DOM 树相关的副作用，或是在[服务端渲染应用](https://cn.vuejs.org/guide/scaling-up/ssr.html)中用于确保 DOM 相关代码仅在客户端执行。

### 5.2 onUpdated()

注册一个回调函数，在组件因为响应式状态变更而更新其 DOM 树之后调用。

父组件的更新钩子将在其子组件的更新钩子之后调用。

这个钩子会在组件的任意 DOM 更新后被调用，这些更新可能是由不同的状态变更导致的。如果你需要在某个特定的状态更改后访问更新后的 DOM，请使用 [nextTick()](https://cn.vuejs.org/api/general.html#nexttick) 作为替代。

### 5.3 onUnmounted()

注册一个回调函数，在组件实例被卸载之后调用。

一个组件在以下情况下被视为已卸载：

- 其所有子组件都已经被卸载。
- 所有相关的响应式作用 (渲染作用以及 `setup()` 时创建的计算属性和侦听器) 都已经停止。

可以在这个钩子中手动清理一些副作用，例如计时器、DOM 事件监听器或者与服务器的连接。

### 5.4 onBeforeMount()

注册一个钩子，在组件被挂载之前被调用。

当这个钩子被调用时，组件已经完成了其响应式状态的设置，但还没有创建 DOM 节点。它即将首次执行 DOM 渲染过程。

### 5.5 onBeforeUpdate()

注册一个钩子，在组件即将因为响应式状态变更而更新其 DOM 树之前调用。

这个钩子可以用来在 Vue 更新 DOM 之前访问 DOM 状态。在这个钩子中更改状态也是安全的。

### 5.6 onBeforeUnmount()

注册一个钩子，在组件实例被卸载之前调用。

当这个钩子被调用时，组件实例依然还保有全部的功能。

`88_composition_lifeCycle.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>88_composition_lifeCycle</title>
</head>
<body>
  <div id="app">
    {{ count }}
    <button @click="add">加1</button>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { 
    createApp, 
    ref,
    onBeforeMount,
    onMounted,
    onBeforeUpdate,
    onUpdated,
    onBeforeUnmount,
    onUnmounted
  } = Vue
  
  const app = createApp({
    setup () {
      const count = ref(0)

      const add = () => {
        count.value += 1
        if (count.value === 5) {
          app.unmount()
        }
      }
      
      onBeforeMount(() => {
        console.log('onBeforeMount')
      })
      onMounted(() => {
        // DOM操作、数据请求、实例化、计时器、延时器、订阅数据
        console.log('onMounted')
      })
      onBeforeUpdate(() => {
        console.log('onBeforeUpdate')
      })
      onUpdated(() => {
         // DOM操作、实例化
        console.log('onUpdated')
      })
      onBeforeUnmount(() => {
        // 取消订阅，清除计时器、延时器等
        console.log('onBeforeUnmount')
      })
      onUnmounted(() => {
        console.log('onUnmounted')
      })

      return {
        count,
        add
      }
    }
  })
  
  app.mount('#app')
</script>
</html>
```



## 6、依赖注入

### 6.1 provide()

提供一个值，可以被后代组件注入。

`provide()` 接受两个参数：第一个参数是要注入的 key，可以是一个字符串或者一个 symbol，第二个参数是要注入的值。

> 与注册生命周期钩子的 API 类似，`provide()` 必须在组件的 `setup()` 阶段同步调用。

### 6.2 inject()

注入一个由祖先组件或整个应用 (通过 `app.provide()`) 提供的值。

第一个参数是注入的 key。Vue 会遍历父组件链，通过匹配 key 来确定所提供的值。如果父组件链上多个组件对同一个 key 提供了值，那么离得更近的组件将会“覆盖”链上更远的组件所提供的值。如果没有能通过 key 匹配到值，`inject()` 将返回 `undefined`，除非提供了一个默认值。

第二个参数是可选的，即在没有匹配到 key 时使用的默认值。它也可以是一个工厂函数，用来返回某些创建起来比较复杂的值。如果默认值本身就是一个函数，那么你必须将 `false` 作为第三个参数传入，表明这个函数就是默认值，而不是一个工厂函数。

与注册生命周期钩子的 API 类似，`inject()` 必须在组件的 `setup()` 阶段同步调用。

`89_composition_provide_inject.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>89_composition_provide_inject</title>
</head>
<body>
  <div id="app">
    <button @click="count++">点击{{ count }}次</button>
    <my-parent></my-parent>
  </div>
</body>
<template id="parent">
  <div>
    我是父组件 - {{ count }}
    <my-child></my-child>
  </div>
</template>
<template id="child">
  <div>
    我是子组件 <button @click="add">加10</button> {{ test }} {{ fn() }}
  </div>
</template>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, provide, inject } = Vue
  const Child = {
    template: '#child',
    setup () {
      // 注入值的默认方式
      const add = inject('add')
      // 设置默认值
      const test = inject('test', '测试')
      // 如果默认值为函数，设置第三个参数为false
      const fn = inject('fn', () => {
        console.log(100000)
      }, false)

      return {
        add,
        test,
        fn
      }
    }
  }
  const Parent = {
    template: '#parent',
    components: {
      MyChild: Child
    },
    setup () {
      const count = inject('count')
      return {
        count
      }
    }
  }
  const app = createApp({
    components: {
      MyParent: Parent
    },
    setup () {
      const count = ref(10)

      const add = () => {
        count.value += 10
      }
      // 提供值，可以是数据，也可以是函数
      provide('count', count)
      provide('add', add)
      return {
        count
      }

    }
  })

  app.mount('#app')
</script>
</html>
```



## 7、组合式函数

### 7.1 什么是“组合式函数”？

在 Vue 应用的概念中，“组合式函数”(Composables) 是一个利用 Vue 的组合式 API 来封装和复用**有状态逻辑**的函数。

> 也可以叫做自定义hooks

当构建前端应用时，我们常常需要复用公共任务的逻辑。例如为了在不同地方格式化时间，我们可能会抽取一个可复用的日期格式化函数。这个函数封装了**无状态的逻辑**：它在接收一些输入后立刻返回所期望的输出。

相比之下，有状态逻辑负责管理会随时间而变化的状态。一个简单的例子是跟踪当前鼠标在页面中的位置。在实际应用中，也可能是像触摸手势或与数据库的连接状态这样的更复杂的逻辑。

如果我们要直接在组件中使用组合式 API 实现鼠标跟踪功能，它会是这样的

`90_composition_mouse.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>90_composition_mouse</title>
</head>
<body>
  <div id="app">
    鼠标的位置为：({{ x }}, {{ y }})
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, onMounted, onUnmounted } = Vue

  const app = createApp({
    setup () {
      const x = ref(0)
      const y = ref(0)

      function updatePosition (event) {
        x.value = event.pageX
        y.value = event.pageY
      }

      onMounted(() => {
        window.addEventListener('mousemove', updatePosition, false)
      }) 
      onUnmounted(() => {
        window.removeEventListener('mousemove', updatePosition, false)
      })
      return {
        x, y
      }

    }
  })

  app.mount('#app')
</script>
</html>
```

> 但是，如果我们想在多个组件中复用这个相同的逻辑呢？我们可以把这个逻辑以一个组合式函数的形式提取到外部文件中

### 7.2 如何定义和使用组合式函数

`91_composition_mouse_hooks.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>91_composition_mouse_hooks</title>
</head>
<body>
  <div id="app">
    鼠标的位置为：({{ x }}, {{ y }})
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, onMounted, onUnmounted } = Vue
  function useMouse () {
    const x = ref(0)
    const y = ref(0)

    function updatePosition (event) {
      x.value = event.pageX
      y.value = event.pageY
    }

    onMounted(() => {
      window.addEventListener('mousemove', updatePosition, false)
    }) 
    onUnmounted(() => {
      window.removeEventListener('mousemove', updatePosition, false)
    })
    return { x, y }
  }
  const app = createApp({
    setup () {
      const { x, y } = useMouse()
      return {
        x, y
      }

    }
  })

  app.mount('#app')
</script>
</html>
```

如你所见，核心逻辑完全一致，我们做的只是把它移到一个外部函数中去，并返回需要暴露的状态。和在组件中一样，你也可以在组合式函数中使用所有的[组合式 API](https://cn.vuejs.org/api/#composition-api)。现在，`useMouse()` 的功能可以在任何组件中轻易复用了。

更酷的是，你还可以嵌套多个组合式函数：一个组合式函数可以调用一个或多个其他的组合式函数。

`92_composition_mouse_more_hooks.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>92_composition_mouse_more_hooks</title>
</head>
<body>
  <div id="app">
    鼠标的位置为：({{ x }}, {{ y }})
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, onMounted, onUnmounted } = Vue
  function useEventListener (target, event, callback) {
    onMounted(() => {
      target.addEventListener(event, callback, false)
    }) 
    onUnmounted(() => {
      target.removeEventListener(event, callback, false)
    })
  }

  function useMouse () {
    const x = ref(0)
    const y = ref(0)

    // function updatePosition (event) {
    //   x.value = event.pageX
    //   y.value = event.pageY
    // }
    useEventListener(window, 'mousemove', (event) => {
      x.value = event.pageX
      y.value = event.pageY
    })

    
    return { x, y }
  }
  const app = createApp({
    setup () {
      const { x, y } = useMouse()
      return {
        x, y
      }

    }
  })

  app.mount('#app')
</script>
</html>
```

### 7.3 异步状态

在做异步数据请求时，我们常常需要处理不同的状态：加载中、加载成功和加载失败。

`93_composition_async.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>93_composition_async</title>
</head>
<body>
  <div id="app">
    <div v-if="error">{{ error }}</div>
    <div v-else-if="data">
      <ul>
        <li v-for="item of data" :key="item.proid">{{ item.proname }}</li>
      </ul>
    </div>
    <div v-else>加载中....</div>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, onMounted } = Vue

  const app = createApp({
    setup () { 
      const data = ref(null)
      const error = ref(null)

      // 接口文档 http://121.89.205.189:3001/apidoc/#api-Pro-GetProList
      onMounted(() => {
        fetch('http://121.89.205.189:3001/api/pro/list')
          .then(res => res.json())
          .then(res => {
            console.log(res)
            data.value = res.data
          }).catch(err => {
            console.log(err)
            error.value = err
          })
      })

      return {
        data,
        error
      }

    }
  })

  app.mount('#app')
</script>
</html>
```

如果在每个需要获取数据的组件中都要重复这种模式，那就太繁琐了。让我们把它抽取成一个组合式函数：

`94_composition_async_hooks.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>94_composition_async_hooks</title>
</head>
<body>
  <div id="app">
    <div v-if="error">{{ error }}</div>
    <div v-else-if="data">
      <ul>
        <li v-for="item of data" :key="item.proid">{{ item.proname }}</li>
      </ul>
    </div>
    <div v-else>加载中....</div>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, onMounted } = Vue
  function useFetch (url) {
    const data = ref(null)
      const error = ref(null)

      // 接口文档 http://121.89.205.189:3001/apidoc/#api-Pro-GetProList
      fetch(url)
        .then(res => res.json())
        .then(res => {
          console.log(res)
          data.value = res.data
        }).catch(err => {
          console.log(err)
          error.value = err
        })
    return { data, error }
  }
  const app = createApp({
    setup () { 
      const { data, error } = useFetch('http://121.89.205.189:3001/api/pro/list')
      return {
        data,
        error
      }

    }
  })

  app.mount('#app')
</script>
</html>
```

`useFetch()` 接收一个静态的 URL 字符串作为输入，所以它只执行一次请求，然后就完成了。但如果我们想让它在每次 URL 变化时都重新请求呢？那我们可以让它同时允许接收 ref 作为参数：

`95_composition_async_hooks_urls.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>94_composition_async_hooks</title>
</head>
<body>
  <div id="app">
    <button @click="type='list'">获取产品列表数据</button>
    <button @click="type='recommendlist'">获取推荐列表数据</button>
    <div v-if="error">{{ error }}</div>
    <div v-else-if="data">
      <ul>
        <li v-for="item of data" :key="item.proid">{{ item.proname }}</li>
      </ul>
    </div>
    <div v-else>加载中....</div>
  </div>
</body>
<script src="lib/vue.global.js"></script>
<script>
  const { createApp, ref, computed, unref, isRef, watchEffect } = Vue
  function useFetch (url) {
    console.log(url.value)
    const data = ref(null)
    const error = ref(null)

    function doFetch () {
      // 初始化数据
      data.value = null
      error.value = null
      // unref() 解包可能为 ref 的值  unref(url) <===> url.value
      fetch(unref(url))
      // fetch(url.value)
        .then(res => res.json())
        .then(res => {
          console.log(res)
          data.value = res.data
        }).catch(err => {
          console.log(err)
          error.value = err
        })
    }

    // doFetch()
    // 接口文档 http://121.89.205.189:3001/apidoc/#api-Pro-GetProList
    if (isRef(url)) {
      // 若输入的 URL 是一个 ref，那么启动一个响应式的请求
      watchEffect(doFetch)
    } else {
      // 否则只请求一次
      // 避免监听器的额外开销
      doFetch()
    }
    
    return { data, error }
  }
  const app = createApp({
    setup () { 
      const type = ref('list')

      const url = computed(() => 'http://121.89.205.189:3001/api/pro/'+ type.value)

      const { data, error } = useFetch(url)
      return {
        data,
        error,
        type
      }

    }
  })

  app.mount('#app')
</script>
</html>
```



### 7.4 约定和最佳实践

#### 7.4.1 命名

组合式函数约定用驼峰命名法命名，并以“use”作为开头。

#### 7.44.2 输入参数

尽管其响应性不依赖 ref，组合式函数仍可接收 ref 参数。如果编写的组合式函数会被其他开发者使用，你最好在处理输入参数时兼容 ref 而不只是原始的值。[`unref()`](https://cn.vuejs.org/api/reactivity-utilities.html#unref) 工具函数会对此非常有帮助：

```
import { unref } from 'vue'

function useFeature(maybeRef) {
  // 若 maybeRef 确实是一个 ref，它的 .value 会被返回
  // 否则，maybeRef 会被原样返回
  const value = unref(maybeRef)
}
```

如果你的组合式函数在接收 ref 为参数时会产生响应式 effect，请确保使用 `watch()` 显式地监听此 ref，或者在 `watchEffect()` 中调用 `unref()` 来进行正确的追踪。

#### 7.4.3  返回值

你可能已经注意到了，我们一直在组合式函数中使用 `ref()` 而不是 `reactive()`。我们推荐的约定是组合式函数始终返回一个包含多个 ref 的普通的非响应式对象，这样该对象在组件中被解构为 ref 之后仍可以保持响应性：

```
// x 和 y 是两个 ref
const { x, y } = useMouse()
```

从组合式函数返回一个响应式对象会导致在对象解构过程中丢失与组合式函数内状态的响应性连接。与之相反，ref 则可以维持这一响应性连接。

如果你更希望以对象属性的形式来使用组合式函数中返回的状态，你可以将返回的对象用 `reactive()` 包装一次，这样其中的 ref 会被自动解包，例如：

```
const mouse = reactive(useMouse())
// mouse.x 链接到了原来的 x ref
console.log(mouse.x)
```

```
Mouse position is at: {{ mouse.x }}, {{ mouse.y }}
```

#### 7.4.4  副作用

在组合式函数中的确可以执行副作用 (例如：添加 DOM 事件监听器或者请求数据)，但请注意以下规则：

- 如果你的应用用到了[服务端渲染](https://cn.vuejs.org/guide/scaling-up/ssr.html) (SSR)，请确保在组件挂载后才调用的生命周期钩子中执行 DOM 相关的副作用，例如：`onMounted()`。这些钩子仅会在浏览器中被调用，因此可以确保能访问到 DOM。
- 确保在 `onUnmounted()` 时清理副作用。举例来说，如果一个组合式函数设置了一个事件监听器，它就应该在 `onUnmounted()` 中被移除 (就像我们在 `useMouse()` 示例中看到的一样)。当然也可以像之前的 `useEventListener()` 示例那样，使用一个组合式函数来自动帮你做这些事。

#### 7.4.5  使用限制

组合式函数在 `<script setup>` 或 `setup()` 钩子中，应始终被**同步地**调用。在某些场景下，你也可以在像 `onMounted()` 这样的生命周期钩子中使用他们。

这个限制是为了让 Vue 能够确定当前正在被执行的到底是哪个组件实例，只有能确认当前组件实例，才能够：

1. 将生命周期钩子注册到该组件实例上
2. 将计算属性和监听器注册到该组件实例上，以便在该组件被卸载时停止监听，避免内存泄漏。
