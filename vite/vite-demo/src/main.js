import { createApp } from 'vue'
import App from './App.vue'
import mitt from 'mitt'
/**
 * 1. npm i mitt
 * 2. import mitt from 'mitt'
 */

const app = createApp(App)

const event = mitt()
app.config.globalProperties.$eventBus = event
  
app.mount('#app')
