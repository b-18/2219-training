import { Parcel } from "@parcel/core";
import fs from "fs";
import path from "path";
import chalk from "chalk";

const port = 1235;
const pagePath = path.join(process.cwd(), "page");
const pages = fs
  .readdirSync(pagePath, { withFileTypes: true })
  .filter((dirent) => dirent.isDirectory())
  .sort((direntA, direntB) => {
    const birthtimeMsA = fs.statSync(`${pagePath}/${direntA.name}`).birthtimeMs;
    const birthtimeMsB = fs.statSync(`${pagePath}/${direntB.name}`).birthtimeMs;
    return birthtimeMsA - birthtimeMsB;
  })
  .map((dirent) => dirent.name);

const bundler = new Parcel({
  entries: pages.map(
    (page) => path.join(process.cwd(), "page", page) + "/*.html"
  ),
  defaultConfig: "@parcel/config-default",
  serveOptions: {
    port,
  },
  hmrOptions: {
    port,
  },
});

await bundler.watch((err, event) => {
  if (err) {
    throw err;
  }

  if (event.type === "buildSuccess") {
    let bundles = event.bundleGraph.getBundles();
    console.log(`✨ Built ${bundles.length} bundles in ${event.buildTime}ms!`);
  } else if (event.type === "buildFailure") {
    console.log(event.diagnostics);
  }
});

const link = `http://localhost:${port}`;
console.log();
console.log(chalk.green(`Running on ${link}`));
console.log();
for (let page of pages) {
  console.log(chalk.green(`${link}/${page}`));
}
console.log();