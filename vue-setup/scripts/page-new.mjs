import path from "path";
import fs from "fs";

let folderName;
try {
  folderName = process.argv[2];
} catch (e) {
  process.exit(1);
}

const pathFolder = path.join(process.cwd(), "page");
const pathTplFolder = path.join(process.cwd(), "scripts/page-tpl");

const targetFolder = `${pathFolder}/${folderName}`;

console.log(targetFolder)
if (fs.existsSync(targetFolder)) {
  console.log("已存在");
  process.exit(1);
}

const noop = () => {};

fs.mkdirSync(targetFolder);
fs.readFile(`${pathTplFolder}/index.html.txt`, (err, content) => {
  fs.writeFile(`${targetFolder}/index.html`, content, noop);
});
fs.readFile(`${pathTplFolder}/main.js.txt`, (err, content) => {
  fs.writeFile(`${targetFolder}/main.js`, content, noop);
});
fs.readFile(`${pathTplFolder}/app.vue.txt`, (err, content) => {
  fs.writeFile(`${targetFolder}/app.vue`, content, noop);
});
console.log("创建成功");