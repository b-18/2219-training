import { ref, onMounted, onUnmounted } from 'vue'

/**
  组合式函数(hook)
  普通函数 + vue响应式数据和生命周期
*/
export default function () {
  const w = ref(window.innerWidth) // 窗口宽度
  const h = ref(window.innerHeight) // 窗口高度

  const onResize = () => {
    w.value = window.innerWidth
    h.value = window.innerHeight
  }

  onMounted(() => {
    window.addEventListener('resize', onResize)
  })

  onUnmounted(() => {
    window.removeEventListener('resize', onResize)
  })

  return {
    w,
    h,
  }
}