/**
 * mixin(混入)
 */

// 和组件的写法一样
export default {
  data() {
    return {
      w: window.innerWidth,
      h: window.innerHeight
    }
  },
  mounted() {
    window.addEventListener('resize', this.onResize)
  },
  unMounted() {
    window.removeEventListener('resize', this.onResize)
  },
  methods: {
    onResize() {
      this.w = window.innerWidth
      this.h = window.innerHeight
    }
  }
}