import { createApp } from "vue";
import App from "./app.vue";

const app = createApp(App);
// 全局 provide
// app.provide(变量名, 值)
app.provide('name', 'vue3 setup')
app.provide('hello', () => {
  alert('hello vue3')
})

app.mount("#root");