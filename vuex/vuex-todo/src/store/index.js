import { nanoid } from 'nanoid'
import { createStore } from 'vuex'

import { fetchTodoList } from '../api/todo'

// vuex 对象
const store = createStore({
  // 类似 vue 的 data
  // 所有的数据都在这里
  // 唯一数据源
  state() {
    return {
      // 元素 : {
      // content: 内容
      // status: 0: 未完成 1: 完成
      // }
      todoList: [],
      // 0 全部
      // 1 未完成
      // 2 已完成
      filterType: 0
    }
  },

  // 类似 vue 的 计算属性
  getters: {
    // 未完成
    unfinishedTodoList(state) {
      // 挑选出 status 等于 0
      return state.todoList.filter((todo) => todo.status === 0)
    },
    // 已完成
    finishedTodoList(state) {
      // 挑选出 status 等于 1
      return state.todoList.filter((todo) => todo.status === 1)
    },
    // todoList 过滤结果
    filteredTodoList(state, getters) {
      if (state.filterType === 1) {
        // 未完成
        // 挑选出 status 等于 0
        // return state.todoList.filter((todo) => todo.status === 0)
        return getters.unfinishedTodoList
      } else if (state.filterType === 2) {
        // 已完成
        // 挑选出 status 等于 1
        // return state.todoList.filter((todo) => todo.status === 1)
        return getters.finishedTodoList
      } else {
        // 全部
        return state.todoList
      }
    }
  },

  // mutations 里面的方法, 用来修改 state
  // 只允许 这里面的方法, 修改 state
  mutations: {
    // 修改todoList
    setTodoList(state, list) {
      state.todoList = list
    },

    // 新增todo
    // 方法名(整个state对象, 参数)
    add(state, content) {
      state.todoList.push({
        // 使用 nanoid
        id: nanoid(),
        status: 0,
        content,
      })
    },
    // 完成
    finish(state, id) {
      // state.todoList[index].status = 1
      const index = state.todoList.findIndex(todo => todo.id === id)
      state.todoList[index].status = 1
    },
    // 删除
    remove(state, id) {
      // state.todoList.splice(index, 1)
      const index = state.todoList.findIndex(todo => todo.id === id)
      state.todoList.splice(index, 1)
    },
    // 修改过滤条件
    changeFilterType(state, newType) {
      state.filterType = newType
    }

  },

  // 主要用来做异步
  actions: {
    // 请求 todolist 数据
    fetchList(context) {
      // context.state
      // context.getters
      // context.commit
      // context.dispatch

      fetchTodoList().then(result => {
        // ❌ action 不能修改 state
        // context.state.todoList = result

        // 通过 mutation 修改数据
        context.commit('setTodoList', result)
      })
    }
  }

    // async fetchList(context) {
    //     const result = await fetchTodoList()

    //     context.commit('setTodoList', result)
    //   }
    // }
 
})


export default store