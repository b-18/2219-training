import { nanoid } from "nanoid"

// 模拟接口返回
export function fetchTodoList() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve([
        {
          id: nanoid(),
          content: 'aaa',
          status: 0
        },
        {
          id: nanoid(),
          content: 'bbb',
          status: 0
        },
        {
          id: nanoid(),
          content: 'ccc',
          status: 0
        },
        {
          id: nanoid(),
          content: 'ddd',
          status: 0
        }
      ])
    }, 1000)
  })
}