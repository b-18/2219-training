import { createApp } from 'vue'
import App from './App.vue'

import store from './store'


const app = createApp(App)

// 注册 store
app.use(store)
/**
 * 注册完成后, 提供了 this.$store
 * 1. this.$store.state
 * 2. this.$store.getters
 * 3. this.$store.commit(mutation名称, 参数)
 * 4. this.$store.dispatch(action名称, 参数)
 */
  
app.mount('#app')
