import axios from 'axios'

const request = axios.create({
  baseURL: 'http://121.89.205.189:3000/api'

  // 相当于: http://localhost:5173/api
  // baseURL: '/api'
})

request.interceptors.request.use(req => {
  req.headers.token = localStorage.getItem('token')
  return req
})

request.interceptors.response.use(res => {
  return res.data
})

export default request
