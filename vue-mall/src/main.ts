import { createApp } from 'vue'

// 引入 amfe-flexible
import 'amfe-flexible'
// reset-css
import 'reset-css'
// vant 组件样式
import 'vant/lib/index.css';

// css-pro-layout
// import 'css-pro-layout/dist/css/css-pro-layout.css';
import './assets/style/layout.scss'

import { createPinia } from 'pinia'


import App from './App.vue'
import router from './router'


const app = createApp(App)

app.use(router)

// 注册pinia
// const pinia = createPinia()
// app.use(pinia)
app.use(createPinia())

app.mount('#app')
