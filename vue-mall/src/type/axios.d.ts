// 接口返回结构固定
// data 的类型, 传进来
type Result<T = undefined> = {
  code: string,
  message: string,
  data: T
}
