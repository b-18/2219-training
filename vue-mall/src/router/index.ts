import { createRouter, createWebHistory } from 'vue-router'

import HomeView from '../views/home.vue'
import DetailView from '../views/detail.vue'
import CartView from '../views/cart.vue'
import LoginView from '../views/login.vue'
import RegView from '../views/reg.vue'
import SassView from '../views/sass.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/home',
      component: HomeView
    },
    {
      path: '/detail/:id',
      component: DetailView
    },
    {
      path: '/cart',
      component: CartView
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView
    },
    {
      path: '/reg',
      name: 'reg',
      component: RegView
    },
    {
      path: '/sass',
      component: SassView
    }
  ]
})

export default router
