import request from "@/utils/request"

export function login(loginname: string, password: string) {
  return request.post<
    any,
    Result<{ token: string, userid: string}>
    >('/user/login', {
    loginname,
    password
  })
}