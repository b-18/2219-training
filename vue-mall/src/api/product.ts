import request from "@/utils/request"


// 获取商品列表
export function getProductList(count: number, limitNum = 10) {
  return request.get<
      any, 
      Result<Product[]>
    >('/pro/list', {
    params: {
      count,
      limitNum
    }
  })
}

// 获取商品详情
export function getProductDetail(proid: string) {
  return request.get<
    any,
    Result<Product>
  >(`/pro/detail/${proid}`)
}