import axios, { type CancelToken, type CancelTokenSource } from "axios"
import request from "@/utils/request"

const getUserId = () => localStorage.getItem('userid')

// 获取购物车列表
export function getCartList() {
  return request.post<
    any,
    Result<CartItem[]>
  >('/cart/list', {
    userid: getUserId()
  })
}

// 加入购物车
export function addToCart(proid: string) {
  return request.post<
    any,
    Result
  >('/cart/add', {
    userid: getUserId(),
    proid
  })
}

// let controller: AbortController
// // 更改购物车产品数量
// export function updateCartNum(cartid: string, num: number) {

//   if (controller) {
//     controller.abort()
//   }

//   controller = new AbortController()
  
//   return request.post<
//     any,
//     Result
//     >(
//       '/cart/updatenum',
//       {
//         cartid,
//         num
//       },
//       {
//         signal: controller.signal
//       }
//   )
// }


// let cancelTokenSource: CancelTokenSource
// // 更改购物车产品数量
// export function updateCartNum(cartid: string, num: number) {

//   if (cancelTokenSource) {
//     cancelTokenSource.cancel()
//   }

//   cancelTokenSource = axios.CancelToken.source()
  
//   return request.post<
//     any,
//     Result
//     >(
//       '/cart/updatenum',
//       {
//         cartid,
//         num
//       },
//       {
//         cancelToken: cancelTokenSource.token
//       }
//   )
// }


// 更改购物车产品数量
// 使用 AbortController 取消请求

let abortController: AbortController
export function updateCartNum(cartid: string, num: number) {
  if (abortController) {
    // 1. 尝试取消请求
    // 如果请求还在进行, 取消
    abortController.abort()
  }

  // 2. 生成 abortController
  abortController = new AbortController()

  return request.post<
    any,
    Result
    >(
      '/cart/updatenum',
      {
        cartid,
        num
      },
      {
        // 3. 传递 signal 属性
        signal: abortController.signal
      }
  )
}

// 选中/取消选中
export function toggleCartItem(cartid: string, flag: boolean) {
  return request.post('/cart/selectone', {
    cartid,
    flag
  })
}

// 购物车 全选/全不选
export function toggleAllCartItems(type: boolean) {
  return request.post('/cart/selectall', {
    userid: getUserId(),
    type
  })
}