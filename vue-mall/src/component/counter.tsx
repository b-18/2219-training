import { defineComponent, ref, mergeProps } from 'vue'

type Props = {
  
}

export default defineComponent<Props>({
  setup(props, { emit, slots }) {
    const count = ref(111)
    const add = () => count.value++
    const minus = () => count.value--

    // const newProps = mergeProps(
    //   {
    //     class: 'counter'
    //   },
    //   props
    // )

    // console.log(props, newProps)

    return () => (
      <div>
        <div>count is, { count.value }</div>
        <button onClick={minus}>-</button>
        <button onClick={add}>+</button>
      </div>
    )
  }
})