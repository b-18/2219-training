import { defineStore } from 'pinia'

import { getCartList, addToCart, updateCartNum, toggleCartItem, toggleAllCartItems } from '../api/cart'

type State = {
  cartList: CartItem[]
}

export const useCartStore = defineStore(
  'cart-store',
  {
    state: (): State => {
      return {
        cartList: []
      }
    },

    // 相当于 vue 的 computed
    getters: {
      // 总价
      totalMoney(state) {
        // return this.cartList.reduce(
        //   (prev, item) => {
        //     return prev + item.originprice * item.discount / 10 * item.num
        //   },
        //   0
        // )
        // 不要使用 this 取值, 会让类型失效
        // 使用 state 能够正确的推导出类型
        // return state.cartList.reduce(
        //   (prev, item) => {
        //     if (item.flag) {
        //       return prev + item.originprice * item.discount / 10 * item.num
        //     }
        //     return prev
        //   },
        //   0
        // )
        return state.cartList.filter(item => item.flag).reduce(
          (prev, item) => {
            return prev + item.originprice * item.discount / 10 * item.num
          },
          0
        )
      },

      isAllSelected(state) {
        // 写法1: 使用 filter
        // 挑选出 flag 值为true 的元素, 新数组和原数组长度对比
        // 长度相等, 说明全部全中
        // return state.cartList.filter(item => item.flag === true).length === state.cartList.length

        // 写法2: 使用 every
        return state.cartList.every(item => item.flag === true)

        // 写法3: 使用 some
        // return !state.cartList.some(item => item.flag === false)

      }
    },
    actions: {
      // 获取购物车数据
      async fetchCartList() {
        const result = await getCartList()
        this.cartList = result.data ?? []
        return result.data
      },

      // 添加到购物车
      async addToCart(proid: string) {
        await addToCart(proid)
        this.fetchCartList()
      },

      // 更新产品数量
      async updateCartNum(cartid: string, num: number) {
        await updateCartNum(cartid, num)
        const cart = this.cartList.find(item => item.cartid === cartid)
        if (cart) {
          cart.num = num
        }
      },

      // 选中/取消选中
      async toggleCartItem(cartid: string, flag: boolean) {
        await toggleCartItem(cartid, flag)
        const cart = this.cartList.find(item => item.cartid === cartid)
        if (cart) {
          cart.flag = flag
        }
      },

      // 购物车 全选/全不选
      async toggleAll(type: boolean) {
        await toggleAllCartItems(type)
        this.cartList.forEach(cart => {
          cart.flag = type
        })
      }

    },
  }
)