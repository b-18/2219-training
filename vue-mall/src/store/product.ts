import { defineStore } from 'pinia'

import { getProductList, getProductDetail } from '../api/product'


type State = {
  productList: Product[]
  // .content元素的 scrollTop
  productListScrollY: number
}

// 命名以 use 开头
// 用法 defineStore(名称, 对象)
export const useProductStore = defineStore(
  'product-store', // store 名称, 要求唯一
  {
    // 为了更好的支持 ts, state 最好写成箭头函数
    // 相当于 vue 的 data
    state: (): State => {
      return {
        productList: [],
        productListScrollY: 0
      }
    },
    // 相当于 vue 的 computed
    getters: {

    },
    // 相当于 vue 的 methods
    actions: {
      // 获取商品列表
      async fetchList(page: number) {
        const result = await getProductList(page)

        // 接口的数据, 赋给 state
        if (page === 1) {
          this.productList = result.data
        } else {
          this.productList.push(...result.data)
        }
        

        return result.data
      },

      // fetchProduct(proid: string) {
      //   // 根据 proid 从产品列表里找出 对应的产品详情数据
      //   const product = this.productList.find(item => item.proid === proid)

      //   return product
      // },

      fetchProduct(proid: string) {
        // 根据 proid 从产品列表里找出 对应的产品详情数据
        const product = this.productList.find(item => item.proid === proid)

        if (product) {
          // 如果 pinia 里能找到, 直接返回数据

          return product

          // !!! 这里讲错了, 可以直接 return product, await 可以推导出类型
          
          // return Promise.resolve(product)
          // 相当于:
          // return new Promise((resolve) => {
          //   resolve(product)
          // })
        } else {
          // pinia 里找不到, 发请求
          return this.requestProduct(proid)
        }
      },

      async requestProduct(proid: string) {
        // 发送请求, 获取产品详情数据
        const result = await getProductDetail(proid)
        return result.data
      }
    }
  }
)