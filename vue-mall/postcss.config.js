module.exports = {
  plugins: {
    'postcss-pxtorem': {
      // 37.5 表示 375 设计稿
      // 37.5 是在 375 宽度下, amfe-flexible 计算出来的 html 元素 的 font-size
      rootValue: 37.5, 
      propList: ['*'],
      // 选择器黑名单, 匹配这个名单的选择器, 不转换
      // sass 学习演示用
      selectorBlackList: ['scss-']
    }
  }
}