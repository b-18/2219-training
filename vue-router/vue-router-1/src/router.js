import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'

import HomePage from './component/HomePage.vue'
import AboutPage from './component/AboutPage.vue'

// 使用 createRouter 方法,创建 router
const router = createRouter({
  // 指定路由模式
  // 1. history 路由
  history: createWebHistory(),
  // 2. hash 路由
  // history: createWebHashHistory(),

  // 定义路由表
  routes: [
    {
      path: '/home',
      component: HomePage
    },
    {
      path: '/about',
      component: AboutPage
    }
  ]
})


export default router