import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'

import HomePage from './component/HomePage.vue'
import AboutPage from './component/AboutPage.vue'
import ListPage from './component/ListPage.vue'
import DetailPage from './component/DetailPage.vue'
import ListPage1 from './component/ListPage1.vue'
import DetailPage1 from './component/DetailPage1.vue'
import A0 from './component/A0.vue'
import A1 from './component/A1.vue'
import A2 from './component/A2.vue'
import NotFound from './component/404.vue'

const router = createRouter({
  // 指定路由模式
  // 1. history 模式
  history: createWebHistory(),
  // 2. hash 模式
  // history: createWebHashHistory(),

  // 路由表
  routes: [
    // {
    //   path: '/',
    //   // 重定向
    //   // 自动跳转到 /home, 网址也变成 /home
    //   // 1. 写法1
    //   // redirect: '/home'
    //   // 2. 写法2
    //   // redirect: { name: 'home' }
    // },
    {
      path: '/home',
      name: 'home',
      // 别名
      // / 和 /home 显示的组件一样, 网站不变化
      alias: '/',
      component: HomePage
    },
    {
      path: '/about',
      component: AboutPage,

      // 路由级别的导航守卫
      // 准备跳转到 about 之前触发
      // beforeEnter(to, from) {
      //   // 从 list 页过来的, 不允许看 about 页
      //   if (from.path === '/list') {
      //     return false
      //   }
      // },

      // 子路由
      // 内层的 path, 前面不加 /
      children: [
        {
          // /about
          path: '',
          component: A0
        },
        {
          // /about/a1
          path: 'a1',
          component: A1,
        },
        {
          // /about/a2
          path: 'a2',
          component: A2
        }
      ]
    },
    {
      path: '/list',
      component: ListPage
    },
    {
      // 传参
      // path: '/detail/:id',
      // 可选参数
      path: '/detail/:id?',
      // 给路由命名
      name: 'detail',
      component: DetailPage,

      // 将路由的参数,转换成组件的 props
      props: true,
    },
    {
      path: '/404',
      name: '404',
      component: NotFound
    },

    {
      path: '/list1',
      component: ListPage1,
      children: [
        {
          path: ':id',
          component: DetailPage1
        }
      ]
    }
  ]
})

// 全局级别的导航守卫
// 所有的跳转的都会触发
router.beforeEach((to, from) => {
  console.log(to) 

  if (to.matched.length === 0) {
    // 没有匹配的路径
    // 404
    // 方法1
    // return的字符串, 会认为是跳转路径
    // return '/404'
    // 方法2
      // return {name: '404'}
    // 方法3
    // 使用 router对象 进行跳转
    router.push('/404')
  }


  // to 要去的
  // from 从哪里来
  // https://router.vuejs.org/zh/api/#routelocationnormalized

  // 不放跳转到 about 页
  // if (to.path === '/about') {
  //   return false
  // }

  // 阻止跳转
  // return false
})


export default router