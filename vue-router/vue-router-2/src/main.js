import { createApp } from 'vue'
import App from './App.vue'

// 引入 router.js
import router from './router'

const app = createApp(App)

// 注册 router
app.use(router)
/**
 * 注册 router 之后:
 * 1) 注册了两个全局组件 RouterView 和 RouterLink
 * 2) 提供了两个对象 this.$router 和 this.$route
 */
  
app.mount('#app')
