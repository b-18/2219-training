export const RULE_TYPE = [
  {
    value: 1,
    label: "学员",
  },
  {
    value: 2,
    label: "班主任",
  },
  {
    value: 3,
    label: "讲师",
  },
  {
    value: 4,
    label: "管理员",
  },
];
