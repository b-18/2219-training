type Result<T = undefined> = {
  code: string;
  message: string;
  data: T;
};
