type Student = {
  id: number;
  username: string;
  password: string;
  phone: string;
  type: number;
  create_at: number;
};
