import http from "../utils/http";

export function getAll() {
  return http.get<any, Result<Student[]>>("/students");
}

export function get(id: Student['id']) {
  return http.get<any, Result<Student[]>>(`/students/${id}`);
}

export function create(student: Omit<Student, "id" | "create_at">) {
  return http.post<any, Result>("/students", {
    ...student,
    create_at: Date.now(),
  });
}

export function update(student: Omit<Student, "create_at">) {
  return http.put(`/students/${student.id}`, student);
}

export function remove(id: Student["id"]) {
  return http.delete(`/students/${id}`);
}
