import { createApp } from "vue";
import { createPinia } from "pinia";

import ElementPlus from "element-plus";
import * as ElementPlusIcons from "@element-plus/icons-vue";

import "reset-css";
import "element-plus/dist/index.css";

import App from "./App.vue";
import router from "./router";

const app = createApp(App);

app.use(createPinia());
app.use(router);

app.use(ElementPlus);

type IconKeys = keyof typeof ElementPlusIcons;
type KeyAlias = { [key in IconKeys]?: string };
// 注册图标
// 图标的 menu 组件名称 和 html 的 menu 冲突了, 改个名字
// menu -> icon-menu
const keyAlias: KeyAlias = {
  Menu: "IconMenu",
};
for (let key of Object.keys(ElementPlusIcons)) {
  app.component(
    keyAlias[key as keyof KeyAlias] ?? key,
    ElementPlusIcons[key as IconKeys]
  );
}

app.mount("#app");
