import { defineStore } from "pinia";

import * as studentApi from "../api/student";

type State = {
  students: Student[];
  isFormVisible: boolean;
  formTargetId?: Student["id"];
};

export const useStudentStore = defineStore("student-store", {
  state: (): State => {
    return {
      students: [],
      isFormVisible: false, // 表单是否显示
      formTargetId: undefined, // 打开表单时, 对应的 id. 如果是新建, 则为 undefined
    };
  },
  getters: {},
  actions: {
    // 列表数据
    async fetchAll() {
      const result = await studentApi.getAll();
      this.students = result.data;
    },
    // 新建
    async create(student: Parameters<typeof studentApi.create>[0]) {
      await studentApi.create(student);
      this.fetchAll();
    },
    // 删除
    async remove(id: Student["id"]) {
      await studentApi.remove(id);
      const index = this.students.findIndex((item) => item.id === id);
      this.students.splice(index, 1);
    },
    // 更新
    async update(student: Omit<Student, "create_at">) {
      await studentApi.update(student);
      const index = this.students.findIndex((item) => item.id === student.id);
      this.students[index] = {
        ...this.students[index],
        ...student,
      };
    },
    // 打开表单
    openForm(targetId?: Student["id"]) {
      // targetId: 打开表单时, 对应的数据的 id
      this.formTargetId = targetId;
      this.isFormVisible = true;
    },
    // 关闭表单
    closeForm() {
      this.isFormVisible = false;
    },
  },
});
